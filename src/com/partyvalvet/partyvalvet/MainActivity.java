package com.partyvalvet.partyvalvet;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.partyvalvet.partyvalvet.NewInvite.Fragment_List_New_Invite;
import com.partyvalvet.partyvalvet.contact.Fragment_Contact_List;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.fragment.Fragment_Create_event_selection;
import com.partyvalvet.partyvalvet.fragment.Fragment_Edit_Invitation;
import com.partyvalvet.partyvalvet.fragment.Fragment_Free_Templates;
import com.partyvalvet.partyvalvet.fragment.Fragment_Home;
import com.partyvalvet.partyvalvet.fragment.Fragment_Login;
import com.partyvalvet.partyvalvet.fragment.Fragment_Profile;
import com.partyvalvet.partyvalvet.fragment.Fragment_Select_Envelope;
import com.partyvalvet.partyvalvet.fragment.Fragment_Select_Envelope_Liners;
import com.partyvalvet.partyvalvet.fragment.Fragment_Select_Stamp;
import com.partyvalvet.partyvalvet.fragment.Fragment_Select_Your_invitetion;
import com.partyvalvet.partyvalvet.fragment.Fragment_invitation_selection;
import com.partyvalvet.partyvalvet.fragment.Fragment_invitetion_grid;
import com.partyvalvet.partyvalvet.fragment.Fragment_preview_envelop;
import com.partyvalvet.partyvalvet.fragment.Fragment_preview_envelop_liners;
import com.partyvalvet.partyvalvet.fragment.Fragment_preview_invitetion;
import com.partyvalvet.partyvalvet.fragment.Fragment_preview_stamp;
import com.partyvalvet.partyvalvet.fragment.Fragment_send_invitetion;
import com.partyvalvet.partyvalvet.fragment.My_parties.Fragment_My_parties;
import com.partyvalvet.partyvalvet.fragment.RSVP.Fragment_My_RSVP;
import com.partyvalvet.partyvalvet.fragment.RSVP.guest.Fragment_My_RSVP_attending_details;
import com.partyvalvet.partyvalvet.fragment.purchased_item.Fragment_My_Envelop_Liners;
import com.partyvalvet.partyvalvet.fragment.purchased_item.Fragment_My_Envelopes;
import com.partyvalvet.partyvalvet.fragment.purchased_item.Fragment_My_Invitataion;
import com.partyvalvet.partyvalvet.fragment.purchased_item.Fragment_My_Stamps;
import com.partyvalvet.partyvalvet.fragment.store.Fragment_Store;
import com.partyvalvet.partyvalvet.fragment.store.Fragment_Store_Templates;
import com.partyvalvet.partyvalvet.helper.ImageLoaderRec;
import com.partyvalvet.partyvalvet.navdrawer.MainLayout;


public class MainActivity extends FragmentActivity implements OnClickListener{

	
	private boolean doubleBackToExitPressedOnce = false;
	
	ViewPager pager;
	
	
	int expand =0;
	public static FrameLayout frm_title;
	public static ImageView img_main_menu,img_main_notification,img_main_setting,img_main_info,img_main_profile_pic;
	/*img_main_plusminus*/

	public static TextView txt_main_title,txt_main_done,txt_main_myprofile,txt_main_myprofile_mail,txt_main_myaddress,txt_main_newparty,txt_main_newinvite,txt_main_myparties,txt_main_myrsvps,txt_main_logout;
	/*txt_main_purchaseditem,txt_main_myinvitations,txt_main_myenevelopes,txt_main_myenevelopliners,txt_main_mystamp,txt_main_store,txt_main_restore_purchase*/

	public static LinearLayout lin_main_myprofile,lin_main_myaddress,lin_main_newparty,lin_main_newinvite,lin_main_myparties,lin_main_myrsvps,lin_setting;
	/*lin_main_purchaseditem,lin_main_expand,lin_main_myinvitations,lin_main_myenevelopes,lin_main_myenevelopliners,lin_main_mystamp,lin_main_store,lin_main_restore_purchase*/
	ImageLoaderRec imageLoaderRec;
	
	
  public static  MainLayout mainLayout;
    Fragment fragment = null;
    Font_Setting fs;
    SharedPreferences preflogin ;
    Editor editor;
  
    public static Context ctx;
    public static String screen= "home",login="";
    
    private static MainActivity mParentInstance = null;
 
    
   public static Button notifCount;
    static int mNotifCount = 0;
    public MainActivity() {
    	// TODO Auto-generated constructor stub
    	 mParentInstance = MainActivity.this;
    }
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		 //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

       
        super.onCreate(savedInstanceState);
		ctx=this;
		 mainLayout = (MainLayout)this.getLayoutInflater().inflate(R.layout.activity_main, null);
	        setContentView(mainLayout);
	        fs = new Font_Setting(getApplicationContext());
	        imageLoaderRec = new ImageLoaderRec(getApplicationContext());
	        bindid();
	         getKeyHash();
	         preflogin = getSharedPreferences("login",
	 				Context.MODE_PRIVATE);
	         
	         if (preflogin!=null) {
	        	 
	        	 String userid =preflogin.getString("id", "");
	        	 
	        	 if (!userid.equalsIgnoreCase("")) {
	        		 txt_main_logout.setText("Log Out");
	        		 txt_main_myprofile_mail.setVisibility(View.VISIBLE);
	        		 txt_main_myprofile.setText(preflogin.getString("name", ""));
	        		 
	        		 txt_main_myprofile_mail.setText(preflogin.getString("email", ""));
	        		 if (!preflogin.getString("image", "").equalsIgnoreCase("")) {
	 					imageLoaderRec.DisplayImage(preflogin.getString("image", ""), MainActivity.img_main_profile_pic);	
	 				}
	        		 
				}
	        	 else
	        	 {
	        		 txt_main_logout.setText("Log In");
	        		 txt_main_myprofile.setText("My Profile");
	        		 txt_main_myprofile_mail.setVisibility(View.INVISIBLE);
	        	 }
				
			}
	         expand =1;
			// lin_main_expand.setVisibility(View.GONE);
			// img_main_plusminus.setImageDrawable(getResources().getDrawable(R.drawable.plusexpand));
	      
			 img_main_menu.setOnClickListener(new OnClickListener() {
		            @Override
		            public void onClick(View v) {
		                // Show/hide the menu
		                mainLayout.toggleMenu();
		            }
		        });
	 		FragmentManager fm = getSupportFragmentManager();
	        FragmentTransaction ft = fm.beginTransaction();
	       
	        Fragment fragment = null;
	 		//fragment = new Fragment_Home();
	        fragment = new Fragment_Select_Your_invitetion();
		       //ft.setCustomAnimations(R.anim.animation_leave,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
		        // **********     Action Bar    **************
		        
		        
		        
		      	 			
		      
	}

	 private void bindid() {
		// TODO Auto-generated method stub
		 //img_main_plusminus=(ImageView)findViewById(R.id.img_main_plusminus);
		 img_main_menu=(ImageView)findViewById(R.id.img_main_menu);
		 img_main_notification=(ImageView)findViewById(R.id.img_main_notification);
		 img_main_setting=(ImageView)findViewById(R.id.img_main_setting);
		 img_main_info=(ImageView)findViewById(R.id.img_main_info);
		 frm_title=(FrameLayout)findViewById(R.id.frm_title);
		 img_main_profile_pic=(ImageView)findViewById(R.id.img_main_profile_pic);
		 
		 img_main_menu.setOnClickListener(this);
		 img_main_notification.setOnClickListener(this);
		 img_main_setting.setOnClickListener(this);
		 img_main_info.setOnClickListener(this);

		 txt_main_title=(TextView)findViewById(R.id.txt_main_title);
		 txt_main_done=(TextView)findViewById(R.id.txt_main_done);
		 txt_main_myprofile=(TextView)findViewById(R.id.txt_main_myprofile);
		 txt_main_myprofile_mail=(TextView)findViewById(R.id.txt_main_myprofile_mail);
		 txt_main_myaddress=(TextView)findViewById(R.id.txt_main_myaddress);
		 txt_main_newparty=(TextView)findViewById(R.id.txt_main_newparty);
		 txt_main_newinvite=(TextView)findViewById(R.id.txt_main_newinvite);
		 txt_main_myparties=(TextView)findViewById(R.id.txt_main_myparties);
		 /*txt_main_purchaseditem=(TextView)findViewById(R.id.txt_main_purchaseditem);
		 txt_main_myinvitations=(TextView)findViewById(R.id.txt_main_myinvitations);
		 txt_main_myenevelopes=(TextView)findViewById(R.id.txt_main_myenevelopes);
		 txt_main_myenevelopliners=(TextView)findViewById(R.id.txt_main_myenevelopliners);
		 txt_main_mystamp=(TextView)findViewById(R.id.txt_main_mystamp);*/
		 txt_main_myrsvps=(TextView)findViewById(R.id.txt_main_myrsvps);
		 /*txt_main_store=(TextView)findViewById(R.id.txt_main_store);
		 txt_main_restore_purchase=(TextView)findViewById(R.id.txt_main_restore_purchase);*/
		 txt_main_logout=(TextView)findViewById(R.id.txt_main_logout);
		 
		 fs.settypefacetextview(txt_main_title);
		 fs.settypefacetextviewbellota(txt_main_done);
		 fs.settypefacetextviewbellota(txt_main_myprofile);
		 fs.settypefacetextviewbellota(txt_main_myprofile_mail);
		 fs.settypefacetextviewbellota(txt_main_myaddress);
		 fs.settypefacetextviewbellota(txt_main_newparty);
		 fs.settypefacetextviewbellota(txt_main_newinvite);
		 fs.settypefacetextviewbellota(txt_main_myparties);
		 /*fs.settypefacetextviewbellota(txt_main_purchaseditem);
		 fs.settypefacetextviewbellota(txt_main_myinvitations);
		 fs.settypefacetextviewbellota(txt_main_myenevelopes);
		 fs.settypefacetextviewbellota(txt_main_myenevelopliners);
		 fs.settypefacetextviewbellota(txt_main_mystamp);*/
		 fs.settypefacetextviewbellota(txt_main_myrsvps);
		 /*fs.settypefacetextviewbellota(txt_main_store);
		 fs.settypefacetextviewbellota(txt_main_restore_purchase);*/
		 fs.settypefacetextview(txt_main_logout);
		 
		 
		 /*txt_main_title
		 txt_main_done
		 txt_main_myprofile
		 txt_main_myprofile_mail
		 txt_main_myaddress
		 txt_main_newparty
		 txt_main_newinvite
		 txt_main_myparties
		 txt_main_purchaseditem
		 txt_main_myinvitations
		 txt_main_myenevelopes
		 txt_main_myenevelopliners
		 txt_main_mystamp
		 txt_main_myrsvps
		 txt_main_store
		 txt_main_restore_purchase
		 txt_main_logout*/
		 

		 lin_main_myprofile=(LinearLayout)findViewById(R.id.lin_main_myprofile);
		 lin_main_myaddress=(LinearLayout)findViewById(R.id.lin_main_myaddress);
		 lin_main_newparty=(LinearLayout)findViewById(R.id.lin_main_newparty);
		 lin_main_newinvite=(LinearLayout)findViewById(R.id.lin_main_newinvite);
		 lin_main_myparties=(LinearLayout)findViewById(R.id.lin_main_myparties);
		/* lin_main_purchaseditem=(LinearLayout)findViewById(R.id.lin_main_purchaseditem);
		 lin_main_expand=(LinearLayout)findViewById(R.id.lin_main_expand);
		 lin_main_myinvitations=(LinearLayout)findViewById(R.id.lin_main_myinvitations);
		 lin_main_myenevelopes=(LinearLayout)findViewById(R.id.lin_main_myenevelopes);
		 lin_main_myenevelopliners=(LinearLayout)findViewById(R.id.lin_main_myenevelopliners);
		 lin_main_mystamp=(LinearLayout)findViewById(R.id.lin_main_mystamp);*/
		 lin_main_myrsvps=(LinearLayout)findViewById(R.id.lin_main_myrsvps);
		 /*lin_main_store=(LinearLayout)findViewById(R.id.lin_main_store);
		 lin_main_restore_purchase=(LinearLayout)findViewById(R.id.lin_main_restore_purchase);*/
		 lin_setting=(LinearLayout)findViewById(R.id.lin_setting);
		 
		 lin_main_myprofile.setOnClickListener(this);
		 lin_main_myaddress.setOnClickListener(this);
		 lin_main_newparty.setOnClickListener(this);
		 lin_main_newinvite.setOnClickListener(this);
		 lin_main_myparties.setOnClickListener(this);
		 /*lin_main_purchaseditem.setOnClickListener(this);
		 lin_main_expand.setOnClickListener(this);
		 lin_main_myinvitations.setOnClickListener(this);
		 lin_main_myenevelopes.setOnClickListener(this);
		 lin_main_myenevelopliners.setOnClickListener(this);
		 lin_main_mystamp.setOnClickListener(this);*/
		 lin_main_myrsvps.setOnClickListener(this);
		/* lin_main_store.setOnClickListener(this);
		 lin_main_restore_purchase.setOnClickListener(this);*/
		 
		 txt_main_logout.setOnClickListener(this);
		 
		
		
	}

	public void toggleMenu(View v){
	        mainLayout.toggleMenu();
	    }

	


	
	
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
		togglemenu();
		 FragmentManager fm = MainActivity.this.getSupportFragmentManager();
	        FragmentTransaction ft = fm.beginTransaction();
	        
			if(screen.equalsIgnoreCase("home"))
			{
				if (doubleBackToExitPressedOnce) {
					super.onBackPressed();
					return;
				}

				this.doubleBackToExitPressedOnce = true;
				Toast.makeText(this, getString(R.string.backtoexit),
						Toast.LENGTH_SHORT).show();

				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						doubleBackToExitPressedOnce = false;
					}
				}, 2000);
			}
			else if(screen.equalsIgnoreCase("signup"))
			{
				
				fragment = new Fragment_Login();
				
			}
			else if(screen.equalsIgnoreCase("login"))
			{
				
				fragment = new Fragment_Home();
				
			}
			else if(screen.equalsIgnoreCase("createeventselection"))
			{
				
				//fragment = new Fragment_Home();
				fragment = new Fragment_Select_Your_invitetion();
				
			}
			else if(screen.equalsIgnoreCase("freetemplates"))
			{
				
				fragment = new Fragment_Create_event_selection();
				
			}
			else if (screen.equalsIgnoreCase("selectinvitation")) {
				//fragment = new Fragment_Free_Templates();
				fragment = new Fragment_Home();
			}
			
			else if (screen.equalsIgnoreCase("invitationgrid")) {
				//fragment = new Fragment_Select_Your_invitetion();
				fragment = new Fragment_Free_Templates();
			}
			else if (screen.equalsIgnoreCase("invitationselection")) {
				fragment = new Fragment_invitetion_grid();
			}
			else if (screen.equalsIgnoreCase("selectenvelop")) {
				fragment = new Fragment_invitation_selection();
			}
			else if (screen.equalsIgnoreCase("selectenvelopliners")) {
				fragment = new Fragment_Select_Envelope();
			}
			else if (screen.equalsIgnoreCase("selectstamp")) {
				fragment = new Fragment_Select_Envelope_Liners();
			}
			else if (screen.equalsIgnoreCase("editinvitation")) {
				fragment = new Fragment_Select_Stamp();
			}
			else if (screen.equalsIgnoreCase("previewenvelop")) {
				fragment = new Fragment_Edit_Invitation();
			}
			else if (screen.equalsIgnoreCase("previewenvelopliners")) {
				fragment = new Fragment_preview_envelop();
			}
			else if (screen.equalsIgnoreCase("previewsatamps")) {
				fragment = new Fragment_preview_envelop_liners();
			}
			else if (screen.equalsIgnoreCase("previewinvitation")) {
				fragment = new Fragment_preview_stamp();
			}
			else if (screen.equalsIgnoreCase("sendinvitation")) {
				fragment = new Fragment_preview_invitetion();
			}
			else if (screen.equalsIgnoreCase("templates")) {
				
				fragment = new Fragment_Store();
			}
			else if (screen.equalsIgnoreCase("storeinvitation")) {
				fragment = new Fragment_Store_Templates();
			}
			else if (screen.equalsIgnoreCase("addtocalendar")) {
				fragment = new Fragment_send_invitetion();
			}
			else if (screen.equalsIgnoreCase("viewmap")) {
				fragment = new Fragment_preview_invitetion();
			}
			else if (screen.equalsIgnoreCase("myinvitation"))
			{
				fragment =null;
				 mainLayout.toggleMenu();
			}
			else if (screen.equalsIgnoreCase("myrsvphosting"))
			{
				fragment = new Fragment_My_RSVP();
			}
			else if (screen.equalsIgnoreCase("rsvpscheduledetail"))
			{
				fragment = new Fragment_send_invitetion();
			}
			else if (screen.equalsIgnoreCase("changersvp"))
			{
				fragment = new Fragment_My_RSVP_attending_details();
			}
			
			
			
			
			
			
			
			
			if(!screen.equalsIgnoreCase("home")&&fragment!=null)
			{
			
			 ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
			 ft.replace(R.id.framl_main_screen, fragment);
	         ft.commit();
			}

		
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();

		Fragment fragment = null;
		switch (v.getId()) {
		
		/*case R.id.lin_main_purchaseditem:
			if (check()) {
				
			
			if (expand == 0) {

				expand = 1;
				lin_main_expand.setVisibility(View.GONE);
				img_main_plusminus.setImageDrawable(getResources().getDrawable(
						R.drawable.plusexpand));
			} else {
				expand = 0;
				lin_main_expand.setVisibility(View.VISIBLE);
				img_main_plusminus.setImageDrawable(getResources().getDrawable(
						R.drawable.minusexpand));
			}
			}
			break;
*/
		case R.id.lin_main_myprofile:

			if (txt_main_logout.getText().toString().equalsIgnoreCase("Log Out")) {
				togglemenu();
				fragment = new Fragment_Profile();
				
				
			       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
			       ft.replace(R.id.framl_main_screen, fragment);
			        ft.commit();
			}
				
			break;

		case R.id.lin_main_myaddress:
			if (check()) {
				
			
			togglemenu();
			fragment = new Fragment_Contact_List();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			}
			break;
		case R.id.lin_main_newparty:
			if (check()) {
				
		
			togglemenu();
		//	fragment = new Fragment_Home();
			 fragment = new Fragment_Select_Your_invitetion();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			}
			break;

		case R.id.lin_main_newinvite:
			if (check()) {
				togglemenu();
				fragment = new Fragment_List_New_Invite();
				
			       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
			       ft.replace(R.id.framl_main_screen, fragment);
			        ft.commit();
			}
			

			break;
		case R.id.lin_main_myparties:
			
			if (check()) {
				togglemenu();
				fragment = new Fragment_My_parties();
				
			       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
			       ft.replace(R.id.framl_main_screen, fragment);
			        ft.commit();
			}
			

			break;

		/*case R.id.lin_main_myinvitations:
			if (check()) {
				
			
			 togglemenu();
			fragment = new Fragment_My_Invitataion();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			}
			break;*/
		/*case R.id.lin_main_myenevelopes:
			if (check()) {
				
			
			togglemenu();
			fragment = new Fragment_My_Envelopes();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			}
			break;*/
		/*case R.id.lin_main_myenevelopliners:
			if (check()) {
				
			
			togglemenu();
			fragment = new Fragment_My_Envelop_Liners();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			}

			break;*/
	/*	case R.id.lin_main_mystamp:
			if (check()) {
				
			
			togglemenu();
			fragment = new Fragment_My_Stamps();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			}
			break;*/
		case R.id.lin_main_myrsvps:
			if (check()) {
				
			
			togglemenu();
			fragment = new Fragment_My_RSVP();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			}
			break;
		/*case R.id.lin_main_store:
			if (check()) {
				
			
			togglemenu();
			fragment = new Fragment_Store();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			}
			break;*/
		/*case R.id.lin_main_restore_purchase:
			if (check()) {
				
				
				togglemenu();
				fragment = new Fragment_Store();
				
			       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
			       ft.replace(R.id.framl_main_screen, fragment);
			        ft.commit();
				}
			break;*/
			
		case R.id.txt_main_logout:
			/*fragment = new Fragment_Home();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();*/
			togglemenu();
			if (txt_main_logout.getText().toString().equalsIgnoreCase("Log Out")) {
				AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
				builder1.setMessage("Are you sure you want to log out ");
				builder1.setCancelable(true);
				builder1.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								
							
									preflogin.edit().clear().commit();
									txt_main_logout.setText("Log In");
									
									 txt_main_myprofile.setText("My Profile");
					        		 txt_main_myprofile_mail.setVisibility(View.INVISIBLE);
					        		 
					        		FragmentManager fm = getSupportFragmentManager();
					     	        FragmentTransaction ft = fm.beginTransaction();
					     	       
					     	        Fragment fragment = null;				
					        		 //fragment = new Fragment_Home();
					        		 fragment = new Fragment_Select_Your_invitetion();
								       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
								       ft.replace(R.id.framl_main_screen, fragment);
								        ft.commit();
								
							}
						});
				builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int arg1) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});
				AlertDialog alert11 = builder1.create();
				alert11.show();
			}
			else if(txt_main_logout.getText().toString().equalsIgnoreCase("Log In"))
			{
				
				fragment = new Fragment_Login();
				
			       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
			       ft.replace(R.id.framl_main_screen, fragment);
			        ft.commit();
			}
			

			break;


		default:
			break;
		}

	}
	/*private void toast(String string) {
		// TODO Auto-generated method stub
		Toast.makeText(getApplicationContext(), string, Toast.LENGTH_SHORT).show();
		
	}*/

	private void getKeyHash() {
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					getPackageName(), PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.d("Keyhash:",
						Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {
		} catch (NoSuchAlgorithmException e) {
		}
	}
	 
	public void togglemenu()
	{
		 if( MainLayout.currentMenuState == MainLayout.MenuState.SHOWN)
	   	  {
	   		  mainLayout.toggleMenu();
	   	  }
	}
	public boolean check()
	{
		if (txt_main_logout.getText().toString().equalsIgnoreCase("Log Out")) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	 
}
