package com.partyvalvet.partyvalvet.customeAdepter;

import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.android.gms.actions.ReserveIntents;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.fragment.RSVP.Fragment_My_RSVP_Hosting;
import com.partyvalvet.partyvalvet.fragment.RSVP.guest.Fragment_My_RSVP_attending_details;

@SuppressLint("ViewHolder")
public class Customlist_List_New_Invite extends BaseAdapter {

	// Declare Variables
	Context context;
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	Font_Setting fs;
	SharedPreferences preflogin ;
	Editor editor;
	HashMap<String, String> resultp = new HashMap<String, String>();

	public Customlist_List_New_Invite(Context context,
			ArrayList<HashMap<String, String>> arraylist) {
		this.context = context;

		data = arraylist;

	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables

		
		
		TextView txt_rsvp_ename;
		//ImageView img_ls_image;
		fs = new Font_Setting(context);
		
		

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		final View itemView = inflater.inflate(
				R.layout.listview_my_rsvp_host_adttend, parent, false);
		// Get the position
		resultp = data.get(position);

		preflogin = context.getSharedPreferences("login",Context.MODE_PRIVATE);
		editor = preflogin.edit();
		
		//txt_list_address=(TextView)itemView.findViewById(R.id.txt_list_address);
		
		txt_rsvp_ename=(TextView)itemView.findViewById(R.id.txt_rsvp_ename);
	//	txt_rsvp_edate=(TextView)itemView.findViewById(R.id.txt_rsvp_edate);
		
		
		
		
		
		fs.settypefacetextviewbellota(txt_rsvp_ename);
		//fs.settypefacetextviewbellota(txt_rsvp_edate);
		
		
		
		
		//map.put("event_status",objdata.getString("event_status"));

		/*String eventdetail = "" ;
		if (resultp.get("event_status").equalsIgnoreCase("completed")) {
			eventdetail = "<font color=#ffffff>"+resultp.get("event_name")+"</font><font color=#ffffff> - "+resultp.get("event_date")+"</font>";	
		}
		else if (resultp.get("event_status").equalsIgnoreCase("pending")) {
			eventdetail = "<font color=#ffffff>"+resultp.get("event_name")+"</font><font color=#f33f74> - "+resultp.get("event_date")+"</font>";
		}*/
		//txt_rsvp_ename.setText(Html.fromHtml(eventdetail));
		txt_rsvp_ename.setText(resultp.get("event_name")+" - "+resultp.get("event_date"));
		//txt_rsvp_edate.setText(resultp.get("event_date"));
		
		
		
		// Locate the TextViews in listview_item.xml
		
		
//		String fulladdress =resultp.get("country")+"\n"+resultp.get("city")+"\n"+resultp.get("neighboor")+"\n"+resultp.get("street")+"\n"+resultp.get("street_house_number")+"\n"+resultp.get("postal_code")+"\n"+resultp.get("who_receive_package_name");
		
		
		/*itemView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				resultp = data.get(position);
				editor.putString("eid", resultp.get("eid"));
				editor.putString("event_name",resultp.get("event_name"));
				editor.putString("event_status", resultp.get("event_status"));
				editor.commit();
				
				FragmentManager fm = ((FragmentActivity) context).getSupportFragmentManager();
		        FragmentTransaction ft = fm.beginTransaction();
		       
		        Fragment fragment = null;
		        fragment = new Fragment_My_RSVP_attending_details();
				
			       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
			       ft.replace(R.id.framl_main_screen, fragment);
			        ft.commit();
			}
		});*/
		return itemView;
	}

	
	

}