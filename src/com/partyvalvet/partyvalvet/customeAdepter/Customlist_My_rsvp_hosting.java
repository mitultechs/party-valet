package com.partyvalvet.partyvalvet.customeAdepter;

import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;

@SuppressLint("ViewHolder")
public class Customlist_My_rsvp_hosting extends BaseAdapter {

	// Declare Variables
	Context context;
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	Font_Setting fs;
	SharedPreferences preflogin ;
	Editor editor;
	HashMap<String, String> resultp = new HashMap<String, String>();

	public Customlist_My_rsvp_hosting(Context context,
			ArrayList<HashMap<String, String>> arraylist) {
		this.context = context;

		data = arraylist;

	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables

		
		
		TextView txt_list_my_rsvp_hosting_name,txt_list_my_rsvp_hosting_status;
		ImageView img_list_my_rsvp_hosting_yesno;
		fs = new Font_Setting(context);
		
		

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		final View itemView = inflater.inflate(
				R.layout.listview_my_rsvp_hosting, parent, false);
		// Get the position
		resultp = data.get(position);

		preflogin = context.getSharedPreferences("login",Context.MODE_PRIVATE);
		editor = preflogin.edit();
		
		//txt_list_address=(TextView)itemView.findViewById(R.id.txt_list_address);
		
		txt_list_my_rsvp_hosting_name=(TextView)itemView.findViewById(R.id.txt_list_my_rsvp_hosting_name);
		txt_list_my_rsvp_hosting_status=(TextView)itemView.findViewById(R.id.txt_list_my_rsvp_hosting_status);
		
		img_list_my_rsvp_hosting_yesno=(ImageView)itemView.findViewById(R.id.img_list_my_rsvp_hosting_yesno);
		
		
		
		fs.settypefacetextviewbellota(txt_list_my_rsvp_hosting_name);
		fs.settypefacetextviewbellota(txt_list_my_rsvp_hosting_status);
		
		
		
		
		//map.put("event_status",objdata.getString("event_status"));
/*
		String eventdetail = "" ;
		if (resultp.get("event_status").equalsIgnoreCase("completed")) {
			eventdetail = "<font color=#ffffff>"+resultp.get("event_name")+"</font><font color=#ffffff> - "+resultp.get("event_date")+"</font>";	
		}
		else if (resultp.get("event_status").equalsIgnoreCase("pending")) {
			eventdetail = "<font color=#ffffff>"+resultp.get("event_name")+"</font><font color=#f33f74> - "+resultp.get("event_date")+"</font>";
		}
		*/
		
		txt_list_my_rsvp_hosting_name.setText(resultp.get("name"));
		String status =resultp.get("rsvp");
		String eventstatus="";
		if (status.equalsIgnoreCase("0")) {
			eventstatus="Pending";
			txt_list_my_rsvp_hosting_status.setBackground(context.getResources().getDrawable(R.drawable.rshosingpending));
		}
		else if (status.equalsIgnoreCase("1")) {
			eventstatus="Yes";
			img_list_my_rsvp_hosting_yesno.setImageDrawable(context.getResources().getDrawable(R.drawable.rshostingselected));
			txt_list_my_rsvp_hosting_status.setBackground(context.getResources().getDrawable(R.drawable.rshosingyes));
		}
		else if (status.equalsIgnoreCase("2")) {
			eventstatus="No";
			txt_list_my_rsvp_hosting_status.setBackground(context.getResources().getDrawable(R.drawable.rshosingno));
		}
		txt_list_my_rsvp_hosting_status.setText(eventstatus);
		
		
		
		
		// Locate the TextViews in listview_item.xml
		
		
//		String fulladdress =resultp.get("country")+"\n"+resultp.get("city")+"\n"+resultp.get("neighboor")+"\n"+resultp.get("street")+"\n"+resultp.get("street_house_number")+"\n"+resultp.get("postal_code")+"\n"+resultp.get("who_receive_package_name");
		
		
			
		return itemView;
	}

	
	

}