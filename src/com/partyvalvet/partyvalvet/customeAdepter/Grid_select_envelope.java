package com.partyvalvet.partyvalvet.customeAdepter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.imageloader.ImageLoader;





public class Grid_select_envelope extends BaseAdapter{




	public ImageLoader imageLoader;
    private Context mContext;
  //  public final String[] web;
    public final ArrayList<String> price,images;
 //   private final int[] Imageid;
    Font_Setting fs;

      public Grid_select_envelope(Context c,ArrayList<String> images,ArrayList<String> price) {
          mContext = c;
          this.images = images;
          this.price = price;
         // this.web = web;
      }

      /*@Override
      public int getCount() {
          // TODO Auto-generated method stub
          return web.length;
      }*/
      @Override
  	public int getCount() {
  		// TODO Auto-generated method stub
    	  return images.size();
  	}
      @Override
      public Object getItem(int position) {
          // TODO Auto-generated method stub
          return null;
      }

      @Override
      public long getItemId(int position) {
          // TODO Auto-generated method stub
          return 0;
      }

      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
          // TODO Auto-generated method stub
          View grid;
          LayoutInflater inflater = (LayoutInflater) mContext
              .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
          fs = new Font_Setting(mContext);
          imageLoader = new ImageLoader(mContext);
          if (convertView == null) {

              grid = new View(mContext);
              grid = inflater.inflate(R.layout.gridview_select_envelop, null);
              TextView textView = (TextView) grid.findViewById(R.id.txt_buy);
              fs.settypefacetextviewbellota(textView);
              ImageView imageView = (ImageView)grid.findViewById(R.id.grid_image);
              textView.setText(price.get(position).toString());
              //imageView.setImageResource(images.get(position).toString());
          	imageLoader.DisplayImage(images.get(position).toString(), imageView);
              
          } else {
              grid = (View) convertView;
          }

          return grid;
      }

	
}