package com.partyvalvet.partyvalvet.font;

import com.partyvalvet.partyvalvet.MainActivity;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Font_Setting {

	Context ctx;
	Typeface tf,tfbullet,tfAlexBrush,tfBlackJack,tfEngland,tfPRISTINA,tfITCKRIST;
	public Font_Setting(Context context) {
		// TODO Auto-generated constructor stub
		ctx = context;
		 tf= Typeface.createFromAsset(ctx.getAssets(), "fonts/DancingScript-Regular.otf");
		 
		 tfbullet= Typeface.createFromAsset(ctx.getAssets(), "fonts/Bellota-Regular.otf");
		 tfAlexBrush= Typeface.createFromAsset(ctx.getAssets(), "fonts/AlexBrush-Regular.ttf");
		 tfBlackJack= Typeface.createFromAsset(ctx.getAssets(), "fonts/black_jack.ttf");
		 tfEngland= Typeface.createFromAsset(ctx.getAssets(), "fonts/england.ttf");
		 tfPRISTINA=Typeface.createFromAsset(ctx.getAssets(), "fonts/PRISTINA.ttf");
		 tfITCKRIST=Typeface.createFromAsset(ctx.getAssets(), "fonts/ITCKRIST.ttf");
	}
	public void settypefacetextview(TextView id)
	{		
		id.setTypeface(tf);	
	}
	public void settypefacebutton(Button id)
	{		
		id.setTypeface(tf);	
	}
	public void settypefaceedittext(EditText id)
	{		
		id.setTypeface(tf);	
	}
	
	
	public void settypefacetextviewbellota(TextView id)
	{		
		id.setTypeface(tfbullet);	
	}
	public void settypefacebuttonbellota(Button id)
	{		
		id.setTypeface(tfbullet);	
	}
	public void settypefaceedittextbellota(EditText id)
	{		
		id.setTypeface(tfbullet);	
	}
	
	public void toast(String msg)
	{
		Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
	}
	
	
	
	/* Alexbrush*/
	
	public void settypefacetextviewalexbrush(TextView id)
	{		
		id.setTypeface(tfAlexBrush);	
	}
	
	/* BlackJack*/
	
	public void settypefacetextviewblackjack(TextView id)
	{		
		id.setTypeface(tfBlackJack);	
	}
	
	/*PRISTINA.ttf*/
	public void settypefacetextviewPRISTINA(TextView id)
	{		
		id.setTypeface(tfPRISTINA);	
	}
	/*tfITCKRIST*/
	public void settypefacetextviewITCKRIST(TextView id)
	{		
		id.setTypeface(tfITCKRIST);	
	}
	
	/* England */
	
	public void settypefacetextviewengland(TextView id)
	{		
		id.setTypeface(tfEngland);	
	}
	public void hideheading()
	{
		MainActivity.img_main_menu.setVisibility(View.INVISIBLE);
		MainActivity.img_main_menu.setEnabled(false);
		
		MainActivity.img_main_info.setVisibility(View.GONE);
		MainActivity.img_main_notification.setVisibility(View.GONE);
		MainActivity.img_main_setting.setVisibility(View.GONE);
		//MainActivity.frm_title.setVisibility(View.GONE);
	}
	public void showheading()
	{
		MainActivity.img_main_menu.setVisibility(View.VISIBLE);
		
		MainActivity.img_main_menu.setEnabled(true);
		MainActivity.img_main_info.setVisibility(View.VISIBLE);
		MainActivity.img_main_notification.setVisibility(View.VISIBLE);
		MainActivity.img_main_setting.setVisibility(View.VISIBLE);
		MainActivity.txt_main_done.setVisibility(View.GONE);
		//MainActivity.frm_title.setVisibility(View.VISIBLE);
	}
}
