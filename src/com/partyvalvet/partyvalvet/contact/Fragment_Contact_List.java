package com.partyvalvet.partyvalvet.contact;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_Contact_List extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	
	
	private ListView list_contact;
//	private List<ContactBean> list = new ArrayList<ContactBean>();
	ArrayList<String> vCard,namelist,idlist,numlist;
	ArrayAdapter<String> adapter;
	
	int flag =0;
	ProgressDialog pDialog;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_contacts_list , null);
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();

		namelist= new ArrayList<String>();
        idlist = new ArrayList<String>();
        numlist =new ArrayList<String>();
        
		bindid(view);
		
		new getcontact().execute();
		/* String selection = ContactsContract.Contacts.IN_VISIBLE_GROUP + " = '"
	                + ("1") + "'";
	        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME
	                + " COLLATE LOCALIZED ASC";
	        Cursor phones = getActivity().getContentResolver().query(
					ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
					null, null);
	        Cursor phones = getActivity().getContentResolver().query(				
	        ContactsContract.Contacts.CONTENT_URI, null, selection
	        + " AND " + ContactsContract.Contacts.HAS_PHONE_NUMBER
	        + "=1", null, sortOrder);
	        
			while (phones.moveToNext()) {

				//get(phones);
				  //String id = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
				 String id = phones.getString(phones.getColumnIndex(ContactsContract.Contacts._ID));
				 idlist.add(id);
			      
				String name = phones
						.getString(phones
								.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				namelist.add(name);
				if (Integer.parseInt(phones.getString(phones.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) 
	            {
	                // Query phone here. Covered next
	                Cursor phonesnum = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id,null, null); 
	                while (phonesnum.moveToNext()) { 
	                         String phoneNumber = phonesnum.getString(phonesnum.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
	                         numlist.add(phoneNumber);
	                         Log.i("Number", phoneNumber);
	                        } 
	                phonesnum.close(); 
	            }
				
				
			}
			phones.close();
*/			/*adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_multiple_choice, namelist);
			list_contact.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
			list_contact.setAdapter(adapter);*/
		
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		list_contact = (ListView)view.findViewById(R.id.list_contact);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	public class getcontact extends AsyncTask<Void, Void, Void>
	{

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			
			 String selection = ContactsContract.Contacts.IN_VISIBLE_GROUP + " = '"
		                + ("1") + "'";
		        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME
		                + " COLLATE LOCALIZED ASC";
		       /* Cursor phones = getActivity().getContentResolver().query(
						ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
						null, null);*/
		        Cursor phones = getActivity().getContentResolver().query(				
		        ContactsContract.Contacts.CONTENT_URI, null, selection
		        + " AND " + ContactsContract.Contacts.HAS_PHONE_NUMBER
		        + "=1", null, sortOrder);
		        
				while (phones.moveToNext()) {

					//get(phones);
					  //String id = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
					 String id = phones.getString(phones.getColumnIndex(ContactsContract.Contacts._ID));
					 idlist.add(id);
				      
					String name = phones
							.getString(phones
									.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
					namelist.add(name);
					if (Integer.parseInt(phones.getString(phones.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) 
		            {
		                // Query phone here. Covered next
		                Cursor phonesnum = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id,null, null); 
		                while (phonesnum.moveToNext()) { 
		                         String phoneNumber = phonesnum.getString(phonesnum.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
		                         numlist.add(phoneNumber);
		                         Log.i("Number", phoneNumber);
		                        } 
		                phonesnum.close(); 
		            }
					
					
				}
				phones.close();

			return null;
			
			
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_multiple_choice, namelist);
			list_contact.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
			list_contact.setAdapter(adapter);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			//pDialog.setMessage(getString(R.string.plzwait));
			pDialog.setMessage("Fatching Your Contact...");
            pDialog.setCancelable(false);
            pDialog.show();
		}
		
	}
}
