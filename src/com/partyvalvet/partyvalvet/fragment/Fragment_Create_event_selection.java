package com.partyvalvet.partyvalvet.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_Create_event_selection extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	TextView txt_frag_home_invitation,txt_frag_home_enevelop,txt_frag_home_enevelop_Liners,txt_frag_home_stamps;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_create_event_selection, null);
		MainActivity.screen="createeventselection";
		MainActivity.txt_main_title.setText("");
				
		//MainActivity.txt_main_title.setText(getString(R.string.headingfreetemplates));
		fs = new Font_Setting(getActivity());
		fs.showheading();
		sh = new ServiceHandler();
		
		bindid(view);
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub

		txt_frag_home_invitation=(TextView)view.findViewById(R.id.txt_frag_home_invitation);
		txt_frag_home_enevelop=(TextView)view.findViewById(R.id.txt_frag_home_enevelop);
		txt_frag_home_enevelop_Liners=(TextView)view.findViewById(R.id.txt_frag_home_enevelop_Liners);
		txt_frag_home_stamps=(TextView)view.findViewById(R.id.txt_frag_home_stamps);
		
		
		fs.settypefacetextview(txt_frag_home_invitation);
		fs.settypefacetextview(txt_frag_home_enevelop);
		fs.settypefacetextview(txt_frag_home_enevelop_Liners);
		fs.settypefacetextview(txt_frag_home_stamps);
		
		txt_frag_home_invitation.setOnClickListener(this);
		txt_frag_home_enevelop.setOnClickListener(this);
		txt_frag_home_enevelop_Liners.setOnClickListener(this);
		txt_frag_home_stamps.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		
		
		// TODO Auto-generated method stub
		FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
       
        Fragment fragment = null;
		switch (v.getId()) {
		case R.id.txt_frag_home_invitation:

			fragment = new Fragment_Free_Templates();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;
		case R.id.txt_frag_home_enevelop:
			fragment = new Fragment_Select_Envelope();
			
			ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;
		case R.id.txt_frag_home_enevelop_Liners:

			fragment = new Fragment_Select_Envelope_Liners();
			
			ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;
		case R.id.txt_frag_home_stamps:

			fragment = new Fragment_Select_Stamp();
			
			ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;

		default:
			break;
		}
	}

}
