package com.partyvalvet.partyvalvet.fragment.purchased_item;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.customeAdepter.Grid_select_myenvelope;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.fragment.Fragment_invitation_selection;
import com.partyvalvet.partyvalvet.helper.ConnectionDetector;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_My_Invitataion extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	
	
	SharedPreferences preflogin;
	String userid,urlmyinvitation,url ,c_id,plan;
	
	GridView grid;
	LinearLayout lin_spin_invitation;
	/*TextView txt_select_invitation;
	Spinner spin_se_select;*/
	ArrayList<String> myimages;
	
	AsyncTask<Void, Void, Void> task;
	
	int flaglistmyinvitaiton=0;;
	
	
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_invitetion_grid, null);
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		
		
		bindid(view);

		lin_spin_invitation.setVisibility(View.GONE);
		
		myimages= new ArrayList<String>();
		preflogin = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
		if (preflogin!=null) {
			userid = preflogin.getString("id", "");
			c_id= preflogin.getString("c_id", "");
			plan = preflogin.getString("plan", "");
		}
		MainActivity.screen ="myinvitation";
		MainActivity.txt_main_title.setText(getString(R.string.headingmyinvitation));
		bindid(view);
		
	
		if (ConnectionDetector
				.isNetworkAvailable(getActivity())) {

			urlmyinvitation = getString(R.string.url)+"select_my_card/index.php?user_id="+userid;
			//url = getString(R.string.url)+"select_envelope_liner/index.php?plan=all";
			
		task =	new getmyinvitation();
		task.execute();
		
		} else {
			fs.toast(getString(R.string.nointernet));
		}

		
	                /*grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	 
	                    @Override
	                    public void onItemClick(AdapterView<?> parent, View view,
	                                            int position, long id) {
	                        //Toast.makeText(getActivity(), "You Clicked at " +web[+ position], Toast.LENGTH_SHORT).show();
	                    	FragmentManager fm = getActivity().getSupportFragmentManager();
	                        FragmentTransaction ft = fm.beginTransaction();
	                       
	                        Fragment fragment = null;
	                        
	                        fragment = new Fragment_invitation_selection();
	            			
	            			ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
	            		       ft.replace(R.id.framl_main_screen, fragment);
	            		        ft.commit();
	 
	                    }
	                });
*/		

		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		grid=(GridView)view.findViewById(R.id.grid);
		 
		 lin_spin_invitation=(LinearLayout)view.findViewById(R.id.lin_spin_invitation);
		 /*txt_select_invitation=(TextView)view.findViewById(R.id.txt_select_invitation);
		 txt_select_invitation.setText("Select Invitation From");
		 fs.settypefacetextviewbellota(txt_select_invitation);
		 spin_se_select=(Spinner)view.findViewById(R.id.spin_se_select);
		 
		 lin_select_invitation.setOnClickListener(this);*/
	}

	class getmyinvitation extends AsyncTask<Void, Void, Void>
	{
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			String jsonString = sh.makeServiceCall(urlmyinvitation, ServiceHandler.GET);
			if (jsonString!=null) {
				try {
					JSONObject  jobj = new JSONObject(jsonString);
					if (jobj.has("200")) {
						
						JSONArray jary = jobj.getJSONArray("200");
						if (jary.length()>0) {
							flaglistmyinvitaiton=1;
							for (int i = 0; i < jary.length(); i++) {
								
								JSONObject objdata = jary.getJSONObject(i);
								myimages.add(objdata.getString("image"));
							//	price.add(objdata.getString("price"));
								
							}
						}
						else
						{
							flaglistmyinvitaiton=0;
						}
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
			if (flaglistmyinvitaiton==1) {
				Grid_select_myenvelope adapter = new Grid_select_myenvelope(getActivity(),myimages);
				
		        grid.setAdapter(adapter);
			}
			
			
			
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
		
	}
	
	public void onDestroy() {
	    super.onDestroy();

	    if(task != null && task.getStatus() != AsyncTask.Status.FINISHED) {
	        task.cancel(true);
	    }
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		/*case R.id.lin_select_invitation:
			spin_se_select.performClick();
			
			break;

		default:
			break;*/
		}
	}


}
