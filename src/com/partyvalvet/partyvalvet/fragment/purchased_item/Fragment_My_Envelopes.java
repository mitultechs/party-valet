package com.partyvalvet.partyvalvet.fragment.purchased_item;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.customeAdepter.Grid_select_myenvelope;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.fragment.purchased_item.Fragment_My_Invitataion.getmyinvitation;
import com.partyvalvet.partyvalvet.helper.ConnectionDetector;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_My_Envelopes extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	
	
	SharedPreferences preflogin;
	String userid,urlmyeneveleop,url ,c_id,plan;
	
	GridView grid;
	LinearLayout lin_spin_invitation,lin_se_bottom_btns;
	/*TextView txt_select_invitation;
	Spinner spin_se_select;*/
	ArrayList<String> myimages;
	
	AsyncTask<Void, Void, Void> task;
	
	int flaglistmyenevelop=0;;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_select_envelope, null);
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		
		bindid(view);
		lin_spin_invitation.setVisibility(View.GONE);
		lin_se_bottom_btns.setVisibility(View.GONE);
		myimages= new ArrayList<String>();
		preflogin = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
		if (preflogin!=null) {
			userid = preflogin.getString("id", "");
			/*c_id= preflogin.getString("c_id", "");
			plan = preflogin.getString("plan", "");*/
		}
		MainActivity.screen ="myinvitation";
		MainActivity.txt_main_title.setText(getString(R.string.headingmyenvelop));
		bindid(view);
		
	
		if (ConnectionDetector
				.isNetworkAvailable(getActivity())) {

			//urlmyinvitation = getString(R.string.url)+"select_my_card/index.php?user_id="+userid;
			//url = getString(R.string.url)+"select_envelope_liner/index.php?plan=all";
			urlmyeneveleop = getString(R.string.url)+"select_my_envelope/index.php?user_id="+userid;
			
		task =	new getmyinvitation();
		task.execute();
		
		} else {
			fs.toast(getString(R.string.nointernet));
		}
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		grid=(GridView)view.findViewById(R.id.grid);
		 
		 lin_spin_invitation=(LinearLayout)view.findViewById(R.id.lin_spin_invitation);
		 lin_se_bottom_btns=(LinearLayout)view.findViewById(R.id.lin_se_bottom_btns);
	}

	class getmyinvitation extends AsyncTask<Void, Void, Void>
	{
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			String jsonString = sh.makeServiceCall(urlmyeneveleop, ServiceHandler.GET);
			if (jsonString!=null) {
				try {
					JSONObject  jobj = new JSONObject(jsonString);
					if (jobj.has("200")) {
						
						JSONArray jary = jobj.getJSONArray("200");
						if (jary.length()>0) {
							flaglistmyenevelop=1;
							for (int i = 0; i < jary.length(); i++) {
								
								JSONObject objdata = jary.getJSONObject(i);
								myimages.add(objdata.getString("image"));
							//	price.add(objdata.getString("price"));
								
							}
						}
						else
						{
							flaglistmyenevelop=0;
						}
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
			if (flaglistmyenevelop==1) {
				Grid_select_myenvelope adapter = new Grid_select_myenvelope(getActivity(),myimages);
				
		        grid.setAdapter(adapter);
			}
			
			
			
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
		
	}
	
	public void onDestroy() {
	    super.onDestroy();

	    if(task != null && task.getStatus() != AsyncTask.Status.FINISHED) {
	        task.cancel(true);
	    }
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		/*case R.id.lin_select_invitation:
			spin_se_select.performClick();
			
			break;

		default:
			break;*/
		}
	}

}
