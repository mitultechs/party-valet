package com.partyvalvet.partyvalvet.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_view_map extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	
	private GoogleMap map;
	MapView mapView;
	ImageView img_map_close;
	
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_view_map, null);
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		MainActivity.screen = "viewmap";
		MainActivity.txt_main_title.setText(getString(R.string.headingviewmap));
		bindid(view);
		
		
		mapView.onCreate(savedInstanceState);
		mapView.onResume();// needed to get the map to display immediately

		try {
			MapsInitializer.initialize(getActivity().getApplicationContext());
		} catch (Exception e) {
			e.printStackTrace();
		}

		map = mapView.getMap();
		map.setMyLocationEnabled(true);
		
		map.getUiSettings().setZoomControlsEnabled(false);

		// Enable / Disable my location button
		map.getUiSettings().setMyLocationButtonEnabled(true);

		// Enable / Disable Compass icon
		map.getUiSettings().setCompassEnabled(true);

		// Enable / Disable Rotate gesture
		map.getUiSettings().setRotateGesturesEnabled(true);

		// Enable / Disable zooming functionality
		map.getUiSettings().setZoomGesturesEnabled(true);

		

		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		mapView = (MapView) view.findViewById(R.id.mapView);
		img_map_close =(ImageView)view.findViewById(R.id.img_map_close);
		img_map_close.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
       
        Fragment fragment = null;
		switch (v.getId()) {
		case R.id.img_map_close:
			fragment = new Fragment_preview_invitetion();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;

		default:
			break;
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		mapView.onResume();
	}

}
