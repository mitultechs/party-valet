package com.partyvalvet.partyvalvet.fragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.BitmapProcessor;
import com.partyvalvet.partyvalvet.helper.ConnectionDetector;
import com.partyvalvet.partyvalvet.helper.ImageLoaderRec;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;


public class Fragment_Profile extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	
	ImageView img_edit_profile_image;
	TextView txt_edit_profile_edit,txt_edit_profile_change_pic,txt_edit_profile_name,txt_edit_profile_phone,txt_edit_profile_email,txt_edit_profile_bdate,txt_edit_profile_statetitle,txt_edit_profile_state,txt_edit_profile_update,txt_edit_profile_cancel;
	EditText edt_edit_profile_name,edt_edit_profile_phone,edt_edit_profile_email,edt_edit_profile_bdate;
	LinearLayout lin_edit_profile_bdate,lin_edit_profile_state;
	
	Spinner spin_edit_profile_state;
	
	SharedPreferences preflogin;
	Editor editor;
	 ImageLoaderRec imageLoader ;
	AsyncTask< Void, Void, Void> task;
	ArrayList<String> liststate;
	
	String url,urlupdate,msg="",imageprofile,urluser;
	String id,name,phone,email,dob,state,gender;
	
	ProgressDialog pDialog,progressDialog;
	int flag =0,st=0;
	
	
	// image
	
	 String response_str;
	  HttpEntity resEntity;
	  String filename;
	  String selectedPath1 = "NONE";
	   String selectedPath2 = "NONE";
	  
	  private static final int IMAGE_PICK 	= 1;
		private static final int IMAGE_CAPTURE 	= 2;
		
		 Uri selectedImageUri=null;
		 private Bitmap image;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View view = inflater.inflate(R.layout.fragment_profile, null);
		preflogin = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
		editor = preflogin.edit();
		imageLoader = new ImageLoaderRec(getActivity());
		
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		
		liststate = new ArrayList<String>();
		liststate.add("State");
		MainActivity.screen ="myinvitation";
		MainActivity.txt_main_title.setText(getString(R.string.headingprofile));
		bindid(view);
		///========================= edited enable false ===============
		edt_edit_profile_name.setEnabled(false);
		edt_edit_profile_phone.setEnabled(false);
		edt_edit_profile_email.setEnabled(false);
		/*edt_edit_profile_pass.setEnabled(false);
		edt_edit_profile_pass_confpass.setEnabled(false);*/
		edt_edit_profile_bdate.setEnabled(false);
		
		lin_edit_profile_bdate.setEnabled(false);
		lin_edit_profile_state.setEnabled(false);
		txt_edit_profile_change_pic.setEnabled(false);
		txt_edit_profile_change_pic.setVisibility(View.INVISIBLE);
		txt_edit_profile_change_pic.setEnabled(false);
		///========================= edited enable false ===============
		if (preflogin!=null) {

			id=preflogin.getString("id", "");
			name=preflogin.getString("name", "");
			phone=preflogin.getString("phone", "");
			email=preflogin.getString("email", "");
			dob=preflogin.getString("dob", "");
			state=preflogin.getString("state", "");
			gender=preflogin.getString("gender", "");
			imageprofile=preflogin.getString("image", "");
			
			if (!imageprofile.equalsIgnoreCase("")) {
				imageLoader.DisplayImage(imageprofile, img_edit_profile_image);
			}

		
			edt_edit_profile_name.setText(name);
			edt_edit_profile_phone.setText(phone);
			edt_edit_profile_email.setText(email);
			
			edt_edit_profile_bdate.setText(dob);
			txt_edit_profile_state.setText(state);
			
		}
		if(ConnectionDetector.isNetworkAvailable(getActivity()))
		{
		
		url = getString(R.string.url)+"state/";
		task =new getstate();
		
		task.execute();
		}
		else
		{
			fs.toast(getString(R.string.nointernet));
		}

		spin_edit_profile_state.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if (st==0) {
					txt_edit_profile_state.setText(state);
					
				}
				else
				{
				txt_edit_profile_state.setText(spin_edit_profile_state.getSelectedItem().toString());
				}				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				txt_edit_profile_state.setText(state);
			}
		});

		//txt_edit_profile_state.setText(state);
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub

		img_edit_profile_image=(ImageView)view.findViewById(R.id.img_edit_profile_image);
		
		txt_edit_profile_edit=(TextView)view.findViewById(R.id.txt_edit_profile_edit);
		txt_edit_profile_change_pic=(TextView)view.findViewById(R.id.txt_edit_profile_change_pic);
		txt_edit_profile_name=(TextView)view.findViewById(R.id.txt_edit_profile_name);
		txt_edit_profile_phone=(TextView)view.findViewById(R.id.txt_edit_profile_phone);
		txt_edit_profile_email=(TextView)view.findViewById(R.id.txt_edit_profile_email);
		/*txt_edit_profile_pass=(TextView)view.findViewById(R.id.txt_edit_profile_pass);
		txt_edit_profile_pass_confpass=(TextView)view.findViewById(R.id.txt_edit_profile_pass_confpass);*/
		txt_edit_profile_bdate=(TextView)view.findViewById(R.id.txt_edit_profile_bdate);
		txt_edit_profile_statetitle=(TextView)view.findViewById(R.id.txt_edit_profile_statetitle);
		txt_edit_profile_state=(TextView)view.findViewById(R.id.txt_edit_profile_state);
		txt_edit_profile_update=(TextView)view.findViewById(R.id.txt_edit_profile_update);
		txt_edit_profile_cancel=(TextView)view.findViewById(R.id.txt_edit_profile_cancel);
		
		fs.settypefacetextviewbellota(txt_edit_profile_edit);
		fs.settypefacetextviewbellota(txt_edit_profile_change_pic);
		fs.settypefacetextviewbellota(txt_edit_profile_name);
		fs.settypefacetextviewbellota(txt_edit_profile_phone);
		fs.settypefacetextviewbellota(txt_edit_profile_email);
		/*fs.settypefacetextviewbellota(txt_edit_profile_pass);
		fs.settypefacetextviewbellota(txt_edit_profile_pass_confpass);*/
		fs.settypefacetextviewbellota(txt_edit_profile_bdate);
		fs.settypefacetextviewbellota(txt_edit_profile_statetitle);
		fs.settypefacetextviewbellota(txt_edit_profile_state);
		fs.settypefacetextview(txt_edit_profile_update);
		fs.settypefacetextview(txt_edit_profile_cancel);
		
		txt_edit_profile_update.setOnClickListener(this);
		
		txt_edit_profile_cancel.setOnClickListener(this);
		
		txt_edit_profile_edit.setOnClickListener(this);
		
		txt_edit_profile_change_pic.setOnClickListener(this);
		
		
		edt_edit_profile_name=(EditText)view.findViewById(R.id.edt_edit_profile_name);
		edt_edit_profile_phone=(EditText)view.findViewById(R.id.edt_edit_profile_phone);
		edt_edit_profile_email=(EditText)view.findViewById(R.id.edt_edit_profile_email);
		/*edt_edit_profile_pass=(EditText)view.findViewById(R.id.edt_edit_profile_pass);
		edt_edit_profile_pass_confpass=(EditText)view.findViewById(R.id.edt_edit_profile_pass_confpass);*/
		edt_edit_profile_bdate=(EditText)view.findViewById(R.id.edt_edit_profile_bdate);

		fs.settypefacetextviewbellota(edt_edit_profile_name);
		fs.settypefacetextviewbellota(edt_edit_profile_phone);
		fs.settypefacetextviewbellota(edt_edit_profile_email);
		/*fs.settypefacetextviewbellota(edt_edit_profile_pass);
		fs.settypefacetextviewbellota(edt_edit_profile_pass_confpass);*/
		fs.settypefacetextviewbellota(edt_edit_profile_bdate);
		
		lin_edit_profile_bdate=(LinearLayout)view.findViewById(R.id.lin_edit_profile_bdate);
		lin_edit_profile_state=(LinearLayout)view.findViewById(R.id.lin_edit_profile_state);
		
		lin_edit_profile_bdate.setOnClickListener(this);
		lin_edit_profile_state.setOnClickListener(this);
		
		spin_edit_profile_state=(Spinner)view.findViewById(R.id.spin_edit_profile_state);
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.lin_edit_profile_bdate:
			Calendar c = Calendar.getInstance();
	        int mYear = c.get(Calendar.YEAR);
	        int mMonth = c.get(Calendar.MONTH);
	        int mDay = c.get(Calendar.DAY_OF_MONTH);
	        System.out.println("the selected " + mDay);
	        DatePickerDialog dialog = new DatePickerDialog(getActivity(),
	                new mDateSetListener(), mYear, mMonth, mDay);
	        
	        dialog.show();
			break;
		case R.id.lin_edit_profile_state:
			
			spin_edit_profile_state.performClick();
			
			break;
			
		case R.id.txt_edit_profile_edit:
			
			disablefield();			
			break;
		case R.id.txt_edit_profile_cancel:
			enablefield();
		break;
		case R.id.txt_edit_profile_update:
			if (!edt_edit_profile_name.getText().toString().trim().equalsIgnoreCase("")&&!edt_edit_profile_phone.getText().toString().trim().equalsIgnoreCase("")&&!edt_edit_profile_email.getText().toString().trim().equalsIgnoreCase("")&&!edt_edit_profile_bdate.getText().toString().trim().equalsIgnoreCase("")&&!txt_edit_profile_state.getText().toString().equalsIgnoreCase("state")&&!txt_edit_profile_state.getText().toString().equalsIgnoreCase("")) {
				if(isValidEmail(edt_edit_profile_email.getText().toString().trim()))
				{
					
					
						
					
					
					if(ConnectionDetector.isNetworkAvailable(getActivity()))
					{
					
					urlupdate = getString(R.string.url)+"update_profile/index.php?id="+id+"&name="+edt_edit_profile_name.getText().toString().trim()+"&email="+edt_edit_profile_email.getText().toString().trim()+"&phone="+edt_edit_profile_phone.getText().toString().trim()+"&dob="+edt_edit_profile_bdate.getText().toString().trim()+"&gender="+gender+"&state="+txt_edit_profile_state.getText().toString().trim();
					//new signup().execute();
					
					progressDialog = ProgressDialog.show(getActivity(), "", getString(R.string.plzwait), false);
					 Thread thread=new Thread(new Runnable(){
		                   public void run(){
		                       doFileUpload();
		                       getActivity().runOnUiThread(new Runnable(){
		                           public void run() {
		                               if(progressDialog.isShowing())
		                               {
		                                   progressDialog.dismiss();
		                                   
		                                   if (response_str!=null) {
		                       				try {
		                       					
		                       					JSONObject jobj= new JSONObject(response_str);
		                       					if (jobj.has("200")) {
		                       						flag=1;
		                       						fs.toast(getString(R.string.profileupdatesucess));
		                       						
		                       						
		                    						editor.putString("name", edt_edit_profile_name.getText().toString().trim());
		                    						editor.putString("phone",edt_edit_profile_phone.getText().toString().trim());
		                    						editor.putString("email", edt_edit_profile_email.getText().toString().trim());
		                    						editor.putString("dob", edt_edit_profile_bdate.getText().toString().trim());
		                    						editor.putString("state", txt_edit_profile_state.getText().toString().trim());
		                    						editor.putString("gender",gender);
		                    						
		                    						editor.commit();
		                    						enablefield();
		                    						MainActivity.txt_main_myprofile.setText(edt_edit_profile_name.getText().toString().trim());
		                    						MainActivity.txt_main_myprofile_mail.setText(edt_edit_profile_email.getText().toString().trim());
		                       						/*Intent intent = new Intent(Coach_register.this, Login_screen.class);
		                       						startActivity(intent);
		                       						finish();*/
		                    						urluser ="http://27.109.11.53/eventapp/api/user_detail/index.php?user_id="+id;
		                    						new updateprofile().execute(); 
		                       					}
		                       					else if(jobj.has("400"))
		                       					{
		                       						fs.toast(jobj.getString("400"));
		                       					}
		                       					else
		                       					{
		                       						flag=0;
		                       					}
		                       				} catch (JSONException e) {
		                       					// TODO Auto-generated catch block
		                       					e.printStackTrace();
		                       				}
		                       			}
		                               }
		                           }
		                       });
		                   }
		           });
		           thread.start();
					}
					else
					{
						fs.toast(getString(R.string.nointernet));
					}
					
					
					
					
				}
				else
				{
					fs.toast(getString(R.string.entervalidemail));
				}
				
			}
			else if (edt_edit_profile_name.getText().toString().trim().equalsIgnoreCase("")) {
				fs.toast(getString(R.string.entername));	
			}
			else if (edt_edit_profile_phone.getText().toString().trim().equalsIgnoreCase("")) {
				fs.toast(getString(R.string.enterphone));
			}
			else if (edt_edit_profile_email.getText().toString().trim().equalsIgnoreCase("")) {
				fs.toast(getString(R.string.enteremail));
			}
			/*else if (edt_ss_password.getText().toString().trim().equalsIgnoreCase("")) {
				fs.toast(getString(R.string.enterpassword));
			}
			else if (edt_ss_confpassword.getText().toString().trim().equalsIgnoreCase("")) {
				fs.toast(getString(R.string.enterconfpassword));
			}
			*/
			else if (edt_edit_profile_bdate.getText().toString().trim().equalsIgnoreCase("")) {
				fs.toast(getString(R.string.selectbdate));
			}
			else if (txt_edit_profile_state.getText().toString().trim().equalsIgnoreCase("")) {
				fs.toast(getString(R.string.selectstate));
			}
			else if (txt_edit_profile_state.getText().toString().trim().equalsIgnoreCase("State")) {
				fs.toast(getString(R.string.selectstate));
			}

			
			
		break;
		case R.id.txt_edit_profile_change_pic:
			selectImage();
			
			
			break;
		
		default:
			break;
		}
	}
	class mDateSetListener implements DatePickerDialog.OnDateSetListener {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                int dayOfMonth) {
            // TODO Auto-generated method stub
            // getCalender();
            int mYear = year;
            int mMonth = monthOfYear;
            int mDay = dayOfMonth;
            Calendar c = Calendar.getInstance();
	        int curYear = c.get(Calendar.YEAR);
            if (mYear>=curYear) {
				Toast.makeText(getActivity(), "Please check your birthdate", Toast.LENGTH_SHORT).show();
				edt_edit_profile_bdate.setText("");
			}
            
            else if(mYear<=curYear-10)
            {
            	edt_edit_profile_bdate.setText(new StringBuilder()
                    // Month is 0 based so add 1
            	
            	.append(mYear).append("-").append(mMonth + 1).append("-").append(mDay).append(" ")
                    );
            }
            else 
            {
            	Toast.makeText(getActivity(), "Please check your birthdate", Toast.LENGTH_SHORT).show();
            	edt_edit_profile_bdate.setText("");
            }
            //System.out.println(v.getText().toString());


        }
    }
	
	class getstate extends AsyncTask<Void, Void, Void>
	{

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
		
			
			String jsonString = sh.makeServiceCall(url, ServiceHandler.GET);
			if (jsonString!=null) {
				
				try {
					JSONObject jobj = new JSONObject(jsonString);
					if (jobj.has("200")) {
						//flag =1;
						
						JSONArray arystate = jobj.getJSONArray("200");
						for (int i = 0; i < arystate.length(); i++) {
							
							JSONObject objdata = arystate.getJSONObject(i);
							liststate.add(objdata.getString("state"));
						}
						
					}
					else if(jobj.has("400"))
					{
						//flag =0;
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			ArrayAdapter<String> adptyear = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_new, liststate);
			//adptyear.setDropDownViewResource(R.layout.spinner_item);
			spin_edit_profile_state.setAdapter(adptyear);
			txt_edit_profile_state.setText(state);
			
			urluser ="http://27.109.11.53/eventapp/api/user_detail/index.php?user_id="+id;
			
			task = new updateprofile();
			task.execute();
		
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
		
	}

	public void enablefield()
	{
		/*edt_edit_profile_name.setFocusable(false);
		edt_edit_profile_phone.setFocusable(false);
		edt_edit_profile_email.setFocusable(false);
		edt_edit_profile_pass.setFocusable(false);
		edt_edit_profile_pass_confpass.setFocusable(false);
		edt_edit_profile_bdate.setFocusable(false);
		*/
		
		txt_edit_profile_edit.setVisibility(View.VISIBLE);
		txt_edit_profile_edit.setEnabled(true);
		
		txt_edit_profile_change_pic.setVisibility(View.INVISIBLE);
		txt_edit_profile_change_pic.setEnabled(false);
		
		edt_edit_profile_name.setEnabled(false);
		edt_edit_profile_phone.setEnabled(false);
		edt_edit_profile_email.setEnabled(false);
		/*edt_edit_profile_pass.setEnabled(false);
		edt_edit_profile_pass_confpass.setEnabled(false);*/
		edt_edit_profile_bdate.setEnabled(false);
		
		lin_edit_profile_bdate.setEnabled(false);
		lin_edit_profile_state.setEnabled(false);
		txt_edit_profile_change_pic.setEnabled(false);
	/*	edt_edit_profile_name.setKeyListener(null);
		edt_edit_profile_phone.setKeyListener(null);
		edt_edit_profile_email.setKeyListener(null);
		edt_edit_profile_pass.setKeyListener(null);
		edt_edit_profile_pass_confpass.setKeyListener(null);
		edt_edit_profile_bdate.setKeyListener(null);*/
	//	Toast.makeText(getActivity(), "safdfas", Toast.LENGTH_SHORT).show();
		
	/*	android:clickable="false" 
		        android:cursorVisible="false" 
		        android:focusable="false" 
		        android:focusableInTouchMode="false"*/
	}
	public void disablefield()
	{
		st=1;
		txt_edit_profile_edit.setVisibility(View.INVISIBLE);
		txt_edit_profile_edit.setEnabled(false);
		
		lin_edit_profile_bdate.setEnabled(true);
		lin_edit_profile_state.setEnabled(true);
		txt_edit_profile_change_pic.setEnabled(true);

		
		edt_edit_profile_name.setEnabled(true);
		edt_edit_profile_phone.setEnabled(true);
		edt_edit_profile_email.setEnabled(true);
		//edt_edit_profile_pass.setEnabled(true);
		//edt_edit_profile_pass_confpass.setEnabled(true);
		//edt_edit_profile_bdate.setEnabled(true);
		
		edt_edit_profile_name.setFocusable(true);
		edt_edit_profile_phone.setFocusable(true);
		edt_edit_profile_email.setFocusable(true);
		//edt_edit_profile_pass.setFocusable(true);
		//edt_edit_profile_pass_confpass.setFocusable(true);
		edt_edit_profile_bdate.setFocusable(true);
		
		

		txt_edit_profile_change_pic.setVisibility(View.VISIBLE);
		txt_edit_profile_change_pic.setEnabled(true);
		
		
		//Toast.makeText(getActivity(), "safdfas", Toast.LENGTH_SHORT).show();
		
	}
	public void onDestroy() {
	    super.onDestroy();

	    if(task != null && task.getStatus() != AsyncTask.Status.FINISHED) {
	        task.cancel(true);
	    }
	}
	
	
	class updateprofile extends AsyncTask<Void, Void, Void>
	{
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
		//	urlupdate = urlupdate.replaceAll(" ", "%20");
			String jsonString = sh.makeServiceCall(urluser, ServiceHandler.GET);
			if (jsonString!=null) {
				
				try {
					JSONObject jobj = new JSONObject(jsonString);
					if (jobj.has("200")) {
						flag =1;
						
						JSONObject objdata = jobj.getJSONObject("200");
						editor.putString("id",objdata.getString("id"));
						editor.putString("name", objdata.getString("name"));
						editor.putString("phone", objdata.getString("phone"));
						editor.putString("email", objdata.getString("email"));
						editor.putString("dob", objdata.getString("dob"));
						editor.putString("state", objdata.getString("state"));
						editor.putString("gender", objdata.getString("gender"));
						editor.putString("image", objdata.getString("image"));
						editor.commit();
						
					}
					else if(jobj.has("400"))
					{
						msg ="";
						flag =0;
						if (!jobj.getString("400").toString().equalsIgnoreCase("0")) {
							msg = jobj.getString("400").toString();
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			if (flag==1) {
				
				MainActivity.txt_main_myprofile.setText(preflogin.getString("name", ""));
				
				MainActivity.txt_main_myprofile_mail.setText(preflogin.getString("email", ""));
				String img = preflogin.getString("image", "");
				if (!img.equalsIgnoreCase("")) {
					imageLoader.DisplayImage(img, MainActivity.img_main_profile_pic);	
					imageLoader.DisplayImage(img, img_edit_profile_image);
				}
				
				
				
				id=preflogin.getString("id", "");
				name=preflogin.getString("name", "");
				phone=preflogin.getString("phone", "");
				email=preflogin.getString("email", "");
				dob=preflogin.getString("dob", "");
				state=preflogin.getString("state", "");
				gender=preflogin.getString("gender", "");
				imageprofile=preflogin.getString("image", "");
				
				

			
				edt_edit_profile_name.setText(name);
				edt_edit_profile_phone.setText(phone);
				edt_edit_profile_email.setText(email);
				
				edt_edit_profile_bdate.setText(dob);
				txt_edit_profile_state.setText(state);

				/*fs.toast(getString(R.string.profileupdatesucess));
				edt_ls_email.setText("");
				edt_ls_password.setText("");
				
					
				FragmentManager fm = getActivity().getSupportFragmentManager();
		        FragmentTransaction ft = fm.beginTransaction();
		       
		        Fragment fragment = null;
		 		fragment = new Fragment_Login();
				
			       //ft.setCustomAnimations(R.anim.animation_leave,R.anim.animation_leave);
			       ft.replace(R.id.framl_main_screen, fragment);
			        ft.commit();*/
			}
			else if(flag==0)
			{
			/*	if (!msg.equalsIgnoreCase("")) {
					fs.toast(msg);
				}
				else
				{
					fs.toast(getString(R.string.somethingwrong));	
				}*/
				
				//edt_ls_password.setText("");
			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage(getString(R.string.plzwait));
            pDialog.setCancelable(false);
            pDialog.show();
		}
		
	}

	
	private void doFileUpload(){
	     
        //   File file1 = new File(selectedPath1);
           //File file2 = new File(selectedPath1);
           //String urlString = getString(R.string.url).toString()+"addphoto.php?uid="+userid;
	//String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		urlupdate = urlupdate.replaceAll(" ", "%20");
	String urlString = urlupdate;
	//+"&session_id="+session_id
	//String urlString = getString(R.string.url).toString()+"orderpost/index.php?user_id="+userid+"&delivery_date=2015-05-06&shipped_to="+postaddressid+"&status_id=2&imageqty[]=1&imagesize[]=4*6&imageqty[]=2&imagesize[]=4*8";
           
           try
           {      
        	   
        	  MultipartEntity reqEntity = new MultipartEntity();
             // File file1 = new File("/storage/emulated/0/FSM/11098706_812527472176791_418525782_n.jpg");
             // File file2 = new File("/storage/emulated/0/FSM/11232597_1418231045163543_1720053909_n.jpg");
         //  File file1 = new File("");
              /*Charset chars = Charset.forName("UTF-8");
              FileBody bin1 = new FileBody(file1);*/       
              
              /*if(file1.exists())
              {
            	  Log.d("file=========>>", "Exitstign");
              }
              else
              {
            	  Log.d("file=========>>", "Not exist");
              }*/
           
           //   StringBody stringB;  // Now lets add some extra information in a StringBody
               
            
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(urlString);
             
                //ContentBody cbFile1 = new FileBody(file1, "image/jpeg", "FILE_NAME");
               
                if (!selectedPath1.equalsIgnoreCase("NONE")) {
                	File fileupload = new File(selectedPath1);
                	ContentBody cbFile = new FileBody(fileupload, "image/jpeg");
                	reqEntity.addPart("image", cbFile);
				}
                	
				
                
                /*ContentBody cbFile = new FileBody(file1, "image/jpeg");
                ContentBody cbFile1 = new FileBody(file2, "image/jpeg");
                reqEntity.addPart("image[]", cbFile);
                
                reqEntity.addPart("image[]", cbFile1);*/
              //  reqEntity.addPart("image", new FileBody(file1));
                
                post.setEntity(reqEntity);
                HttpResponse response = client.execute(post);
              
                resEntity = response.getEntity();
                 response_str = EntityUtils.toString(resEntity);
               /*  String[] res = response_str.split("\n");
                 response_str= res[1];*/
                 
                if (resEntity != null) {
                 
                    Log.i("RESPONSE",response_str);
                    
                    //Toast.makeText(getActivity(),response_str , Toast.LENGTH_LONG).show();
              //      new getphoto().execute();
                  getActivity().runOnUiThread(new Runnable(){
                           public void run() {
                                try {
                                  // res.setTextColor(Color.BLUE);
                                  // res.setText("n Response from server : n " + response_str);
                                  // Toast.makeText(getApplicationContext(),"Upload Complete. Check the server uploads directory.", Toast.LENGTH_LONG).show();
                               
                               } catch (Exception e) {
                                   e.printStackTrace();
                               }
                              }
                       });
                }
           }
           catch (Exception ex){
                Log.e("Debug", "error: " + ex.getMessage(), ex);
           }
         }
	
	
	private void selectImage() {
		final CharSequence[] items = { "Take Photo", "Choose from Library",
				"Cancel" };

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Upload Profie Pic..!");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (items[item].equals("Take Photo")) {
					Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
					startActivityForResult(intent, IMAGE_CAPTURE);
				
				} else if (items[item].equals("Choose from Library")) {
					Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					intent.setType("image/*");
					startActivityForResult(Intent.createChooser(intent, "Pick a Picture"), IMAGE_PICK);
				} else if (items[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}
	//============================ Photo Upload onActivityResult ============================
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	
    	if (resultCode == Activity.RESULT_OK) { 
	    	switch (requestCode) {
			case IMAGE_PICK:	
				this.imageFromGallery(resultCode, data);
				
				  selectedImageUri = data.getData();
		            /*if (requestCode == SELECT_FILE1)
		            {*/
		                selectedPath1 = getPath(selectedImageUri);
		                System.out.println("selectedPath1 : " + selectedPath1);
		            //}
				break;
			case IMAGE_CAPTURE:
				this.imageFromCamera(resultCode, data);
				  /*if (requestCode == SELECT_FILE2)
		            {*/// Uri selectedImageUri = data.getData();
			/*	selectedImageUri = data.getData();
				   selectedPath1 = getPath(selectedImageUri);
		               // System.out.println("selectedPath2 : " + selectedPath2);
		            //}
		            tv.setText("Selected File paths : " + selectedPath1 + "," + selectedPath2);
		            */
		            
		            
		      /*******************/      
		            Bitmap photo = (Bitmap) data.getExtras().get("data"); 
		            img_edit_profile_image.setImageBitmap(getCroppedBitmap(photo, 180));
		        //    knop.setVisibility(Button.VISIBLE);


		            // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
		            Uri tempUri = getImageUri(getActivity(), photo);

		            // CALL THIS METHOD TO GET THE ACTUAL PATH
		            File finalFile = new File(getRealPathFromURI(tempUri));
		            selectedPath1 = getPath(tempUri);
		          //  System.out.println(mImageCaptureUri);
		            /************************/
		            
		            
		            
				break;
			default:
				break;
			}
    	}
    }
    
    /**
     * Update the imageView with new bitmap
     * @param newImage
     */
    private void updateImageView(Bitmap newImage) {
    	/*BitmapProcessor bitmapProcessor = new BitmapProcessor(newImage, 200, 200, 100);
    	
    	this.image = bitmapProcessor.getBitmap();
    	this.img_edit_profile_image.setImageBitmap(getCroppedBitmap(this.image, 100));*/

    	this.img_edit_profile_image.setImageBitmap(getCroppedBitmap(newImage, 180));
    }
    
    /**
     * Image result from camera
     * @param resultCode
     * @param data
     */
    private void imageFromCamera(int resultCode, Intent data) {
    	this.updateImageView((Bitmap) data.getExtras().get("data"));
    }
    
    /**
     * Image result from gallery
     * @param resultCode
     * @param data
     */
    private void imageFromGallery(int resultCode, Intent data) {
    	Uri selectedImage = data.getData();
    	String [] filePathColumn = {MediaStore.Images.Media.DATA};
    	
    	Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
    	cursor.moveToFirst();
    	
    	int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
    	String filePath = cursor.getString(columnIndex);
    	cursor.close();
    	
    	this.updateImageView(BitmapFactory.decodeFile(filePath));
    }
    
    /**
     * Click Listener for selecting images from phone gallery
     * @author tscolari
     *
     */
    
    @SuppressWarnings("deprecation")
 	public String getPath(Uri uri) {
             
         String res = null;
         String[] projection = { MediaStore.Images.Media.DATA };
         Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
         if(cursor.moveToFirst()){;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
         }
         cursor.close();
         return res;
     }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null); 
        cursor.moveToFirst(); 
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA); 
        return cursor.getString(idx); 
    }

	
	
	
    public final static boolean isValidEmail(CharSequence target) {
		return !TextUtils.isEmpty(target)
				&& android.util.Patterns.EMAIL_ADDRESS.matcher(target)
						.matches();
	}
    public static Bitmap getCroppedBitmap(Bitmap bmp, int radius) {
	    Bitmap sbmp;
	    if(bmp.getWidth() != radius || bmp.getHeight() != radius)
	        sbmp = Bitmap.createScaledBitmap(bmp, radius, radius, true);
	    else
	        sbmp = bmp;
	    Bitmap output = Bitmap.createBitmap(sbmp.getWidth(),
	            sbmp.getHeight(), Config.ARGB_8888);
	    Canvas canvas = new Canvas(output);

	    final int color = 0xffa19774;
	    final Paint paint = new Paint();
	    final Rect rect = new Rect(0, 0, sbmp.getWidth(), sbmp.getHeight());

	 //   paint.setAntiAlias(true);
	    paint.setFilterBitmap(true);
	    paint.setDither(true);
	    canvas.drawARGB(0, 0, 0, 0);
	    paint.setColor(Color.parseColor("#BAB399"));
	    canvas.drawCircle(sbmp.getWidth() / 2+0.7f, sbmp.getHeight() / 2+0.7f,
	            sbmp.getWidth() / 2+0.1f, paint);
	    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	    canvas.drawBitmap(sbmp, rect, rect, paint);


	            return output;
	}


}
