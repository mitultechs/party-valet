package com.partyvalvet.partyvalvet.fragment;

import java.util.Arrays;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.facebook.widget.LoginButton.UserInfoChangedCallback;
import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ConnectionDetector;
import com.partyvalvet.partyvalvet.helper.ImageLoaderRec;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;
import com.partyvalvet.partyvalvet.imageloader.ImageLoader;
import com.partyvalvet.partyvalvet.instagram.ApplicationData;
import com.partyvalvet.partyvalvet.instagram.InstagramApp;
import com.partyvalvet.partyvalvet.instagram.InstagramApp.OAuthAuthenticationListener;

public class Fragment_Login extends Fragment implements OnClickListener {

	TextView  txt_ls_forgotpwd, txt_ls_login, txt_ls_or,
			txt_ls_signup, txt_ls_loginwith;
	EditText edt_ls_email, edt_ls_password;
	ImageView img_login_instagram;
	Font_Setting fs;
	ServiceHandler sh;
	String url,urlfb,urlinsta;
	ProgressDialog pDialog;
	SharedPreferences preflogin;
	Editor editor;
	ImageLoaderRec imageLoaderRec;
	public static InstagramApp mApp;
	
	private LoginButton loginBtn;
	private UiLifecycleHelper uiHelper;

	int flag = 0;
	
	private HashMap<String, String> userInfoHashmap = new HashMap<String, String>();
	private Handler handler = new Handler(new Callback() {

		@Override
		public boolean handleMessage(Message msg) {
			if (msg.what == InstagramApp.WHAT_FINALIZE) {
				userInfoHashmap = mApp.getUserInfo();
				if (userInfoHashmap!=null) {
					connectOrDisconnectUser();
				}
				//connectOrDisconnectUser();
			} else if (msg.what == InstagramApp.WHAT_FINALIZE) {
				Toast.makeText(getActivity(), getString(R.string.nointernet),
						Toast.LENGTH_SHORT).show();
			}
			return false;
		}
	});
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_login__screen, null);
		//MainActivity.screen="login";
		MainActivity.screen="createeventselection";
		
		uiHelper = new UiLifecycleHelper(getActivity(), statusCallback);
		uiHelper.onCreate(savedInstanceState);
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		imageLoaderRec = new ImageLoaderRec(getActivity());
		fs.hideheading();
		MainActivity.txt_main_title.setText(getString(R.string.headinglogin));
		//MainActivity.txt_main_title.setTextSize(15);
		preflogin = getActivity().getSharedPreferences("login",
				Context.MODE_PRIVATE);
		editor = preflogin.edit();
		
	
		bindid(view);
		if (Session.getActiveSession()!=null) {
			Session.setActiveSession(null);
		}
		loginBtn = (LoginButton)view.findViewById(R.id.fb_login_button);
		
		loginBtn.setFragment(this);
		loginBtn.setReadPermissions(Arrays.asList("email"));
		loginBtn.setBackgroundResource(R.drawable.fb);
		loginBtn.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
		
		
		
		
		loginBtn.setUserInfoChangedCallback(new UserInfoChangedCallback() {
			@Override
			public void onUserInfoFetched(GraphUser user) {
				if (user != null) {
					
					//txt_sign_in_msg.setText("You are currently logged in as " + user.getName());
					//Log.d("Uname", user.getName());
				
						
					
					String email,name,gender,fname,lname;
					email=user.asMap().get("email").toString();
					//fbemail=user.asMap().get("email").toString();
					name=user.getName();
					gender=user.asMap().get("gender").toString();
					fname = user.getFirstName();
					lname = user.getLastName();
					Log.d("facebook loging call autometically", email+"FACEBOOK FACEBOOK FACEBOOK");
					
					if (ConnectionDetector.isNetworkAvailable(getActivity())) {
						
						/*String android_id = Secure.getString(getContentResolver(),
				                Secure.ANDROID_ID);*/
						//Log.d("android device id ------>>>>>", android_id);
						//url = getString(R.string.url)+"login/index.php?email="+edt_si_userid.getText().toString().trim()+"&password="+edt_si_password.getText().toString().trim()+"&device_token="+deviceid;
					//	urlfb = getString(R.string.url)+"fb_login/index.php?user_type="+logintype+"&email="+email+"&first_name="+fname+"&last_name="+lname;
						urlfb= getString(R.string.url)+"fb_login/index.php?name="+fname+"&email="+email+"&gender="+gender;
						new fblogin().execute();
					}
					else
					{
						fs.toast(getString(R.string.nointernet));
					}
					}
					
					
				 else {
					//username.setText("You are not logged in.");
				}
			}
		});
		
		
		
		mApp = new InstagramApp(getActivity(), ApplicationData.CLIENT_ID,
				ApplicationData.CLIENT_SECRET, ApplicationData.CALLBACK_URL);
		mApp.resetAccessToken();
		mApp.setListener(new OAuthAuthenticationListener() {

			@Override
			public void onSuccess() {
				// tvSummary.setText("Connected as " + mApp.getUserName());
			//	btnConnect.setText("Disconnect");
				//llAfterLoginView.setVisibility(View.VISIBLE);
				// userInfoHashmap = mApp.
				mApp.fetchUserName(handler);
				
				/*FragmentManager fm = getActivity().getSupportFragmentManager();
		        FragmentTransaction ft = fm.beginTransaction();
		       
		        
		        android.support.v4.app.Fragment fragment = null;
				
					
				fragment = new Fragment_instagram_gallery();
			     Bundle args = new Bundle();
			        
			      args.putSerializable("userinfo", userInfoHashmap);
					fragment.setArguments(args);
				
			    //   ft.setCustomAnimations(R.anim.animation_leave,R.anim.animation_leave);
			       ft.replace(R.id.framl_main_screen, fragment);
			        ft.commit();*/
			        
				
			}

			@Override
			public void onFail(String error) {
				Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT)
						.show();
			}
		});
		
		if (mApp.hasAccessToken()) {
			// tvSummary.setText("Connected as " + mApp.getUserName());
			//btnConnect.setText("Disconnect");
			//llAfterLoginView.setVisibility(View.VISIBLE);
			mApp.fetchUserName(handler);
			//connectOrDisconnectUser();
			FragmentManager fm = getActivity().getSupportFragmentManager();
	        FragmentTransaction ft = fm.beginTransaction();
	       
	    /*    android.support.v4.app.Fragment fragment = null;
			
				
			fragment = new Fragment_instagram_gallery();
		     Bundle args = new Bundle();
		        
		      args.putSerializable("userinfo", userInfoHashmap);
				fragment.setArguments(args);
			
		    //   ft.setCustomAnimations(R.anim.animation_leave,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
*/
		}

		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		txt_ls_forgotpwd = (TextView) view.findViewById(R.id.txt_ls_forgotpwd);
		txt_ls_login = (TextView) view.findViewById(R.id.txt_ls_login);
		txt_ls_or = (TextView) view.findViewById(R.id.txt_ls_or);
		txt_ls_signup = (TextView) view.findViewById(R.id.txt_ls_signup);
		txt_ls_loginwith = (TextView) view.findViewById(R.id.txt_ls_loginwith);
		
		
		//img_login_facebook= (ImageView) view.findViewById(R.id.img_login_facebook);
		img_login_instagram= (ImageView) view.findViewById(R.id.img_login_instagram);

		//img_login_facebook.setOnClickListener(this);
		img_login_instagram.setOnClickListener(this);
		//fs.settypefacetextview(txt_ls_heading);
		fs.settypefacetextviewbellota(txt_ls_forgotpwd);
		fs.settypefacetextview(txt_ls_login);
		fs.settypefacetextviewbellota(txt_ls_or);
		fs.settypefacetextview(txt_ls_signup);
		fs.settypefacetextviewbellota(txt_ls_loginwith);

		txt_ls_forgotpwd.setOnClickListener(this);
		txt_ls_login.setOnClickListener(this);

		txt_ls_signup.setOnClickListener(this);

		edt_ls_email = (EditText) view.findViewById(R.id.edt_ls_email);
		edt_ls_password = (EditText) view.findViewById(R.id.edt_ls_password);

		edt_ls_email.setHintTextColor(getResources().getColor(
				R.color.whitecolor));
		edt_ls_password.setHintTextColor(getResources().getColor(
				R.color.whitecolor));

		fs.settypefaceedittextbellota(edt_ls_email);
		fs.settypefaceedittextbellota(edt_ls_password);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.txt_ls_login:

			if (!edt_ls_email.getText().toString().trim().equalsIgnoreCase("")
					&& !edt_ls_password.getText().toString().trim()
							.equalsIgnoreCase("")) {
				if (isValidEmail(edt_ls_email.getText().toString().trim())) {

					if (ConnectionDetector
							.isNetworkAvailable(getActivity())) {

						url = getString(R.string.url)
								+ "login/index.php?email="
								+ edt_ls_email.getText().toString().trim()
								+ "&password="
								+ edt_ls_password.getText().toString().trim();
						new login().execute();
					} else {
						fs.toast(getString(R.string.nointernet));
					}
				} else {
					fs.toast(getString(R.string.entervalidemail));
				}

			} else if (edt_ls_email.getText().toString().trim()
					.equalsIgnoreCase("")) {
				fs.toast(getString(R.string.enteremail));
			}

			else if (edt_ls_password.getText().toString().trim()
					.equalsIgnoreCase("")) {
				fs.toast(getString(R.string.enterpassword));
			}

			break;

		case R.id.txt_ls_signup:
			/*
			 * Intent intent = new Intent(getApplicationContext(),
			 * Sign_Up_Screen.class); startActivity(intent); finish();
			 */

			FragmentManager fm = getActivity().getSupportFragmentManager();
	        FragmentTransaction ft = fm.beginTransaction();
	       
	        Fragment fragment = null;
	 		fragment = new Fragment_Signup();
			
		       //ft.setCustomAnimations(R.anim.animation_leave,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;
		case R.id.txt_ls_forgotpwd:
			/*
			 * Intent intent = new Intent(getApplicationContext(), .class);
			 * startActivity(intent); finish();
			 */

			break;
		/*case R.id.img_login_facebook:
			
			 * Intent intent = new Intent(getApplicationContext(), .class);
			 * startActivity(intent); finish();
			 
			
			break;*/
		case R.id.img_login_instagram:
			/*
			 * Intent intent = new Intent(getApplicationContext(), .class);
			 * startActivity(intent); finish();
			 */

			connectOrDisconnectUser();
			break;
			
		}

	}
	public final static boolean isValidEmail(CharSequence target) {
		return !TextUtils.isEmpty(target)
				&& android.util.Patterns.EMAIL_ADDRESS.matcher(target)
						.matches();
	}
	
	class login extends AsyncTask<Void, Void, Void>
	{
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			url =url.replaceAll(" ", "%20");
			String jsonString = sh.makeServiceCall(url, ServiceHandler.GET);
			if (jsonString!=null) {
				
				try {
					JSONObject jobj = new JSONObject(jsonString);
					if (jobj.has("200")) {
						flag =1;
						
						JSONObject objdata = jobj.getJSONObject("200");
						editor.putString("id",objdata.getString("id"));
						editor.putString("name", objdata.getString("name"));
						editor.putString("phone", objdata.getString("phone"));
						editor.putString("email", objdata.getString("email"));
						editor.putString("dob", objdata.getString("dob"));
						editor.putString("state", objdata.getString("state"));
						editor.putString("gender", objdata.getString("gender"));
						editor.putString("image", objdata.getString("image"));
						
						editor.commit();
						
					}
					else if(jobj.has("400"))
					{
						flag =0;
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			if (flag==1) {
				fs.toast(getString(R.string.loginsucess));
				edt_ls_email.setText("");
				edt_ls_password.setText("");
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(
					      Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(edt_ls_password.getWindowToken(), 0);
					imm.hideSoftInputFromWindow(edt_ls_email.getWindowToken(), 0);
				/*Intent intent = new Intent(get, MainActivity.class);
				startActivity(intent);
				finish();*/
				MainActivity.txt_main_logout.setText("Log Out");
				MainActivity.txt_main_myprofile_mail.setVisibility(View.VISIBLE);
				MainActivity.txt_main_myprofile.setText(preflogin.getString("name", ""));
				if (!preflogin.getString("image", "").equalsIgnoreCase("")) {
					imageLoaderRec.DisplayImage(preflogin.getString("image", ""), MainActivity.img_main_profile_pic);	
				}
				
       		 MainActivity.txt_main_myprofile_mail.setText(preflogin.getString("email", ""));
				
				FragmentManager fm = getActivity().getSupportFragmentManager();
		        FragmentTransaction ft = fm.beginTransaction();
		       
		        Fragment fragment = null;
		        if (MainActivity.login.equalsIgnoreCase("invitationgrid")) {
		        	fragment = new Fragment_invitetion_grid();
				}
		        else
		        //	fragment = new Fragment_Home();
		        fragment = new Fragment_Select_Your_invitetion();
		 		//fragment = new Fragment_Create_event_selection();
				
			       //ft.setCustomAnimations(R.anim.animation_leave,R.anim.animation_leave);
			       ft.replace(R.id.framl_main_screen, fragment);
			        ft.commit();
			}
			else if(flag==0)
			{
				fs.toast(getString(R.string.emailpwdwrong));
				edt_ls_password.setText("");
			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage(getString(R.string.plzwait));
            pDialog.setCancelable(false);
            pDialog.show();
		}
		
	}
	private Session.StatusCallback statusCallback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			if (state.isOpened()) {
				Log.d("MainActivity", "Facebook session opened.");
			} else if (state.isClosed()) {
				Log.d("MainActivity", "Facebook session closed.");
			}
			
			
		}
	};
	
	@Override
	public void onResume() {
		super.onResume();
		uiHelper.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}



	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onSaveInstanceState(Bundle savedState) {
		super.onSaveInstanceState(savedState);
		uiHelper.onSaveInstanceState(savedState);
	}

	
	class fblogin extends AsyncTask<Void, Void, Void>
	{
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			urlfb = urlfb.replaceAll(" ", "%20");
			String jsonsString = sh.makeServiceCall(urlfb, ServiceHandler.GET);
			if (jsonsString!=null) {
				
				try {
					JSONObject jobj  = new JSONObject(jsonsString);
					if (jobj.has("200")) {
						
						JSONObject objdata= jobj.getJSONObject("200");
						editor.putString("id", objdata.getString("id"));
						editor.putString("name", objdata.getString("name"));
						editor.putString("email", objdata.getString("email"));
						editor.putString("gender", objdata.getString("gender"));
						editor.commit();
						
						
					}
					else if (jobj.has("400")) {

						JSONObject objdataalready= jobj.getJSONObject("400");
						editor.putString("id", objdataalready.getString("id"));
						editor.putString("name", objdataalready.getString("name"));
						editor.putString("email", objdataalready.getString("email"));
						editor.putString("gender", objdataalready.getString("gender"));
						editor.commit();
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			fs.toast(getString(R.string.loginsucess));
			edt_ls_email.setText("");
			edt_ls_password.setText("");
			/*Intent intent = new Intent(get, MainActivity.class);
			startActivity(intent);
			finish();*/
			MainActivity.txt_main_logout.setText("Log Out");
			MainActivity.txt_main_myprofile_mail.setVisibility(View.VISIBLE);
			MainActivity.txt_main_myprofile.setText(preflogin.getString("name", ""));
			
   		 MainActivity.txt_main_myprofile_mail.setText(preflogin.getString("email", ""));
			
			FragmentManager fm = getActivity().getSupportFragmentManager();
	        FragmentTransaction ft = fm.beginTransaction();
	       
	        Fragment fragment = null;
	 		fragment = new Fragment_Create_event_selection();
			
		       //ft.setCustomAnimations(R.anim.animation_leave,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage(getString(R.string.plzwait));
            pDialog.setCancelable(false);
            pDialog.show();
		}
			
		
	}

	class instalogin extends AsyncTask<Void, Void, Void>
	{
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			urlinsta = urlinsta.replaceAll(" ", "%20");
			String jsonsString = sh.makeServiceCall(urlinsta, ServiceHandler.GET);
			if (jsonsString!=null) {
				
				try {
					JSONObject jobj  = new JSONObject(jsonsString);
					if (jobj.has("200")) {
						
						JSONObject objdata= jobj.getJSONObject("200");
						editor.putString("id", objdata.getString("id"));
						editor.putString("name", objdata.getString("name"));
						editor.putString("email", "");
						
						editor.commit();
						
						
					}
					/*else if (jobj.has("400")) {

						JSONObject objdataalready= jobj.getJSONObject("400");
						editor.putString("id", objdataalready.getString("id"));
						editor.putString("name", objdataalready.getString("name"));
						editor.putString("email", objdataalready.getString("email"));
						editor.putString("gender", objdataalready.getString("gender"));
						editor.commit();
					}*/
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			fs.toast(getString(R.string.loginsucess));
			edt_ls_email.setText("");
			edt_ls_password.setText("");
			/*Intent intent = new Intent(get, MainActivity.class);
			startActivity(intent);
			finish();*/
			MainActivity.txt_main_logout.setText("Log Out");
			MainActivity.txt_main_myprofile_mail.setVisibility(View.VISIBLE);
			MainActivity.txt_main_myprofile.setText(preflogin.getString("name", ""));
			
   		 	MainActivity.txt_main_myprofile_mail.setText(preflogin.getString("email", ""));
			
			FragmentManager fm = getActivity().getSupportFragmentManager();
	        FragmentTransaction ft = fm.beginTransaction();
	       
	        Fragment fragment = null;
	 		fragment = new Fragment_Create_event_selection();
			
		       //ft.setCustomAnimations(R.anim.animation_leave,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
		        
		       
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage(getString(R.string.plzwait));
            pDialog.setCancelable(false);
            pDialog.show();
		}
			
		
	}
	
// instagram 
	
	private void connectOrDisconnectUser() {
		if (mApp.hasAccessToken()) {
			
			
			/*FragmentManager fm = getActivity().getSupportFragmentManager();
	        FragmentTransaction ft = fm.beginTransaction();
	       
	        android.support.v4.app.Fragment fragment = null;*/
	        //args.putSerializable("userinfo", userInfoHashmap);
	       String uid,uname;
	       
	       uid= userInfoHashmap.get("id");
	       uname= userInfoHashmap.get("username");

	       if (ConnectionDetector.isNetworkAvailable(getActivity())) {
				
				/*String android_id = Secure.getString(getContentResolver(),
		                Secure.ANDROID_ID);*/
				//Log.d("android device id ------>>>>>", android_id);
				//url = getString(R.string.url)+"login/index.php?email="+edt_si_userid.getText().toString().trim()+"&password="+edt_si_password.getText().toString().trim()+"&device_token="+deviceid;
			//	urlfb = getString(R.string.url)+"fb_login/index.php?user_type="+logintype+"&email="+email+"&first_name="+fname+"&last_name="+lname;
				urlinsta= getString(R.string.url)+"insta_login/index.php?name="+uname+"&insta_id="+uid;
				new instalogin().execute();
			}
			else
			{
				fs.toast(getString(R.string.nointernet));
			}
	       
	       
			//fragment = new Fragment_instagram_gallery();
		/*	fragment = new Fragment_instagram_gallery_new();
		     Bundle args = new Bundle();
		        
		      args.putSerializable("userinfo", userInfoHashmap);
				fragment.setArguments(args);
			
		    //   ft.setCustomAnimations(R.anim.animation_leave,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();*/
			/*final AlertDialog.Builder builder = new AlertDialog.Builder(
					getActivity());
			builder.setMessage("Disconnect from Instagram?")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									mApp.resetAccessToken();
								
									// btnConnect.setVisibility(View.VISIBLE);
								//	llAfterLoginView.setVisibility(View.GONE);
								//	btnConnect.setText("Connect");
									// tvSummary.setText("Not connected");
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
									
									FragmentManager fm = getActivity().getSupportFragmentManager();
							        FragmentTransaction ft = fm.beginTransaction();
							       
							        android.support.v4.app.Fragment fragment = null;
									
										
									fragment = new Fragment_instagram_gallery();
								     Bundle args = new Bundle();
								        
								      args.putSerializable("userinfo", userInfoHashmap);
										fragment.setArguments(args);
									
								    //   ft.setCustomAnimations(R.anim.animation_leave,R.anim.animation_leave);
								       ft.replace(R.id.framl_main_screen, fragment);
								        ft.commit();
								}
							});
			final AlertDialog alert = builder.create();
			alert.show();*/
		} else {
			mApp.authorize();
		}
	}
	  
}
