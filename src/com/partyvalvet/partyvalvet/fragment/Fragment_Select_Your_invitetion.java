package com.partyvalvet.partyvalvet.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;


public class Fragment_Select_Your_invitetion extends Fragment implements OnClickListener {

	Font_Setting fs;
	
	
	SharedPreferences preflogin;
	Editor editor;
	int login =0 ;
	LinearLayout lin_free,lin_premium,lin_all;
	TextView txt_free,txt_premium,txt_all;
	String usreid ,plan= "all";
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_select_your_invitetion, null);
		preflogin = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
		editor = preflogin.edit();
		//MainActivity.screen="selectinvitation";
		MainActivity.screen="home";
		MainActivity.txt_main_title.setText(getString(R.string.headingselectyourinvitation));
		
		fs = new Font_Setting(getActivity());
		fs.showheading();
		bindid(view);
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub

		
		txt_free=(TextView)view.findViewById(R.id.txt_free);
		txt_premium=(TextView)view.findViewById(R.id.txt_premium);
		txt_all=(TextView)view.findViewById(R.id.txt_all);
		
		
		lin_free=(LinearLayout)view.findViewById(R.id.lin_free);
		lin_premium=(LinearLayout)view.findViewById(R.id.lin_premium);
		lin_all=(LinearLayout)view.findViewById(R.id.lin_all);
		
		
		
		fs.settypefacetextviewbellota(txt_free);
		fs.settypefacetextviewbellota(txt_premium);
		fs.settypefacetextviewbellota(txt_all);
		
		
		lin_free.setOnClickListener(this);
		lin_premium.setOnClickListener(this);
		lin_all.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
       
        Fragment fragment = null;
		
	//	if (login==1) {
			
		
		switch (v.getId()) {
		case R.id.lin_free:
			plan="0";
			//fragment = new Fragment_invitetion_grid();
			//fragment = new Fragment_Create_event_selection();
			fragment = new Fragment_Free_Templates();
			
			ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;
		case R.id.lin_premium:
			plan="1";
			//fragment = new Fragment_invitetion_grid();
			//fragment = new Fragment_Create_event_selection();
			fragment = new Fragment_Free_Templates();
			ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		    ft.replace(R.id.framl_main_screen, fragment);
		    ft.commit();

			break;
		case R.id.lin_all:
			plan="all";
			//fragment = new Fragment_invitetion_grid();
			//fragment = new Fragment_Create_event_selection();
			fragment = new Fragment_Free_Templates();
			ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		    ft.replace(R.id.framl_main_screen, fragment);
		    ft.commit();

			break;
		

		default:
			break;
		}
		editor.putString("plan", plan);
		editor.commit();
	//	}
		/*else
		{
			
	 		fragment = new Fragment_Login();
			
		       //ft.setCustomAnimations(R.anim.animation_leave,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
		}*/
	}

}
