package com.partyvalvet.partyvalvet.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.customeAdepter.Grid_invitetion;
import com.partyvalvet.partyvalvet.customeAdepter.Grid_select_envelope;
import com.partyvalvet.partyvalvet.customeAdepter.Grid_select_myenvelope;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ConnectionDetector;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;



public class Fragment_invitetion_grid extends Fragment  implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	
	SharedPreferences preflogin;
	String userid,urlmyinvitation,url ,c_id,c_name,plan,planstr;
	
	GridView grid;
	//LinearLayout lin_select_invitation;
	//TextView txt_select_invitation;
//	Spinner spin_se_select;
	ArrayList<String> images ,price,myimages;
//	String[] spinvalue ={"All Invitation","My Invitation"};
	AsyncTask<Void, Void, Void> task;
	ProgressDialog pDialog;
	
	int flaglistinvitaiton=0 ,flaglistmyinvitaiton=0;;
 
   
	
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_invitetion_grid, null);
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		fs.showheading();
		images = new ArrayList<String>();
		price = new ArrayList<String>();
		myimages= new ArrayList<String>();
		preflogin = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
		if (preflogin!=null) {
			userid = preflogin.getString("id", "");
			c_id= preflogin.getString("c_id", "");
			plan = preflogin.getString("plan", "");
			c_name = preflogin.getString("c_name", "");
			if (plan.equalsIgnoreCase("0")) {
				planstr = "Free";
			}
			else if(plan.equalsIgnoreCase("1"))
			{
				planstr = "Premium";
			}
			else if(plan.equalsIgnoreCase("all"))
			{
				planstr = "All";
			}
		}
		MainActivity.screen ="invitationgrid";
		
		MainActivity.txt_main_title.setText(planstr+" - "+c_name+" "+getString(R.string.headinginvitation));
		bindid(view);
		
	/*	ArrayAdapter<String> adptyear = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_new, spinvalue);
		//adptyear.setDropDownViewResource(R.layout.spinner_item);
		spin_se_select.setAdapter(adptyear);*/
		if (ConnectionDetector
				.isNetworkAvailable(getActivity())) {

			urlmyinvitation = getString(R.string.url)+"select_my_card/index.php?user_id="+userid;
			//url = getString(R.string.url)+"select_envelope_liner/index.php?plan=all";
			
		task =	new getmyinvitation();
		task.execute();
		
		} else {
			fs.toast(getString(R.string.nointernet));
		}

	
	       
	              
	                grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	 
	                    @Override
	                    public void onItemClick(AdapterView<?> parent, View view,
	                                            int position, long id) {
	                    	FragmentManager fm = getActivity().getSupportFragmentManager();
	                        FragmentTransaction ft = fm.beginTransaction();
	                       
	                        Fragment fragment = null;
	                        
	                        //Toast.makeText(getActivity(), "You Clicked at " +web[+ position], Toast.LENGTH_SHORT).show();
	                    	if (!userid.equalsIgnoreCase("")) {
	                    		
		                        
	                    		fragment = new Fragment_invitation_selection();
		            			
		            			ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		            		       ft.replace(R.id.framl_main_screen, fragment);
		            		        ft.commit();
		 	
							}
	                    	else
	                    	{
	                    		
	                    		fragment = new Fragment_Login();
	                    		MainActivity.login ="invitationgrid";
		            			ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		            		       ft.replace(R.id.framl_main_screen, fragment);
		            		        ft.commit();
	                    	}
	                    	
	                    }
	                });
		
		
	             /*   spin_se_select.setOnItemSelectedListener(new OnItemSelectedListener() {

	            		@Override
	            		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
	            				long arg3) {
	            			// TODO Auto-generated method stub
	            			if (spin_se_select.getSelectedItem().toString().equalsIgnoreCase("My Invitation")) {
	            				
	            				Grid_invitetion adapter = new Grid_invitetion(getActivity(),imageId);
	            				
	            		        grid.setAdapter(adapter);
	            				Grid_select_myenvelope adapter = new Grid_select_myenvelope(getActivity(),myimages);
	            				
	            		        grid.setAdapter(adapter);
	            			}
	            			else
	            			{
	            				Grid_select_envelope adapter = new Grid_select_envelope(getActivity(),images, price);
	            				
	            		        grid.setAdapter(adapter);
	            			}

	            			if (spin_se_select.getSelectedItem().toString().equalsIgnoreCase("All Invitation")) {
	            				
		            				Grid_invitetion adapter = new Grid_invitetion(getActivity(),imageId);
		            				
		            		        grid.setAdapter(adapter);
	            				Grid_select_envelope adapter = new Grid_select_envelope(getActivity(),images, price);
	            				
	            		        grid.setAdapter(adapter);
		            			}
		            		else if (spin_se_select.getSelectedItem().toString().equalsIgnoreCase("My Invitation")) {
		            			
		            				
		            				Grid_select_myenvelope adapter = new Grid_select_myenvelope(getActivity(),myimages);
		            				
		            		        grid.setAdapter(adapter);
		            			}

	            			
	            		}

	            		@Override
	            		public void onNothingSelected(AdapterView<?> arg0) {
	            			// TODO Auto-generated method stub
	            			
	            		}
	            	});
	            	*/
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		 grid=(GridView)view.findViewById(R.id.grid);
		 
		 /*lin_select_invitation=(LinearLayout)view.findViewById(R.id.lin_select_invitation);
		 txt_select_invitation=(TextView)view.findViewById(R.id.txt_select_invitation);
		 txt_select_invitation.setText("Select Invitation From");
		 fs.settypefacetextviewbellota(txt_select_invitation);
		 spin_se_select=(Spinner)view.findViewById(R.id.spin_se_select);
		 
		 lin_select_invitation.setOnClickListener(this);
		 */
		 
	}

	/*@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}*/
	
	class getinvitation extends AsyncTask<Void, Void, Void>
	{
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			String jsonString = sh.makeServiceCall(url, ServiceHandler.GET);
			if (jsonString!=null) {
				try {
					JSONObject  jobj = new JSONObject(jsonString);
					if (jobj.has("200")) {
						
						JSONArray jary = jobj.getJSONArray("200");
						if (jary.length()>0) {
							flaglistinvitaiton=1;
							for (int i = 0; i < jary.length(); i++) {
								
								JSONObject objdata = jary.getJSONObject(i);
								images.add(objdata.getString("image"));
								price.add(objdata.getString("price"));
								
							}
						}
						else
						{
							flaglistinvitaiton=0;
						}
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			if (flaglistinvitaiton==1) {
			Grid_select_envelope adapter = new Grid_select_envelope(getActivity(),images, price);
	            				
	            		        grid.setAdapter(adapter);
			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage(getString(R.string.plzwait));
            pDialog.setCancelable(false);
            pDialog.show();
		}
		
	}

	class getmyinvitation extends AsyncTask<Void, Void, Void>
	{
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			String jsonString = sh.makeServiceCall(urlmyinvitation, ServiceHandler.GET);
			if (jsonString!=null) {
				try {
					JSONObject  jobj = new JSONObject(jsonString);
					if (jobj.has("200")) {
						
						JSONArray jary = jobj.getJSONArray("200");
						if (jary.length()>0) {
							flaglistmyinvitaiton=1;
							for (int i = 0; i < jary.length(); i++) {
								
								JSONObject objdata = jary.getJSONObject(i);
								myimages.add(objdata.getString("image"));
							//	price.add(objdata.getString("price"));
								
							}
						}
						else
						{
							flaglistmyinvitaiton=0;
						}
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			//pDialog.dismiss();
			/*if (flaglistmyinvitaiton==1) {
				Grid_select_myenvelope adapter = new Grid_select_myenvelope(getActivity(),myimages);
				
		        grid.setAdapter(adapter);
			}*/
			
			if (ConnectionDetector
					.isNetworkAvailable(getActivity())) {

				url = getString(R.string.url)+"select_card/index.php?c_id="+c_id+"&user_id="+userid+"&plan="+plan;
				//url = getString(R.string.url)+"select_envelope_liner/index.php?plan=all";
				
				new getinvitation().execute();
			} else {
				fs.toast(getString(R.string.nointernet));
			}

			
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			/*pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage(getString(R.string.plzwait));
            pDialog.setCancelable(false);
            pDialog.show();*/
		}
		
	}
	
	public void onDestroy() {
	    super.onDestroy();

	    if(task != null && task.getStatus() != AsyncTask.Status.FINISHED) {
	        task.cancel(true);
	    }
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
	/*	case R.id.lin_select_invitation:
			spin_se_select.performClick();
			
			break;*/

		default:
			break;
		}
	}


}
