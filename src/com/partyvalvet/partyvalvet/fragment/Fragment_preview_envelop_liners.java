package com.partyvalvet.partyvalvet.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_preview_envelop_liners extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	TextView txt_skip;
	
	
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_preview_envalogliners, null);
		MainActivity.screen = "previewenvelopliners";
		MainActivity.txt_main_title.setText(getString(R.string.headingpreview));
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		
		bindid(view);
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		txt_skip=(TextView)view.findViewById(R.id.txt_skip);		
		txt_skip.setOnClickListener(this);		
		fs.settypefacetextview(txt_skip);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
       
        Fragment fragment = null;
		
		switch (v.getId()) {
		case R.id.txt_skip:
			   fragment = new Fragment_preview_stamp();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		       ft.commit();
			break;
		

		default:
			break;
		}
		
		
		
	}

}
