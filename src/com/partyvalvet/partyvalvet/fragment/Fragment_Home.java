package com.partyvalvet.partyvalvet.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.fragment.RSVP.Fragment_My_RSVP;
import com.partyvalvet.partyvalvet.fragment.purchased_item.Fragment_My_Invitataion;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_Home extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	TextView txt_frag_home_newparty,txt_frag_home_myparties,txt_frag_home_myaddbook,txt_frag_home_myrsvp;
	SharedPreferences preflogin;
	int login =0 ;
	
	String usreid ;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_home, null);
		MainActivity.screen="home";
		MainActivity.txt_main_title.setText("");
		preflogin = getActivity().getSharedPreferences("login",
				Context.MODE_PRIVATE);
		
		MainActivity.login="home";
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		fs.showheading();
		//MainActivity.txt_main_title.setText(getString(R.string.headinghome));
		
		if (preflogin!=null) {
			
			usreid = preflogin.getString("id", "");
			if (!usreid.equalsIgnoreCase("")) {
				login=1;
			}
			else 
			{
				login=0;
			}
		}
		bindid(view);
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub

		txt_frag_home_newparty=(TextView)view.findViewById(R.id.txt_frag_home_newparty);
		txt_frag_home_myparties=(TextView)view.findViewById(R.id.txt_frag_home_myparties);
		txt_frag_home_myaddbook=(TextView)view.findViewById(R.id.txt_frag_home_myaddbook);
		txt_frag_home_myrsvp=(TextView)view.findViewById(R.id.txt_frag_home_myrsvp);
		
		
		fs.settypefacetextview(txt_frag_home_newparty);
		fs.settypefacetextview(txt_frag_home_myparties);
		fs.settypefacetextview(txt_frag_home_myaddbook);
		fs.settypefacetextview(txt_frag_home_myrsvp);
		
		txt_frag_home_newparty.setOnClickListener(this);
		txt_frag_home_myparties.setOnClickListener(this);
		txt_frag_home_myaddbook.setOnClickListener(this);
		txt_frag_home_myrsvp.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
       
        Fragment fragment = null;
		
			
		
		switch (v.getId()) {
		case R.id.txt_frag_home_newparty:
			//fragment = new Fragment_Create_event_selection();
			fragment = new Fragment_Select_Your_invitetion();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;
			
				
		case R.id.txt_frag_home_myparties:
			if (login==1) {
			fragment = new Fragment_Create_event_selection();
			
			ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			}
			
			else
			{
				
		 		fragment = new Fragment_Login();
				
			       //ft.setCustomAnimations(R.anim.animation_leave,R.anim.animation_leave);
			       ft.replace(R.id.framl_main_screen, fragment);
			        ft.commit();
			}
			break;
		case R.id.txt_frag_home_myaddbook:
			if (login==1) {
				
			
			fragment = new Fragment_Create_event_selection();
			
			ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			}
		        else
				{
					
			 		fragment = new Fragment_Login();
					
				       //ft.setCustomAnimations(R.anim.animation_leave,R.anim.animation_leave);
				       ft.replace(R.id.framl_main_screen, fragment);
				        ft.commit();
				}

			break;
		case R.id.txt_frag_home_myrsvp:
			if (login==1) {
				
			
			fragment = new Fragment_My_RSVP();
			
		       //ft.setCustomAnimations(R.anim.animation_leave,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			}
			else
			{
				
		 		fragment = new Fragment_Login();
				
			       //ft.setCustomAnimations(R.anim.animation_leave,R.anim.animation_leave);
			       ft.replace(R.id.framl_main_screen, fragment);
			        ft.commit();
			}
			break;

		default:
			break;
		}
		
		
	}

}
