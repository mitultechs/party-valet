package com.partyvalvet.partyvalvet.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_Free_Templates extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	
	ImageView img_ft_otherparties,img_ft_baby,img_ft_wedding,img_ft_holidays,img_ft_graduation,img_ft_birthday;
	
	SharedPreferences preflogin;
	Editor editor;

	String c_id ="1",c_name="";
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_free_templa, null);
		preflogin = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
		editor = preflogin.edit(); 
		
		//MainActivity.screen="freetemplates";
		MainActivity.screen="createeventselection";
		
		MainActivity.txt_main_title.setText(getString(R.string.headingfreetemplates));
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		
		bindid(view);
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		img_ft_otherparties=(ImageView)view.findViewById(R.id.img_ft_otherparties);
		img_ft_baby=(ImageView)view.findViewById(R.id.img_ft_baby);
		img_ft_wedding=(ImageView)view.findViewById(R.id.img_ft_wedding);
		img_ft_holidays=(ImageView)view.findViewById(R.id.img_ft_holidays);
		img_ft_graduation=(ImageView)view.findViewById(R.id.img_ft_graduation);
		img_ft_birthday=(ImageView)view.findViewById(R.id.img_ft_birthday);
		
		img_ft_otherparties.setOnClickListener(this);
		img_ft_baby.setOnClickListener(this);
		img_ft_wedding.setOnClickListener(this);
		img_ft_holidays.setOnClickListener(this);
		img_ft_graduation.setOnClickListener(this);
		img_ft_birthday.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
       
        Fragment fragment = null;
		
		switch (v.getId()) {
		
		case R.id.img_ft_otherparties:
			c_id ="6";
			c_name ="Other Parties";
			//fragment = new Fragment_Select_Your_invitetion();
			fragment = new Fragment_invitetion_grid();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;
		case R.id.img_ft_baby:
			c_id ="5";
			c_name ="Baby";
			//fragment = new Fragment_Select_Your_invitetion();
			fragment = new Fragment_invitetion_grid();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;
		case R.id.img_ft_wedding:
			c_id ="4";
			c_name ="Wedding";
			//fragment = new Fragment_Select_Your_invitetion();
			fragment = new Fragment_invitetion_grid();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;
		case R.id.img_ft_holidays:
			c_id ="3";
			c_name ="Holidays";
			//fragment = new Fragment_Select_Your_invitetion();
			fragment = new Fragment_invitetion_grid();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;
		case R.id.img_ft_graduation:
			c_id ="2";
			c_name ="Graduation";
			//fragment = new Fragment_Select_Your_invitetion();
			fragment = new Fragment_invitetion_grid();
			
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;
		case R.id.img_ft_birthday:
			c_id ="1";
			c_name ="Birthday";
			
			fragment = new Fragment_invitetion_grid();
			//fragment = new Fragment_Select_Your_invitetion();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;

		default:
			break;
		}
		editor.putString("c_id", c_id);
		editor.putString("c_name", c_name);
		editor.commit();
	}

}
