package com.partyvalvet.partyvalvet.fragment.store;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.fragment.Fragment_Select_Your_invitetion;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_Store_Templates extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	
	ImageView img_ft_otherparties,img_ft_baby,img_ft_wedding,img_ft_holidays,img_ft_graduation,img_ft_birthday;
	
	SharedPreferences preflogin;
	Editor editor;

	String c_id_store ="1";
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_free_templa, null);
		preflogin = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
		editor = preflogin.edit(); 
		
		MainActivity.screen="templates";
		MainActivity.txt_main_title.setText(getString(R.string.headingtemplates));
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		
		bindid(view);
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		img_ft_otherparties=(ImageView)view.findViewById(R.id.img_ft_otherparties);
		img_ft_baby=(ImageView)view.findViewById(R.id.img_ft_baby);
		img_ft_wedding=(ImageView)view.findViewById(R.id.img_ft_wedding);
		img_ft_holidays=(ImageView)view.findViewById(R.id.img_ft_holidays);
		img_ft_graduation=(ImageView)view.findViewById(R.id.img_ft_graduation);
		img_ft_birthday=(ImageView)view.findViewById(R.id.img_ft_birthday);
		
		img_ft_otherparties.setOnClickListener(this);
		img_ft_baby.setOnClickListener(this);
		img_ft_wedding.setOnClickListener(this);
		img_ft_holidays.setOnClickListener(this);
		img_ft_graduation.setOnClickListener(this);
		img_ft_birthday.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
       
        Fragment fragment = null;
		
		switch (v.getId()) {
		
		case R.id.img_ft_otherparties:
			c_id_store ="6";
			fragment = new Fragment_store_invitetion_grid();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;
		case R.id.img_ft_baby:
			c_id_store ="5";
			fragment = new Fragment_store_invitetion_grid();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;
		case R.id.img_ft_wedding:
			c_id_store ="4";
			fragment = new Fragment_store_invitetion_grid();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;
		case R.id.img_ft_holidays:
			c_id_store ="3";
			fragment = new Fragment_store_invitetion_grid();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;
		case R.id.img_ft_graduation:
			c_id_store ="2";
			fragment = new Fragment_store_invitetion_grid();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;
		case R.id.img_ft_birthday:
			c_id_store ="1";
			fragment = new Fragment_store_invitetion_grid();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;

		default:
			break;
		}
		editor.putString("c_id_store", c_id_store);
		editor.commit();
	}

}
