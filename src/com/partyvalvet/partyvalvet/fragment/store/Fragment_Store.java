package com.partyvalvet.partyvalvet.fragment.store;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.fragment.Fragment_Select_Your_invitetion;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_Store extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	
	LinearLayout lin_store_templates,lin_store_envelop,lin_store_envelopliners,lin_store_stamps;
	TextView txt_store_templates,txt_store_envelop,txt_store_envelopliners,txt_store_stamps;
	
	
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_store, null);
		MainActivity.screen ="login";
		MainActivity.txt_main_title.setText(getString(R.string.headingstore));
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		
		bindid(view);
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		lin_store_templates=(LinearLayout)view.findViewById(R.id.lin_store_templates);
		lin_store_stamps=(LinearLayout)view.findViewById(R.id.lin_store_stamps);
		lin_store_envelop=(LinearLayout)view.findViewById(R.id.lin_store_envelop);
		lin_store_envelopliners=(LinearLayout)view.findViewById(R.id.lin_store_envelopliners);
		
		lin_store_templates.setOnClickListener(this);
		lin_store_envelop.setOnClickListener(this);
		lin_store_envelopliners.setOnClickListener(this);
		lin_store_stamps.setOnClickListener(this);
		
		txt_store_templates=(TextView)view.findViewById(R.id.txt_store_templates);
		txt_store_envelop=(TextView)view.findViewById(R.id.txt_store_envelop);
		txt_store_envelopliners=(TextView)view.findViewById(R.id.txt_store_envelopliners);
		txt_store_stamps=(TextView)view.findViewById(R.id.txt_store_stamps);

		fs.settypefacetextviewbellota(txt_store_templates);
		fs.settypefacetextviewbellota(txt_store_envelop);
		fs.settypefacetextviewbellota(txt_store_envelopliners);
		fs.settypefacetextviewbellota(txt_store_stamps);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
       
        Fragment fragment = null;
		switch (v.getId()) {
		case R.id.lin_store_templates:
			fragment = new Fragment_Store_Templates();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;
		case R.id.lin_store_envelop:
			
			fragment = new Fragment_Store_Envelopes();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			
			break;
		case R.id.lin_store_envelopliners:
			fragment = new Fragment_Store_Envelopes_Liners();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
	
			break;
		case R.id.lin_store_stamps:
			fragment = new Fragment_Store_Stamps();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
	
			break;

		default:
			break;
		}
	}

}
