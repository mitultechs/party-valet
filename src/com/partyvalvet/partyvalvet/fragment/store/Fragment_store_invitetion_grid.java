package com.partyvalvet.partyvalvet.fragment.store;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.customeAdepter.Grid_select_envelope;
import com.partyvalvet.partyvalvet.customeAdepter.Grid_select_myenvelope;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ConnectionDetector;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;



public class Fragment_store_invitetion_grid extends Fragment  implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	
	SharedPreferences preflogin;
	String userid,urlmyinvitation,url ,c_id_store,plan;
	
	GridView grid;
	LinearLayout lin_spin_invitation;
	//TextView txt_select_invitation;
	//Spinner spin_se_select;
	ArrayList<String> images ,price,myimages;
	String[] spinvalue ={"My Invitation","Store"};
	AsyncTask<Void, Void, Void> task;
	
	int flaglistinvitaiton=0 ,flaglistmyinvitaiton=0;;
 
    int[] imageId = {
            R.drawable.rose,
            R.drawable.baby,
            R.drawable.graduation,
            R.drawable.happy,
            R.drawable.otherpartyes,
            R.drawable.holidays,
            R.drawable.tree,
            R.drawable.wedding,
            R.drawable.weding,
            R.drawable.rose,
            R.drawable.happy,
            R.drawable.holidays,
            R.drawable.wedding,
            R.drawable.baby,
            R.drawable.birthday
 
    };
	
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_invitetion_grid, null);
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		
		images = new ArrayList<String>();
		price = new ArrayList<String>();
		myimages= new ArrayList<String>();
		preflogin = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
		if (preflogin!=null) {
			userid = preflogin.getString("id", "");
			c_id_store= preflogin.getString("c_id_store", "");
			plan = preflogin.getString("plan", "");
		}
		MainActivity.screen ="storeinvitation";
		MainActivity.txt_main_title.setText(getString(R.string.headinginvitation));
		bindid(view);
		 lin_spin_invitation.setVisibility(View.GONE);
		
		if (ConnectionDetector
				.isNetworkAvailable(getActivity())) {

			//urlmyinvitation = getString(R.string.url)+"select_my_card/index.php?user_id="+userid;
			url = getString(R.string.url)+"select_card/index.php?c_id="+c_id_store+"&user_id="+userid+"&plan=1";
			//url = getString(R.string.url)+"select_envelope_liner/index.php?plan=all";
			
		task =	new getinvitation();
		task.execute();
		
		} else {
			fs.toast(getString(R.string.nointernet));
		}

	
	       
	              
	                grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	 
	                    @Override
	                    public void onItemClick(AdapterView<?> parent, View view,
	                                            int position, long id) {
	                        //Toast.makeText(getActivity(), "You Clicked at " +web[+ position], Toast.LENGTH_SHORT).show();
	                    /*	FragmentManager fm = getActivity().getSupportFragmentManager();
	                        FragmentTransaction ft = fm.beginTransaction();
	                       
	                        Fragment fragment = null;
	                        
	                        fragment = new Fragment_invitation_selection();
	            			
	            			ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
	            		       ft.replace(R.id.framl_main_screen, fragment);
	            		        ft.commit();
	 */
	                    }
	                });
		
		
	                
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		 grid=(GridView)view.findViewById(R.id.grid);
		 
		 lin_spin_invitation=(LinearLayout)view.findViewById(R.id.lin_spin_invitation);
		
		 
		
		 
	}

	/*@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}*/
	
	class getinvitation extends AsyncTask<Void, Void, Void>
	{
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			String jsonString = sh.makeServiceCall(url, ServiceHandler.GET);
			if (jsonString!=null) {
				try {
					JSONObject  jobj = new JSONObject(jsonString);
					if (jobj.has("200")) {
						
						JSONArray jary = jobj.getJSONArray("200");
						if (jary.length()>0) {
							flaglistinvitaiton=1;
							for (int i = 0; i < jary.length(); i++) {
								
								JSONObject objdata = jary.getJSONObject(i);
								images.add(objdata.getString("image"));
								price.add(objdata.getString("price"));
								
							}
						}
						else
						{
							flaglistinvitaiton=0;
						}
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
			if (flaglistinvitaiton==1) {
				Grid_select_envelope adapter = new Grid_select_envelope(getActivity(),images, price);
				
		        grid.setAdapter(adapter);
			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
		
	}

		
	public void onDestroy() {
	    super.onDestroy();

	    if(task != null && task.getStatus() != AsyncTask.Status.FINISHED) {
	        task.cancel(true);
	    }
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}

	

}
