package com.partyvalvet.partyvalvet.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.internal.pd;
import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.customeAdepter.Grid_select_envelope;
import com.partyvalvet.partyvalvet.customeAdepter.Grid_select_myenvelope;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ConnectionDetector;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_Select_Envelope_Liners extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	LinearLayout lin_se_select_envelope,lin_se_skip; 
	
	TextView txt_se_select_enevelopefrome,txt_se_next,txt_se_skip;

	ArrayList<String> images ,price,myimages;
	String url,urlmyenevlopliners;
	
	SharedPreferences preflogin;
	String userid,plan;
	int flaglistenevelopliners=0,flaglistmyenevelopliners=0;
	Spinner spin_se_select;
	ProgressDialog pDialog;
	AsyncTask<Void, Void, Void> task;
	GridView grid;
	String[] spinvalue ={"All Enevelope Liners","My Enevelope Liners"};
	  int[] imageId = {
	            R.drawable.triparll,
	            R.drawable.triparll,
	            R.drawable.triparll,
	            R.drawable.triparll,
	            R.drawable.triparll,
	            R.drawable.triparll,
	            R.drawable.triparll,
	            R.drawable.triparll,
	            R.drawable.triparll,
	            R.drawable.triparll,
	            R.drawable.triparll,
	            R.drawable.triparll,
	            R.drawable.triparll,
	            R.drawable.triparll,
	            R.drawable.triparll
	 
	    };
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_select_envelope, null);
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		images = new ArrayList<String>();
		price = new ArrayList<String>();
		myimages= new ArrayList<String>();
		preflogin = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
		if (preflogin!=null) {
			userid = preflogin.getString("id", "");
			plan = preflogin.getString("plan", "");
		}
		MainActivity.screen ="selectenvelopliners";
		MainActivity.txt_main_title.setText(getString(R.string.headingselectenvelopeliners));
		bindid(view);
		
		if (ConnectionDetector
				.isNetworkAvailable(getActivity())) {

			//url = getString(R.string.url)+"select_envelope/index.php?plan=all";
			urlmyenevlopliners = getString(R.string.url)+"select_my_envelope_liner/index.php?user_id="+userid;
			
			task =	new getmyenevelopsliners();
			task.execute();
		} else {
			fs.toast(getString(R.string.nointernet));
		}

		ArrayAdapter<String> adptyear = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_new, spinvalue);
		//adptyear.setDropDownViewResource(R.layout.spinner_item);
		spin_se_select.setAdapter(adptyear);
	/*	Grid_select_envelope adapter = new Grid_select_envelope(getActivity(),imageId);
		
        grid.setAdapter(adapter);*/
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //Toast.makeText(getActivity(), "You Clicked at " +web[+ position], Toast.LENGTH_SHORT).show();
            	/*FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
               
                Fragment fragment = null;
                
                fragment = new Fragment_invitetion_grid();
    			
    			ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
    		       ft.replace(R.id.framl_main_screen, fragment);
    		        ft.commit();*/

            }
        });
        
       spin_se_select.setOnItemSelectedListener(new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			if (spin_se_select.getSelectedItem().toString().equalsIgnoreCase("My Enevelope Liners")) {
				
				/*Grid_invitetion adapter = new Grid_invitetion(getActivity(),imageId);
				
		        grid.setAdapter(adapter);*/
				Grid_select_myenvelope adapter = new Grid_select_myenvelope(getActivity(),myimages);
				
		        grid.setAdapter(adapter);
			}
			else
			{
				/*Grid_select_envelope adapter = new Grid_select_envelope(getActivity(),imageId);
				
		        grid.setAdapter(adapter);*/
				Grid_select_envelope adapter = new Grid_select_envelope(getActivity(),images, price);
				
		        grid.setAdapter(adapter);
			}
				
			
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	});
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
				lin_se_select_envelope=(LinearLayout)view.findViewById(R.id.lin_se_select_envelope);
				 lin_se_skip =(LinearLayout)view.findViewById(R.id.lin_se_skip);
				
				 txt_se_select_enevelopefrome=(TextView)view.findViewById(R.id.txt_se_select_enevelopefrome);
					txt_se_next=(TextView)view.findViewById(R.id.txt_se_next);
					txt_se_skip=(TextView)view.findViewById(R.id.txt_se_skip);
					txt_se_next.setOnClickListener(this);
					txt_se_select_enevelopefrome.setText("Select Envelope Liners From");
					
				fs.settypefacetextviewbellota(txt_se_select_enevelopefrome);
				fs.settypefacetextview(txt_se_next);
				fs.settypefacetextview(txt_se_skip);
				
				
				lin_se_select_envelope.setOnClickListener(this);
				lin_se_skip.setOnClickListener(this);
				
				
				
				spin_se_select =(Spinner)view.findViewById(R.id.spin_se_select);
				
				grid=(GridView)view.findViewById(R.id.grid);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
       
        Fragment fragment = null;
		switch (v.getId()) {
		case R.id.lin_se_select_envelope:
			spin_se_select.performClick();
			break;
		case R.id.lin_se_skip:

			fragment = new Fragment_Select_Stamp();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;
			
		case R.id.txt_se_next:
			fragment = new Fragment_Select_Stamp();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			
			break;
		


		default:
			break;
		}
		 		
	}
	class getenevelopsliners extends AsyncTask<Void, Void, Void>
	{
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			String jsonString = sh.makeServiceCall(url, ServiceHandler.GET);
			if (jsonString!=null) {
				try {
					JSONObject  jobj = new JSONObject(jsonString);
					if (jobj.has("200")) {
						
						JSONArray jary = jobj.getJSONArray("200");
						if (jary.length()>0) {
							flaglistenevelopliners=1;
							for (int i = 0; i < jary.length(); i++) {
								
								JSONObject objdata = jary.getJSONObject(i);
								images.add(objdata.getString("image"));
								price.add(objdata.getString("price"));
								
							}
						}
						else
						{
							flaglistenevelopliners=0;
						}
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			if (flaglistenevelopliners==1) {
				Grid_select_envelope adapter = new Grid_select_envelope(getActivity(),images, price);
				
		        grid.setAdapter(adapter);
				
			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage(getString(R.string.plzwait));
            pDialog.setCancelable(false);
            pDialog.show();
		}
		
	}

	class getmyenevelopsliners extends AsyncTask<Void, Void, Void>
	{
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			String jsonString = sh.makeServiceCall(urlmyenevlopliners, ServiceHandler.GET);
			if (jsonString!=null) {
				try {
					JSONObject  jobj = new JSONObject(jsonString);
					if (jobj.has("200")) {
						
						JSONArray jary = jobj.getJSONArray("200");
						if (jary.length()>0) {
							flaglistmyenevelopliners=1;
							for (int i = 0; i < jary.length(); i++) {
								
								JSONObject objdata = jary.getJSONObject(i);
								myimages.add(objdata.getString("image"));
								//price.add(objdata.getString("price"));
								
							}
						}
						else
						{
							flaglistmyenevelopliners=0;
						}
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
			/*if (flaglistmyenevelopliners==1) {
				Grid_select_myenvelope adapter = new Grid_select_myenvelope(getActivity(),myimages);
				
		        grid.setAdapter(adapter);
				
			}*/
			if (ConnectionDetector
					.isNetworkAvailable(getActivity())) {

				//url = getString(R.string.url)+"select_envelope/index.php?plan=all";
				url = getString(R.string.url)+"select_envelope_liner/index.php?user_id="+userid+"&plan="+plan;
				
				new getenevelopsliners().execute();
			} else {
				fs.toast(getString(R.string.nointernet));
			}

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
		
	}
	public void onDestroy() {
	    super.onDestroy();

	    if(task != null && task.getStatus() != AsyncTask.Status.FINISHED) {
	        task.cancel(true);
	    }
	}

}
