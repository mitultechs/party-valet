package com.partyvalvet.partyvalvet.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.customeAdepter.Grid_invitetion;
import com.partyvalvet.partyvalvet.customeAdepter.Grid_invitetion_selection;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_invitation_selection extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	ImageView img_is_mainimage;
	GridView grid;
	SharedPreferences preflogin;
	Editor editor;
	 
    int[] imageId = {
            R.drawable.invi_one,
            R.drawable.invi_two,
            R.drawable.graduation,
            R.drawable.happy,
            R.drawable.otherpartyes,
            R.drawable.holidays,
            R.drawable.tree,
            R.drawable.wedding,
            R.drawable.weding,
            R.drawable.rose,
            R.drawable.happy,
            R.drawable.holidays,
            R.drawable.wedding,
            R.drawable.baby,
            R.drawable.birthday
 
    };
	
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fregment_invitation_selection, null);
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		MainActivity.screen="invitationselection";
		MainActivity.txt_main_title.setText(getString(R.string.headingselectyourinvitation));
		
		bindid(view);
		preflogin = getActivity().getSharedPreferences("login",
				Context.MODE_PRIVATE);
		editor = preflogin.edit();
		Grid_invitetion_selection adapter = new Grid_invitetion_selection(getActivity(),imageId);
		img_is_mainimage.setImageDrawable(getResources().getDrawable(R.drawable.weding));
	       
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
    //            Toast.makeText(getActivity(), "You Clicked at " +web[+ position], Toast.LENGTH_SHORT).show();
            	
            	String name = getResources().getResourceEntryName(imageId[position]);
            	
            	editor.putString("invi_name", name);
            	editor.commit();
            	FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
               
                Fragment fragment = null;
                
                fragment = new Fragment_Select_Envelope();
    			
    			ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
    		       ft.replace(R.id.framl_main_screen, fragment);
    		        ft.commit();

            }
        });
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		 grid=(GridView)view.findViewById(R.id.grid);
		 img_is_mainimage=(ImageView)view.findViewById(R.id.img_is_mainimage);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

}
