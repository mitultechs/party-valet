	package com.partyvalvet.partyvalvet.fragment;

import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ConnectionDetector;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_Signup extends Fragment implements OnClickListener {

	TextView txt_ss_male,txt_ss_female,txt_ss_signup,txt_ss_state;
	EditText edt_ss_name,edt_ss_phone,edt_ss_email,edt_ss_password,edt_ss_confpassword,edt_ss_bdate;
	LinearLayout lin_ss_bdate,lin_ss_state,lin_ss_male,lin_ss_female;
	Spinner spin_ss_state;
	ImageView img_ss_female,img_ss_male;
	
	
	Font_Setting fs;
	ServiceHandler sh;
	String url,gender ="male",urlsignup,msg ="";
	ProgressDialog pDialog ;
	SharedPreferences preflogin;
	Editor editor;
	
	AsyncTask< Void, Void, Void> task;
	ArrayList<String> liststate;
	
	int flag =0;
	
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_sign_up, null);
		MainActivity.screen="signup";
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		
		fs.hideheading();
		MainActivity.txt_main_title.setText(getString(R.string.sign_up));
		liststate = new ArrayList<String>();
		liststate.add("State");
		preflogin= getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
		editor = preflogin.edit();
		
		bindid(view);
		
		
		if(ConnectionDetector.isNetworkAvailable(getActivity()))
		{
		
		url = getString(R.string.url)+"state/";
		task =new getstate();
		
		task.execute();
		}
		else
		{
			fs.toast(getString(R.string.nointernet));
		}

		spin_ss_state.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				txt_ss_state.setText(spin_ss_state.getSelectedItem().toString());
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	
		
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		img_ss_female=(ImageView)view.findViewById(R.id.img_ss_female);
		img_ss_male=(ImageView)view.findViewById(R.id.img_ss_male);
		
		//txt_ss_heading=(TextView)findViewById(R.id.txt_ss_heading);
		txt_ss_male=(TextView)view.findViewById(R.id.txt_ss_male);
		txt_ss_female=(TextView)view.findViewById(R.id.txt_ss_female);
		txt_ss_signup=(TextView)view.findViewById(R.id.txt_ss_signup);
		
		txt_ss_state=(TextView)view.findViewById(R.id.txt_ss_state);
		
		//fs.settypefacetextview(txt_ss_heading);
		fs.settypefacetextviewbellota(txt_ss_male);
		fs.settypefacetextviewbellota(txt_ss_female);
		fs.settypefacetextviewbellota(txt_ss_signup);
		fs.settypefacetextviewbellota(txt_ss_state);
		
		txt_ss_signup.setOnClickListener(this);
		txt_ss_state.setOnClickListener(this);
		
		edt_ss_name=(EditText)view.findViewById(R.id.edt_ss_name);
		edt_ss_phone=(EditText)view.findViewById(R.id.edt_ss_phone);
		edt_ss_email=(EditText)view.findViewById(R.id.edt_ss_email);
		edt_ss_password=(EditText)view.findViewById(R.id.edt_ss_password);
		edt_ss_confpassword=(EditText)view.findViewById(R.id.edt_ss_confpassword);
		edt_ss_bdate=(EditText)view.findViewById(R.id.edt_ss_bdate);
		edt_ss_bdate.setOnClickListener(this);
		
		edt_ss_name.setHintTextColor(getResources().getColor(R.color.whitecolor));
		edt_ss_phone.setHintTextColor(getResources().getColor(R.color.whitecolor));
		edt_ss_email.setHintTextColor(getResources().getColor(R.color.whitecolor));
		edt_ss_password.setHintTextColor(getResources().getColor(R.color.whitecolor));
		edt_ss_confpassword.setHintTextColor(getResources().getColor(R.color.whitecolor));
		edt_ss_bdate.setHintTextColor(getResources().getColor(R.color.whitecolor));
		
		
		
		fs.settypefaceedittextbellota(edt_ss_name);
		fs.settypefaceedittextbellota(edt_ss_phone);
		fs.settypefaceedittextbellota(edt_ss_email);
		fs.settypefaceedittextbellota(edt_ss_password);
		fs.settypefaceedittextbellota(edt_ss_confpassword);
		fs.settypefaceedittextbellota(edt_ss_bdate);
		
		lin_ss_bdate=(LinearLayout)view.findViewById(R.id.lin_ss_bdate);
		lin_ss_state=(LinearLayout)view.findViewById(R.id.lin_ss_state);
		lin_ss_male=(LinearLayout)view.findViewById(R.id.lin_ss_male);
		lin_ss_female=(LinearLayout)view.findViewById(R.id.lin_ss_female);
		
		
		lin_ss_bdate.setOnClickListener(this);
		lin_ss_state.setOnClickListener(this);
		lin_ss_male.setOnClickListener(this);
		lin_ss_female.setOnClickListener(this);
		
		spin_ss_state=(Spinner)view.findViewById(R.id.spin_ss_state);
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.txt_ss_signup:
			
			if (!edt_ss_name.getText().toString().trim().equalsIgnoreCase("")&&!edt_ss_phone.getText().toString().trim().equalsIgnoreCase("")&&!edt_ss_email.getText().toString().trim().equalsIgnoreCase("")&&!edt_ss_password.getText().toString().trim().equalsIgnoreCase("")&&!edt_ss_confpassword.getText().toString().trim().equalsIgnoreCase("")&&!edt_ss_bdate.getText().toString().trim().equalsIgnoreCase("")&&!txt_ss_state.getText().toString().equalsIgnoreCase("state")&&!txt_ss_state.getText().toString().equalsIgnoreCase("")) {
				if(isValidEmail(edt_ss_email.getText().toString().trim()))
				{
					
					if (edt_ss_password.getText().toString().equalsIgnoreCase(edt_ss_confpassword.getText().toString())) {
						
					
					
					if(ConnectionDetector.isNetworkAvailable(getActivity()))
					{
					
					urlsignup = getString(R.string.url)+"signup/index.php?name="+edt_ss_name.getText().toString().trim()+"&email="+edt_ss_email.getText().toString().trim()+"&password="+edt_ss_confpassword.getText().toString()+"&dob="+edt_ss_bdate.getText().toString().trim()+"&gender="+gender+"&state="+txt_ss_state.getText().toString().trim()+"&phone="+edt_ss_phone.getText().toString().trim();
					new signup().execute();
					}
					else
					{
						fs.toast(getString(R.string.nointernet));
					}
					}
					else
					{
						fs.toast(getString(R.string.pwdnotmatch));
						
					}
					
				}
				else
				{
					fs.toast(getString(R.string.entervalidemail));
				}
				
			}
			else if (edt_ss_name.getText().toString().trim().equalsIgnoreCase("")) {
				fs.toast(getString(R.string.entername));	
			}
			else if (edt_ss_phone.getText().toString().trim().equalsIgnoreCase("")) {
				fs.toast(getString(R.string.enterphone));
			}
			else if (edt_ss_email.getText().toString().trim().equalsIgnoreCase("")) {
				fs.toast(getString(R.string.enteremail));
			}
			else if (edt_ss_password.getText().toString().trim().equalsIgnoreCase("")) {
				fs.toast(getString(R.string.enterpassword));
			}
			else if (edt_ss_confpassword.getText().toString().trim().equalsIgnoreCase("")) {
				fs.toast(getString(R.string.enterconfpassword));
			}
			
			else if (edt_ss_bdate.getText().toString().trim().equalsIgnoreCase("")) {
				fs.toast(getString(R.string.selectbdate));
			}
			else if (txt_ss_state.getText().toString().trim().equalsIgnoreCase("")) {
				fs.toast(getString(R.string.selectstate));
			}

			break;

		case R.id.lin_ss_bdate:
			Calendar c = Calendar.getInstance();
	        int mYear = c.get(Calendar.YEAR);
	        int mMonth = c.get(Calendar.MONTH);
	        int mDay = c.get(Calendar.DAY_OF_MONTH);
	        System.out.println("the selected " + mDay);
	        DatePickerDialog dialog = new DatePickerDialog(getActivity(),
	                new mDateSetListener(), mYear, mMonth, mDay);
	        
	        dialog.show();
			break;
		case R.id.edt_ss_bdate:
			Calendar c1 = Calendar.getInstance();
	        int mYear1 = c1.get(Calendar.YEAR);
	        int mMonth1 = c1.get(Calendar.MONTH);
	        int mDay1 = c1.get(Calendar.DAY_OF_MONTH);
	        System.out.println("the selected " + mDay1);
	        DatePickerDialog dialog1 = new DatePickerDialog(getActivity(),
	                new mDateSetListener(), mYear1, mMonth1, mDay1);
	        
	        dialog1.show();
			
			break;

		case R.id.lin_ss_male:
			
			img_ss_female.setImageDrawable(getResources().getDrawable(R.drawable.genderunselect));
			img_ss_male.setImageDrawable(getResources().getDrawable(R.drawable.genderselect));
				
				gender = "male";
			break;

		case R.id.lin_ss_female:
			
			img_ss_male.setImageDrawable(getResources().getDrawable(R.drawable.genderunselect));
			img_ss_female.setImageDrawable(getResources().getDrawable(R.drawable.genderselect));
			
			gender ="female";
			break;
		case R.id.lin_ss_state:
			spin_ss_state.performClick();
			break;
		case R.id.txt_ss_state:
			spin_ss_state.performClick();
			break;
		default:
			
			break;
		}
	}
	public final static boolean isValidEmail(CharSequence target) {
		return !TextUtils.isEmpty(target)
				&& android.util.Patterns.EMAIL_ADDRESS.matcher(target)
						.matches();
	}
	
	class mDateSetListener implements DatePickerDialog.OnDateSetListener {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                int dayOfMonth) {
            // TODO Auto-generated method stub
            // getCalender();
            int mYear = year;
            int mMonth = monthOfYear;
            int mDay = dayOfMonth;
            Calendar c = Calendar.getInstance();
	        int curYear = c.get(Calendar.YEAR);
            if (mYear>=curYear) {
				Toast.makeText(getActivity(), "Please check your birthdate", Toast.LENGTH_SHORT).show();
				edt_ss_bdate.setText("");
			}
            
            else if(mYear<=curYear-10)
            {
            	edt_ss_bdate.setText(new StringBuilder()
                    // Month is 0 based so add 1
            	
            	.append(mYear).append("-").append(mMonth + 1).append("-").append(mDay).append(" ")
                    );
            }
            else 
            {
            	Toast.makeText(getActivity(), "Please check your birthdate", Toast.LENGTH_SHORT).show();
            	edt_ss_bdate.setText("");
            }
            //System.out.println(v.getText().toString());


        }
    }
	
	class getstate extends AsyncTask<Void, Void, Void>
	{

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
		
			
			String jsonString = sh.makeServiceCall(url, ServiceHandler.GET);
			if (jsonString!=null) {
				
				try {
					JSONObject jobj = new JSONObject(jsonString);
					if (jobj.has("200")) {
						//flag =1;
						
						JSONArray arystate = jobj.getJSONArray("200");
						for (int i = 0; i < arystate.length(); i++) {
							
							JSONObject objdata = arystate.getJSONObject(i);
							liststate.add(objdata.getString("state"));
						}
						
					}
					else if(jobj.has("400"))
					{
						//flag =0;
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			ArrayAdapter<String> adptyear = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_new, liststate);
			//adptyear.setDropDownViewResource(R.layout.spinner_item);
			spin_ss_state.setAdapter(adptyear);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
		
	}
	
	class signup extends AsyncTask<Void, Void, Void>
	{
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			urlsignup = urlsignup.replaceAll(" ", "%20");
			String jsonString = sh.makeServiceCall(urlsignup, ServiceHandler.GET);
			if (jsonString!=null) {
				
				try {
					JSONObject jobj = new JSONObject(jsonString);
					if (jobj.has("200")) {
						flag =1;
						
						JSONObject objdata = jobj.getJSONObject("200").getJSONObject("user");
						editor.putString("id",objdata.getString("id"));
						editor.putString("name", objdata.getString("name"));
						editor.putString("phone", objdata.getString("phone"));
						editor.putString("email", objdata.getString("email"));
						editor.putString("dob", objdata.getString("dob"));
						editor.putString("state", objdata.getString("state"));
						editor.putString("gender", objdata.getString("gender"));
						editor.commit();
						
					}
					else if(jobj.has("400"))
					{
						msg ="";
						flag =0;
						if (!jobj.getString("400").toString().equalsIgnoreCase("0")) {
							msg = jobj.getString("400").toString();
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			if (flag==1) {
				fs.toast(getString(R.string.signupsucess));
				/*edt_ls_email.setText("");
				edt_ls_password.setText("");*/
				
					
				FragmentManager fm = getActivity().getSupportFragmentManager();
		        FragmentTransaction ft = fm.beginTransaction();
		       
		        Fragment fragment = null;
		 		fragment = new Fragment_Login();
				
			       //ft.setCustomAnimations(R.anim.animation_leave,R.anim.animation_leave);
			       ft.replace(R.id.framl_main_screen, fragment);
			        ft.commit();
			}
			else if(flag==0)
			{
				if (!msg.equalsIgnoreCase("")) {
					fs.toast(msg);
				}
				else
				{
					fs.toast(getString(R.string.somethingwrong));	
				}
				
				//edt_ls_password.setText("");
			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage(getString(R.string.plzwait));
            pDialog.setCancelable(false);
            pDialog.show();
		}
		
	}
	public void onDestroy() {
	    super.onDestroy();

	    if(task != null && task.getStatus() != AsyncTask.Status.FINISHED) {
	        task.cancel(true);
	    }
	}
	
	
	

}
