package com.partyvalvet.partyvalvet.fragment;

import java.io.File;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_preview_invitetion extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	
	TextView txt_skip,txt_view_map;
	LinearLayout lin_view_map;
	SharedPreferences preflogin;
	ImageView img_invitetion_privew;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_preview_invitetion, null);
		MainActivity.screen = "previewinvitation";
		MainActivity.txt_main_title.setText(getString(R.string.headingpreview));
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		bindid(view);
		preflogin = getActivity().getSharedPreferences("login",
				Context.MODE_PRIVATE);
		
		  String image_name = preflogin.getString("image_name", "");
		
		
		  File f = new File(image_name);
		  
		  Bitmap bmp = BitmapFactory.decodeFile(f.getAbsolutePath());
		  img_invitetion_privew.setImageBitmap(bmp);
		
		
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		txt_skip=(TextView)view.findViewById(R.id.txt_skip);	
		txt_view_map=(TextView)view.findViewById(R.id.txt_skip);
		lin_view_map=(LinearLayout)view.findViewById(R.id.lin_view_map);
		
		img_invitetion_privew=(ImageView)view.findViewById(R.id.img_invitetion_privew);
		
		txt_skip.setOnClickListener(this);		
		lin_view_map.setOnClickListener(this);
		
		fs.settypefacetextview(txt_skip);
		fs.settypefacetextview(txt_view_map);
		
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
       
        Fragment fragment = null;
		
		switch (v.getId()) {
		case R.id.txt_skip:
			   fragment = new Fragment_send_invitetion();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		       ft.commit();
			break;
			
			
		case R.id.lin_view_map:
			   fragment = new Fragment_view_map();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		       ft.commit();
			break;
		

		default:
			break;
		}
	}

}
