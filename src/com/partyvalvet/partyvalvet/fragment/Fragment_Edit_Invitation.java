package com.partyvalvet.partyvalvet.fragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_Edit_Invitation extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	//ImageView img_ei_image_main;
	ImageView img_ei_setting,img_ei_font_alignment_left,img_ei_font_alignment_center,img_ei_font_alignment_right,img_right,img_left;
	//ScrollView scrollView ;
	SharedPreferences preflogin;
	Editor editor;
	int selectvalue=1;
	TextView txt_ei_next,txt_name,txt_date,txt_time,txt_venue_name,txt_street_name,txt_state_name,textheading1,textheading2;
	//txt_name
//	DTextView txt_name; 
	LinearLayout lin_main,lindialog_editinvitatioln,lin_ei_font_alignment,lin_drag,lin_drag_text,lin_edit;
	//,lin_drag_text
//	ViewGroup lin_drag;
	TextView txt_edit_invitation_text1,txt_edit_invitation_text2,txt_edit_invitation_text3,txt_edit_invitation_text4;
	TextView txt_font_bolleta,txt_font_alexbrush,txt_font_blackjack,txt_font_england;
	TextView txt_ei_color_black,txt_ei_color_white,txt_ei_color_pink,txt_ei_color_teal,txt_ei_color_blue_in,txt_ei_color_brown,	txt_ei_color_purple,	txt_ei_color_orange,	txt_ei_color_red,	txt_ei_color_beige,	txt_ei_color_light_pink,	txt_ei_color_blue,txt_ei_color_light_purple,	txt_ei_color_light_green,	txt_ei_color_light_yellow;
	LinearLayout layout;
	
	HorizontalScrollView hsv;
	SeekBar seek_font;
	int set =0;
	FrameLayout frm_seek_font;
	int [] mParams ;
	String LOGCAT;
	
	

	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_edit_invitation, null);
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		MainActivity.screen ="editinvitation";
		MainActivity.txt_main_title.setText(getString(R.string.headingeditinvitation));
		bindid(view);
		preflogin = getActivity().getSharedPreferences("login",
				Context.MODE_PRIVATE);
		editor = preflogin.edit();
		  String strPref = preflogin.getString("invi_name", "");
		
		LayoutInflater inflater1;
		inflater1 = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);              		
		if (strPref.equals("invi_one"))				
		{
			 layout = (LinearLayout) inflater.inflate(R.layout.invitetion_one ,
	                null);					
			    txt_name = (TextView)layout.findViewById(R.id.txt_name);
				txt_date= (TextView)layout.findViewById(R.id.txt_date);
				txt_time= (TextView)layout.findViewById(R.id.txt_time);
				txt_venue_name= (TextView)layout.findViewById(R.id.txt_venue_name);
				txt_street_name= (TextView)layout.findViewById(R.id.txt_street_name);
				txt_state_name= (TextView)layout.findViewById(R.id.txt_state_name);
				lin_drag=(LinearLayout)layout.findViewById(R.id.lin_drag);
				lin_drag_text=(LinearLayout)layout.findViewById(R.id.lin_drag_text);
				
/*				 int optionId = R.id.lin_drag;

				    View C =txt_name;
				    ViewGroup parent = (ViewGroup) C.getParent();
				    int index = parent.indexOfChild(C);
				    parent.removeView(C);
				    C = getActivity().getLayoutInflater().inflate(optionId, parent, true);
				    parent.addView(C, index);*/
				
				
				
				txt_name.setOnClickListener(this);
				txt_date.setOnClickListener(this);
				txt_time.setOnClickListener(this);
				txt_venue_name.setOnClickListener(this);
				txt_street_name.setOnClickListener(this);
				txt_state_name.setOnClickListener(this);
				
				
				
							 
			 textheading1= (TextView)layout.findViewById(R.id.textheading1);
				textheading2= (TextView)layout.findViewById(R.id.textheading2);
				fs.settypefacetextviewPRISTINA(textheading1);
				fs.settypefacetextviewPRISTINA(textheading2);
				
				fs.settypefacetextviewPRISTINA(txt_name);
				fs.settypefacetextviewPRISTINA(txt_date);
				fs.settypefacetextviewPRISTINA(txt_time);
				fs.settypefacetextviewPRISTINA(txt_venue_name);
				fs.settypefacetextviewPRISTINA(txt_street_name);
				fs.settypefacetextviewPRISTINA(txt_state_name);
	}
		else if (strPref.toString().trim()
				.equalsIgnoreCase("invi_two")) {
			 layout = (LinearLayout) inflater.inflate(R.layout.invitetion_two ,
	                null);	
			 
			 txt_name = (TextView)layout.findViewById(R.id.txt_name);
				txt_date= (TextView)layout.findViewById(R.id.txt_date);
				txt_time= (TextView)layout.findViewById(R.id.txt_time);
				txt_venue_name= (TextView)layout.findViewById(R.id.txt_venue_name);
				txt_street_name= (TextView)layout.findViewById(R.id.txt_street_name);
				txt_state_name= (TextView)layout.findViewById(R.id.txt_state_name);
				
				txt_name.setOnClickListener(this);
				txt_date.setOnClickListener(this);
				txt_time.setOnClickListener(this);
				txt_venue_name.setOnClickListener(this);
				txt_street_name.setOnClickListener(this);
				txt_state_name.setOnClickListener(this);
				
			   fs.settypefacetextviewITCKRIST(txt_name);
				fs.settypefacetextviewITCKRIST(txt_date);
				fs.settypefacetextviewITCKRIST(txt_time);
				fs.settypefacetextviewITCKRIST(txt_venue_name);
				fs.settypefacetextviewITCKRIST(txt_street_name);
				fs.settypefacetextviewITCKRIST(txt_state_name);
			}
		else {
			 layout = (LinearLayout) inflater.inflate(R.layout.invitetion_one ,
	                null);	
			    txt_name = (TextView)layout.findViewById(R.id.txt_name);
				txt_date= (TextView)layout.findViewById(R.id.txt_date);
				txt_time= (TextView)layout.findViewById(R.id.txt_time);
				txt_venue_name= (TextView)layout.findViewById(R.id.txt_venue_name);
				txt_street_name= (TextView)layout.findViewById(R.id.txt_street_name);
				txt_state_name= (TextView)layout.findViewById(R.id.txt_state_name);
				lin_drag=(LinearLayout)layout.findViewById(R.id.lin_drag);
				
				
				txt_name.setOnClickListener(this);
				txt_date.setOnClickListener(this);
				txt_time.setOnClickListener(this);
				txt_venue_name.setOnClickListener(this);
				txt_street_name.setOnClickListener(this);
				txt_state_name.setOnClickListener(this);
			 			 
			 textheading1= (TextView)layout.findViewById(R.id.textheading1);
				textheading2= (TextView)layout.findViewById(R.id.textheading2);
				fs.settypefacetextviewPRISTINA(textheading1);
				fs.settypefacetextviewPRISTINA(textheading2);
							
				fs.settypefacetextviewPRISTINA(txt_name);
				fs.settypefacetextviewPRISTINA(txt_date);
				fs.settypefacetextviewPRISTINA(txt_time);
				fs.settypefacetextviewPRISTINA(txt_venue_name);
				fs.settypefacetextviewPRISTINA(txt_street_name);
				fs.settypefacetextviewPRISTINA(txt_state_name);				
		}
		
		/*LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.invitetion_one ,
		                null);*/			
		lin_main.addView(layout);
		//scrollView.addView(layout);		
		seek_font.setProgress((int) txt_name.getTextSize());		
		txt_name.setTextSize(TypedValue.COMPLEX_UNIT_PX,seek_font.getProgress());
		
		seek_font.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub				
			}			
			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub				
			}			
			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				// TODO Auto-generated method stub
			
				if(selectvalue==1)
				{				
				txt_name.setTextSize(TypedValue.COMPLEX_UNIT_PX,arg1);
				}
				else if(selectvalue==2)
				{	
				txt_date.setTextSize(TypedValue.COMPLEX_UNIT_PX,arg1);
				}
				else if(selectvalue==3)
				{
				txt_time.setTextSize(TypedValue.COMPLEX_UNIT_PX,arg1);
				}
				else if(selectvalue==4)
				{
				txt_venue_name.setTextSize(TypedValue.COMPLEX_UNIT_PX,arg1);
				txt_street_name.setTextSize(TypedValue.COMPLEX_UNIT_PX,arg1);
				txt_state_name.setTextSize(TypedValue.COMPLEX_UNIT_PX,arg1);
				}
			}
		});	
		
		
		
		
		
		
	/*	txt_name.setOnTouchListener(new OnTouchListener() {

	    
			@Override
			public boolean onTouch(View v,MotionEvent event) {
				// TODO Auto-generated method stub
				 drag(event, v);
				return false;
			}
		});
*/		
		//txt_name.bringToFront();
		
		txt_name.setOnTouchListener(new View.OnTouchListener() {
            int initialX = 0;
            int initialY = 0;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
            //	v.bringToFront(); 
            //	v.invalidate();
            	
            	
            	/*lin_drag_text.bringToFront();
            	lin_drag_text.invalidate();
            	 
            	lin.bringToFront(); 
            	v.invalidate();
            	*/
            	/*Display display = getWindowManager().getDefaultDisplay();
            	LinearLayout layout= (LinearLayout) findViewById(R.id.left);
            	int width=display.getWidth();
            	int height=display.getHeight();
            	LinearLayout.LayoutParams parms = new inearLayout.LayoutParams(width,height);
            	layout.setLayoutParams(parms);
            	
            	*/
            	
            	/*LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width,height);
            	lin_drag_text.setLayoutParams(parms);
            	
            	Display display = getActivity().getWindowManager().getDefaultDisplay();
            	@SuppressWarnings("deprecation")
				int width=display.getWidth();
            
				@SuppressWarnings("deprecation")
				int height=display.getHeight();
            	//LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width,height);
               // lin_drag_text.setLayoutParams(lp);
                lin_drag_text.bringToFront(); 
               // lin_drag_text.requestLayout();
                
            */    
              //  lin_drag.removeAllViewsInLayout();
                
            	
            //    lin_drag_text.bringToFront(); 
            //    lin_drag_text.invalidate();
              //  lin_drag_text.requestLayout();
                
               // lin_drag_text.bringToFront(); 
               // lin_drag_text.invalidate();
            	
            	

            	
            	
                
                switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                    initialX = (int) event.getX();
                    initialY = (int) event.getY();
                   
                	
                    
                	break;
                case MotionEvent.ACTION_MOVE:
                            int currentX = (int) event.getX();
                            int currentY = (int) event.getY();
                            
                            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) v.getLayoutParams(); 

                         //   View view1 = (View) event.getLocalState();
                           /* ViewGroup owner = (ViewGroup) v.getParent();
                            owner.removeView(v);
                            LinearLayout container = (LinearLayout) lin_drag;
                            container.addView(v);
                            v.setVisibility(View.VISIBLE);
                            v.invalidate();
                            v.requestLayout();*/
                          
                          //LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) txt_name.getLayoutParams();
                        
                          
                          
                     //     lp=(LinearLayout.LayoutParams)v.getLayoutParams();
                          
                          
                         //   LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
                           

                         //   LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width,height);
                            //lin_drag.setLayoutParams(lp);
                            
                            
                           // lp=(LinearLayout.LayoutParams) v.getLayoutParams();
                            
                      /*      LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
                            txt_name.setLayoutParams(lp);
                      */      
                          
                             
                     
                            
                            
                            
                            
                            
                            
                            
                            int left = lp.leftMargin + (currentX - initialX);
                            int top = lp.topMargin + (currentY - initialY);
                            int right = lp.rightMargin - (currentX - initialX);
                            int bottom = lp.bottomMargin - (currentY - initialY);

                            lp.rightMargin = right;
                            lp.leftMargin = left;
                            lp.bottomMargin = bottom;
                            lp.topMargin = top;

                            txt_name.setLayoutParams(lp);
                    break;
                default:
                    break;
                }
        
                return true;
            }
        });
		
		
		
		
		/********************************************/
		
		    
	    /*************************************************/
			
		
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		//img_ei_image_main =(ImageView)view.findViewById(R.id.img_ei_image_main);
		txt_ei_next=(TextView)view.findViewById(R.id.txt_ei_next);
		
		fs.settypefacetextview(txt_ei_next);
		txt_ei_next.setOnClickListener(this);
		lin_main=(LinearLayout)view.findViewById(R.id.lin_main);
		lindialog_editinvitatioln =(LinearLayout)view.findViewById(R.id.lindialog_editinvitatioln);
		
		txt_edit_invitation_text1=(TextView)view.findViewById(R.id.txt_edit_invitation_text1);
		txt_edit_invitation_text2=(TextView)view.findViewById(R.id.txt_edit_invitation_text2);
		txt_edit_invitation_text3=(TextView)view.findViewById(R.id.txt_edit_invitation_text3);
		txt_edit_invitation_text4=(TextView)view.findViewById(R.id.txt_edit_invitation_text4);
		
		
		txt_font_bolleta=(TextView)view.findViewById(R.id.txt_font_bolleta);
		txt_font_alexbrush=(TextView)view.findViewById(R.id.txt_font_alexbrush);
		txt_font_blackjack=(TextView)view.findViewById(R.id.txt_font_blackjack);
		txt_font_england=(TextView)view.findViewById(R.id.txt_font_england);
		
		fs.settypefacetextviewbellota(txt_font_bolleta);	
		fs.settypefacetextviewalexbrush(txt_font_alexbrush);
		fs.settypefacetextviewblackjack(txt_font_blackjack);
		fs.settypefacetextviewengland(txt_font_england);
		
		
		txt_ei_color_black=(TextView)view.findViewById(R.id.txt_ei_color_black);
		txt_ei_color_white=(TextView)view.findViewById(R.id.txt_ei_color_white);
		txt_ei_color_pink=(TextView)view.findViewById(R.id.txt_ei_color_pink);
		txt_ei_color_teal=(TextView)view.findViewById(R.id.txt_ei_color_teal);
		txt_ei_color_blue_in=(TextView)view.findViewById(R.id.txt_ei_color_blue_in);
		txt_ei_color_brown=(TextView)view.findViewById(R.id.txt_ei_color_brown);
		txt_ei_color_purple=(TextView)view.findViewById(R.id.txt_ei_color_purple);
		txt_ei_color_orange=(TextView)view.findViewById(R.id.txt_ei_color_orange);
		txt_ei_color_red=(TextView)view.findViewById(R.id.txt_ei_color_red);
		txt_ei_color_beige=(TextView)view.findViewById(R.id.txt_ei_color_beige);
		txt_ei_color_light_pink=(TextView)view.findViewById(R.id.txt_ei_color_light_pink);
		txt_ei_color_blue=(TextView)view.findViewById(R.id.txt_ei_color_blue);
		txt_ei_color_light_purple=(TextView)view.findViewById(R.id.txt_ei_color_light_purple);
		txt_ei_color_light_green=(TextView)view.findViewById(R.id.txt_ei_color_light_green);
		txt_ei_color_light_yellow=(TextView)view.findViewById(R.id.txt_ei_color_light_yellow);
		
		
		frm_seek_font=(FrameLayout)view.findViewById(R.id.frm_seek_font);
		
		
		lin_edit=(LinearLayout)view.findViewById(R.id.lin_edit);
		lin_ei_font_alignment=(LinearLayout)view.findViewById(R.id.lin_ei_font_alignment);
		
		img_ei_font_alignment_left=(ImageView)view.findViewById(R.id.img_ei_font_alignment_left);
		img_ei_font_alignment_center=(ImageView)view.findViewById(R.id.img_ei_font_alignment_center);
		img_ei_font_alignment_right=(ImageView)view.findViewById(R.id.img_ei_font_alignment_right);
		
		img_ei_setting=(ImageView)view.findViewById(R.id.img_ei_setting);
		img_right=(ImageView)view.findViewById(R.id.img_right);
		img_left=(ImageView)view.findViewById(R.id.img_left);
		
		seek_font=(SeekBar)view.findViewById(R.id.seek_font);
		
		hsv=(HorizontalScrollView)view.findViewById(R.id.hsv);
		
		img_ei_font_alignment_left.setOnClickListener(this);
		img_ei_font_alignment_center.setOnClickListener(this);
		img_ei_font_alignment_right.setOnClickListener(this);		
		img_ei_setting.setOnClickListener(this);		
		img_right.setOnClickListener(this);
		img_left.setOnClickListener(this);
		
		txt_ei_color_black.setOnClickListener(this);
		txt_ei_color_white.setOnClickListener(this);
		txt_ei_color_pink.setOnClickListener(this);
		txt_ei_color_teal.setOnClickListener(this);
		txt_ei_color_blue_in.setOnClickListener(this);
		txt_ei_color_brown.setOnClickListener(this);
		txt_ei_color_purple.setOnClickListener(this);
		txt_ei_color_orange.setOnClickListener(this);
		txt_ei_color_red.setOnClickListener(this);
		txt_ei_color_beige.setOnClickListener(this);
		txt_ei_color_light_pink.setOnClickListener(this);
		txt_ei_color_blue.setOnClickListener(this);
		txt_ei_color_light_purple.setOnClickListener(this);
		txt_ei_color_light_green.setOnClickListener(this);
		txt_ei_color_light_yellow.setOnClickListener(this);
		
		txt_font_bolleta.setOnClickListener(this);
		txt_font_alexbrush.setOnClickListener(this);
		txt_font_blackjack.setOnClickListener(this);
		txt_font_england.setOnClickListener(this);
		
		lin_edit.setOnClickListener(this);
		
		txt_edit_invitation_text1.setOnClickListener(this);
		txt_edit_invitation_text2.setOnClickListener(this);
		txt_edit_invitation_text3.setOnClickListener(this);
		txt_edit_invitation_text4.setOnClickListener(this);
		
		
		MainActivity.img_main_notification.setVisibility(View.GONE);
		MainActivity.lin_setting.setVisibility(View.GONE);
		MainActivity.txt_main_done.setVisibility(View.VISIBLE);
		MainActivity.txt_main_done.setOnClickListener(this);
		//scrollView =(ScrollView)view.findViewById(R.id.scrollView);
		//img_ei_image_main.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
       
        Fragment fragment = null;
		switch (v.getId()) {
		case R.id.txt_main_done:
			
			
			
			Bitmap bitmap = null;
			
			
			 View u =lin_main;
	            u.setDrawingCacheEnabled(true);                                                
	            //ScrollView z = scrollView;
	            LinearLayout z = lin_main;
	            int totalHeight = z.getChildAt(0).getHeight();
	            int totalWidth = z.getChildAt(0).getWidth();
	            u.layout(0, 0, totalWidth, totalHeight);
	            
	             
	            u.buildDrawingCache(true);
	            Bitmap b = Bitmap.createBitmap(u.getDrawingCache()); 
	            
	            
	            
	            u.setDrawingCacheEnabled(false);
	            
			         BitmapDrawable bitmapDrawable = new BitmapDrawable(b);



			     ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			     b.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
			     File f = new File(Environment.getExternalStorageDirectory()
			                       + File.separator + "101.jpg");
			     try {
			    f.createNewFile();
			    FileOutputStream fo;
			    fo = new FileOutputStream(f);
			     fo.write(bytes.toByteArray()); 
			      fo.close();
			   } catch (IOException e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			   }
			
			    editor.putString("image_name", f.getAbsolutePath());
			    editor.commit();
			     
			
			fragment = new Fragment_preview_envelop();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			
		//	txt_name.setTextColor(getResources().getColor(R.color.blue));
			
		
			   //  View v1 = findViewById(R.id.img_ei_image_main);// get ur root view id
			   
			  /* View v1 = scrollView;
			         v1.setDrawingCacheEnabled(true);
			         bitmap = v1.getDrawingCache();
			     
*/
			
			
		
			
			 
			
			
			 // View u =scrollView;
			
		
			break;
		/*case R.id.img_ei_image_main:
			
			fragment = new Fragment_Select_Envelope_Liners();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			popup();
			break;*/
		
		case R.id.lin_edit:
			if(selectvalue==1)
			{
				popup();
			
				
			}
			else if(selectvalue==2)
			{	
				popup1();
			}
			else if(selectvalue==3)
			{
				popup2();
			}
			else if(selectvalue==4)
			{
				popup3();
			}	
			
			
			break; 
			
			
		case R.id.txt_edit_invitation_text1:
			selectvalue=1;
			
			//popup();
			break; 
		case R.id.txt_edit_invitation_text2:
			selectvalue=2;
			//
			break; 
		case R.id.txt_edit_invitation_text3:
			selectvalue=3;
			//	
			break; 
		case R.id.txt_edit_invitation_text4:
		
			selectvalue=4;
			
			break; 
			
		case R.id.txt_font_bolleta:
				
			if(selectvalue==1)
			{
				
			
				fs.settypefacetextviewbellota(txt_name);
			}
			else if(selectvalue==2)
			{	
				fs.settypefacetextviewbellota(txt_date);
			}
			else if(selectvalue==3)
			{
				fs.settypefacetextviewbellota(txt_time);
			}
			else if(selectvalue==4)
			{

				fs.settypefacetextviewbellota(txt_venue_name);
				fs.settypefacetextviewbellota(txt_street_name);
				fs.settypefacetextviewbellota(txt_state_name);
			}	
				
			break;
		case R.id.txt_font_alexbrush:

			if(selectvalue==1)
			{
				fs.settypefacetextviewalexbrush(txt_name);
			}
			else if(selectvalue==2)
			{	
				fs.settypefacetextviewalexbrush(txt_date);
			}
			else if(selectvalue==3)
			{
				fs.settypefacetextviewalexbrush(txt_time);
			}
			else if(selectvalue==4)
			{
				fs.settypefacetextviewalexbrush(txt_venue_name);
				fs.settypefacetextviewalexbrush(txt_street_name);
				fs.settypefacetextviewalexbrush(txt_state_name);
			}	

			
			
			//fs.settypefacetextviewalexbrush(txt_name);
			break;
		case R.id.txt_font_blackjack:
			if(selectvalue==1)
			{
				fs.settypefacetextviewblackjack(txt_name);
			}
			else if(selectvalue==2)
			{	
				fs.settypefacetextviewblackjack(txt_date);
			}
			else if(selectvalue==3)
			{
				fs.settypefacetextviewblackjack(txt_time);
			}
			else if(selectvalue==4)
			{
				fs.settypefacetextviewblackjack(txt_venue_name);
				fs.settypefacetextviewblackjack(txt_street_name);
				fs.settypefacetextviewblackjack(txt_state_name);
			}
			
			
			//fs.settypefacetextviewblackjack(txt_name);
			break;
		case R.id.txt_font_england:
			if(selectvalue==1)
			{
				fs.settypefacetextviewengland(txt_name);
			}
			else if(selectvalue==2)
			{	
				fs.settypefacetextviewengland(txt_date);
			}
			else if(selectvalue==3)
			{
				fs.settypefacetextviewengland(txt_time);
			}
			else if(selectvalue==4)
			{
				fs.settypefacetextviewengland(txt_venue_name);
				fs.settypefacetextviewengland(txt_street_name);
				fs.settypefacetextviewengland(txt_state_name);
			}
			
		//	fs.settypefacetextviewengland(txt_name);
			break;
		case R.id.txt_ei_color_black:
			if(selectvalue==1)
			{
				txt_name.setTextColor(getResources().getColor(R.color.black));
			}
			else if(selectvalue==2)
			{	
				txt_date.setTextColor(getResources().getColor(R.color.black));				
			}
			else if(selectvalue==3)
			{
				txt_time.setTextColor(getResources().getColor(R.color.black));				
			}
			else if(selectvalue==4)
			{
				txt_venue_name.setTextColor(getResources().getColor(R.color.black));
				txt_street_name.setTextColor(getResources().getColor(R.color.black));
				txt_state_name.setTextColor(getResources().getColor(R.color.black));				
			}		
			break;
		case R.id.txt_ei_color_white:
			if(selectvalue==1)
			{
				txt_name.setTextColor(getResources().getColor(R.color.white));
			}
			else if(selectvalue==2)
			{	
				txt_date.setTextColor(getResources().getColor(R.color.white));				
			}
			else if(selectvalue==3)
			{
				txt_time.setTextColor(getResources().getColor(R.color.white));				
			}
			else if(selectvalue==4)
			{
				txt_venue_name.setTextColor(getResources().getColor(R.color.white));
				txt_street_name.setTextColor(getResources().getColor(R.color.white));
				txt_state_name.setTextColor(getResources().getColor(R.color.white));				
			}
			break;
		case R.id.txt_ei_color_pink:
			if(selectvalue==1)
			{
				txt_name.setTextColor(getResources().getColor(R.color.pink));
			}
			else if(selectvalue==2)
			{	
				txt_date.setTextColor(getResources().getColor(R.color.pink));				
			}
			else if(selectvalue==3)
			{
				txt_time.setTextColor(getResources().getColor(R.color.pink));				
			}
			else if(selectvalue==4)
			{
				txt_venue_name.setTextColor(getResources().getColor(R.color.pink));
				txt_street_name.setTextColor(getResources().getColor(R.color.pink));
				txt_state_name.setTextColor(getResources().getColor(R.color.pink));				
			}
			//txt_name.setTextColor(getResources().getColor(R.color.pink));
			break;
		case R.id.txt_ei_color_teal:
			
			if(selectvalue==1)
			{
				txt_name.setTextColor(getResources().getColor(R.color.teal));
			}
			else if(selectvalue==2)
			{	
				txt_date.setTextColor(getResources().getColor(R.color.teal));				
			}
			else if(selectvalue==3)
			{
				txt_time.setTextColor(getResources().getColor(R.color.teal));				
			}
			else if(selectvalue==4)
			{
				txt_venue_name.setTextColor(getResources().getColor(R.color.teal));
				txt_street_name.setTextColor(getResources().getColor(R.color.teal));
				txt_state_name.setTextColor(getResources().getColor(R.color.teal));				
			}
			
			
			//txt_name.setTextColor(getResources().getColor(R.color.teal));
			break;
		case R.id.txt_ei_color_blue_in:
			
			if(selectvalue==1)
			{
				txt_name.setTextColor(getResources().getColor(R.color.blue_in));
			}
			else if(selectvalue==2)
			{	
				txt_date.setTextColor(getResources().getColor(R.color.blue_in));				
			}
			else if(selectvalue==3)
			{
				txt_time.setTextColor(getResources().getColor(R.color.blue_in));				
			}
			else if(selectvalue==4)
			{
				txt_venue_name.setTextColor(getResources().getColor(R.color.blue_in));
				txt_street_name.setTextColor(getResources().getColor(R.color.blue_in));
				txt_state_name.setTextColor(getResources().getColor(R.color.blue_in));				
			}
			
			
			//txt_name.setTextColor(getResources().getColor(R.color.blue_in));
			break;
		case R.id.txt_ei_color_brown:
			
		
			if(selectvalue==1)
			{
				txt_name.setTextColor(getResources().getColor(R.color.brown));
			}
			else if(selectvalue==2)
			{	
				txt_date.setTextColor(getResources().getColor(R.color.brown));				
			}
			else if(selectvalue==3)
			{
				txt_time.setTextColor(getResources().getColor(R.color.brown));				
			}
			else if(selectvalue==4)
			{
				txt_venue_name.setTextColor(getResources().getColor(R.color.brown));
				txt_street_name.setTextColor(getResources().getColor(R.color.brown));
				txt_state_name.setTextColor(getResources().getColor(R.color.brown));				
			}
			
			
			//txt_name.setTextColor(getResources().getColor(R.color.brown));
			break;
		case R.id.txt_ei_color_purple:
		
			if(selectvalue==1)
			{
				txt_name.setTextColor(getResources().getColor(R.color.purple));
			}
			else if(selectvalue==2)
			{	
				txt_date.setTextColor(getResources().getColor(R.color.purple));				
			}
			else if(selectvalue==3)
			{
				txt_time.setTextColor(getResources().getColor(R.color.purple));				
			}
			else if(selectvalue==4)
			{
				txt_venue_name.setTextColor(getResources().getColor(R.color.purple));
				txt_street_name.setTextColor(getResources().getColor(R.color.purple));
				txt_state_name.setTextColor(getResources().getColor(R.color.purple));				
			}
			
			//txt_name.setTextColor(getResources().getColor(R.color.purple));
			break;
		case R.id.txt_ei_color_orange:
		
			if(selectvalue==1)
			{
				txt_name.setTextColor(getResources().getColor(R.color.orange));
			}
			else if(selectvalue==2)
			{	
				txt_date.setTextColor(getResources().getColor(R.color.orange));				
			}
			else if(selectvalue==3)
			{
				txt_time.setTextColor(getResources().getColor(R.color.orange));				
			}
			else if(selectvalue==4)
			{
				txt_venue_name.setTextColor(getResources().getColor(R.color.orange));
				txt_street_name.setTextColor(getResources().getColor(R.color.orange));
				txt_state_name.setTextColor(getResources().getColor(R.color.orange));				
			}
			
		//	txt_name.setTextColor(getResources().getColor(R.color.orange));
			break;
		case R.id.txt_ei_color_red:
			
			if(selectvalue==1)
			{
				txt_name.setTextColor(getResources().getColor(R.color.red));
			}
			else if(selectvalue==2)
			{	
				txt_date.setTextColor(getResources().getColor(R.color.red));				
			}
			else if(selectvalue==3)
			{
				txt_time.setTextColor(getResources().getColor(R.color.red));				
			}
			else if(selectvalue==4)
			{
				txt_venue_name.setTextColor(getResources().getColor(R.color.red));
				txt_street_name.setTextColor(getResources().getColor(R.color.red));
				txt_state_name.setTextColor(getResources().getColor(R.color.red));				
			}
			
			
			//txt_name.setTextColor(getResources().getColor(R.color.red));
			break;
		case R.id.txt_ei_color_beige:
			
			if(selectvalue==1)
			{
				txt_name.setTextColor(getResources().getColor(R.color.beige));
			}
			else if(selectvalue==2)
			{	
				txt_date.setTextColor(getResources().getColor(R.color.beige));				
			}
			else if(selectvalue==3)
			{
				txt_time.setTextColor(getResources().getColor(R.color.beige));				
			}
			else if(selectvalue==4)
			{
				txt_venue_name.setTextColor(getResources().getColor(R.color.beige));
				txt_street_name.setTextColor(getResources().getColor(R.color.beige));
				txt_state_name.setTextColor(getResources().getColor(R.color.beige));				
			}
			
			//txt_name.setTextColor(getResources().getColor(R.color.beige));
			break;
		case R.id.txt_ei_color_light_pink:
			
			if(selectvalue==1)
			{
				txt_name.setTextColor(getResources().getColor(R.color.light_pink));
			}
			else if(selectvalue==2)
			{	
				txt_date.setTextColor(getResources().getColor(R.color.light_pink));				
			}
			else if(selectvalue==3)
			{
				txt_time.setTextColor(getResources().getColor(R.color.light_pink));				
			}
			else if(selectvalue==4)
			{
				txt_venue_name.setTextColor(getResources().getColor(R.color.light_pink));
				txt_street_name.setTextColor(getResources().getColor(R.color.light_pink));
				txt_state_name.setTextColor(getResources().getColor(R.color.light_pink));				
			}
			
			//txt_name.setTextColor(getResources().getColor(R.color.light_pink));
			break;
		case R.id.txt_ei_color_blue:
			if(selectvalue==1)
			{
				txt_name.setTextColor(getResources().getColor(R.color.blue));
			}
			else if(selectvalue==2)
			{	
				txt_date.setTextColor(getResources().getColor(R.color.blue));				
			}
			else if(selectvalue==3)
			{
				txt_time.setTextColor(getResources().getColor(R.color.blue));				
			}
			else if(selectvalue==4)
			{
				txt_venue_name.setTextColor(getResources().getColor(R.color.blue));
				txt_street_name.setTextColor(getResources().getColor(R.color.blue));
				txt_state_name.setTextColor(getResources().getColor(R.color.blue));				
			}
			
			//txt_name.setTextColor(getResources().getColor(R.color.blue));
			break;
		case R.id.txt_ei_color_light_purple:
			
			if(selectvalue==1)
			{
				txt_name.setTextColor(getResources().getColor(R.color.light_purple));
			}
			else if(selectvalue==2)
			{	
				txt_date.setTextColor(getResources().getColor(R.color.light_purple));				
			}
			else if(selectvalue==3)
			{
				txt_time.setTextColor(getResources().getColor(R.color.light_purple));				
			}
			else if(selectvalue==4)
			{
				txt_venue_name.setTextColor(getResources().getColor(R.color.light_purple));
				txt_street_name.setTextColor(getResources().getColor(R.color.light_purple));
				txt_state_name.setTextColor(getResources().getColor(R.color.light_purple));				
			}
			
			//txt_name.setTextColor(getResources().getColor(R.color.light_purple));
			break;
		case R.id.txt_ei_color_light_green:
			
			if(selectvalue==1)
			{
				txt_name.setTextColor(getResources().getColor(R.color.light_green));
			}
			else if(selectvalue==2)
			{	
				txt_date.setTextColor(getResources().getColor(R.color.light_green));				
			}
			else if(selectvalue==3)
			{
				txt_time.setTextColor(getResources().getColor(R.color.light_green));				
			}
			else if(selectvalue==4)
			{
				txt_venue_name.setTextColor(getResources().getColor(R.color.light_green));
				txt_street_name.setTextColor(getResources().getColor(R.color.light_green));
				txt_state_name.setTextColor(getResources().getColor(R.color.light_green));				
			}
			
			//txt_name.setTextColor(getResources().getColor(R.color.light_green));
			break;
		case R.id.txt_ei_color_light_yellow:
			
			if(selectvalue==1)
			{
				txt_name.setTextColor(getResources().getColor(R.color.light_yellow));
			}
			else if(selectvalue==2)
			{	
				txt_date.setTextColor(getResources().getColor(R.color.light_yellow));				
			}
			else if(selectvalue==3)
			{
				txt_time.setTextColor(getResources().getColor(R.color.light_yellow));				
			}
			else if(selectvalue==4)
			{
				txt_venue_name.setTextColor(getResources().getColor(R.color.light_yellow));
				txt_street_name.setTextColor(getResources().getColor(R.color.light_yellow));
				txt_state_name.setTextColor(getResources().getColor(R.color.light_yellow));				
			}
			//txt_name.setTextColor(getResources().getColor(R.color.light_yellow));
			break;
			
		case R.id.img_ei_setting:
				
				if (set==0) {
					seek_font.setVisibility(View.VISIBLE);
					lin_ei_font_alignment.setVisibility(View.GONE);
					set =1;
				}
				else
				{
					seek_font.setVisibility(View.GONE);
					lin_ei_font_alignment.setVisibility(View.VISIBLE);
					set =0;
				}
					
			
			break;
			
		case R.id.img_ei_font_alignment_left:
			if(selectvalue==1)
			{
				txt_name.setGravity(Gravity.LEFT);
			}
			else if(selectvalue==2)
			{	
				txt_date.setGravity(Gravity.LEFT);				
			}
			else if(selectvalue==3)
			{
				txt_time.setGravity(Gravity.LEFT);				
			}
			else if(selectvalue==4)
			{
				txt_venue_name.setGravity(Gravity.LEFT);
				txt_street_name.setGravity(Gravity.LEFT);
				txt_state_name.setGravity(Gravity.LEFT);				
			}
			
			
			break;
		case R.id.img_ei_font_alignment_center:
			
			if(selectvalue==1)
			{
				txt_name.setGravity(Gravity.CENTER);
			}
			else if(selectvalue==2)
			{	
				txt_date.setGravity(Gravity.CENTER);				
			}
			else if(selectvalue==3)
			{
				txt_time.setGravity(Gravity.CENTER);				
			}
			else if(selectvalue==4)
			{
				txt_venue_name.setGravity(Gravity.CENTER);
				txt_street_name.setGravity(Gravity.CENTER);
				txt_state_name.setGravity(Gravity.CENTER);				
			}
		
			break;
		case R.id.img_ei_font_alignment_right:
			
			if(selectvalue==1)
			{
				txt_name.setGravity(Gravity.RIGHT);
			}
			else if(selectvalue==2)
			{	
				txt_date.setGravity(Gravity.RIGHT);				
			}
			else if(selectvalue==3)
			{
				txt_time.setGravity(Gravity.RIGHT);				
			}
			else if(selectvalue==4)
			{
				txt_venue_name.setGravity(Gravity.RIGHT);
				txt_street_name.setGravity(Gravity.RIGHT);
				txt_state_name.setGravity(Gravity.RIGHT);				
			}
			break;
			
			
			
		case R.id.txt_name:
			selectvalue=1;
			break;
		case R.id.txt_date:
			selectvalue=2;
			break;
		case R.id.txt_time:
			selectvalue=3;
			break;
		case R.id.txt_venue_name:
			selectvalue=4;
			break;
		case R.id.txt_street_name:
			selectvalue=4;
			break;
			
		case R.id.txt_state_name:
			selectvalue=4;
			break;
		case R.id.img_right:
			hsv.scrollTo((int)hsv.getScrollX() + 300, (int)hsv.getScrollY());
			break;
		case R.id.img_left:
			hsv.scrollTo((int)hsv.getScrollX() - 300, (int)hsv.getScrollY());
			break;	
			
			
			
		default:
			break;
		}
	}
	
	
public void popup()
{
	final Dialog deleteDialogView = new Dialog(getActivity());
	
	deleteDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);
	deleteDialogView.getWindow().setBackgroundDrawableResource(
		     R.color.transparent);
	deleteDialogView.setContentView(R.layout.dialog_invitetion_text);
	
	deleteDialogView.setCancelable(false);
	
		    
		    final ImageView img_ei_dialog_close;
		    final TextView txt_ei_dialog_title,txt_ei_dialog_ok;
		    
		    final EditText edt_ei_dialog_eventname;
		    
		    txt_ei_dialog_title=(TextView)deleteDialogView.findViewById(R.id.txt_ei_dialog_title);
		    txt_ei_dialog_ok=(TextView)deleteDialogView.findViewById(R.id.txt_ei_dialog_ok);
		    edt_ei_dialog_eventname =(EditText)deleteDialogView.findViewById(R.id.edt_ei_dialog_eventname);
		    
		    img_ei_dialog_close=(ImageView)deleteDialogView.findViewById(R.id.img_ei_dialog_close);
		    
		    fs.settypefacetextviewbellota(txt_ei_dialog_title);
		    fs.settypefacetextview(txt_ei_dialog_ok);
		    
		    txt_ei_dialog_title.setText("Enter Your Name");
		    txt_ei_dialog_ok.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					txt_name.setText(edt_ei_dialog_eventname.getText().toString());
					deleteDialogView.dismiss();
				}
			});
		    
		   
		    img_ei_dialog_close.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					
					deleteDialogView.dismiss();
				}
			});
		    

		    
		    
		    deleteDialogView.show();

}
/***********************************************************************************/

public void popup1()
{
	final Dialog deleteDialogView = new Dialog(getActivity());
	
	deleteDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);
	deleteDialogView.getWindow().setBackgroundDrawableResource(
		     R.color.transparent);
	deleteDialogView.setContentView(R.layout.dialog_invitetion_text);
	
	deleteDialogView.setCancelable(false);
	
		    
		    final ImageView img_ei_dialog_close;
		    final TextView txt_ei_dialog_title,txt_ei_dialog_ok;
		    
		    final EditText edt_ei_dialog_eventname;
		    
		    txt_ei_dialog_title=(TextView)deleteDialogView.findViewById(R.id.txt_ei_dialog_title);
		    txt_ei_dialog_ok=(TextView)deleteDialogView.findViewById(R.id.txt_ei_dialog_ok);
		    edt_ei_dialog_eventname =(EditText)deleteDialogView.findViewById(R.id.edt_ei_dialog_eventname);
		    
		    img_ei_dialog_close=(ImageView)deleteDialogView.findViewById(R.id.img_ei_dialog_close);
		    
		    fs.settypefacetextviewbellota(txt_ei_dialog_title);
		    fs.settypefacetextview(txt_ei_dialog_ok);
		    
		    txt_ei_dialog_title.setText("Enter Event Date");
		    
		    txt_ei_dialog_ok.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					txt_date.setText(edt_ei_dialog_eventname.getText().toString());
					deleteDialogView.dismiss();
				}
			});
		    
		   
		    img_ei_dialog_close.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					
					deleteDialogView.dismiss();
				}
			});
		    

		    
		    
		    deleteDialogView.show();

}





/**************************************************************************************/

public void popup2()
{
	final Dialog deleteDialogView = new Dialog(getActivity());
	
	deleteDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);
	deleteDialogView.getWindow().setBackgroundDrawableResource(
		     R.color.transparent);
	deleteDialogView.setContentView(R.layout.dialog_invitetion_text);
	
	deleteDialogView.setCancelable(false);
	
		    
		    final ImageView img_ei_dialog_close;
		    final TextView txt_ei_dialog_title,txt_ei_dialog_ok;
		    
		    final EditText edt_ei_dialog_eventname;
		    
		    txt_ei_dialog_title=(TextView)deleteDialogView.findViewById(R.id.txt_ei_dialog_title);
		    txt_ei_dialog_ok=(TextView)deleteDialogView.findViewById(R.id.txt_ei_dialog_ok);
		    edt_ei_dialog_eventname =(EditText)deleteDialogView.findViewById(R.id.edt_ei_dialog_eventname);
		    
		    img_ei_dialog_close=(ImageView)deleteDialogView.findViewById(R.id.img_ei_dialog_close);
		    
		    fs.settypefacetextviewbellota(txt_ei_dialog_title);
		    fs.settypefacetextview(txt_ei_dialog_ok);
		    
		    txt_ei_dialog_title.setText("Enter Event Time");
		    
		    txt_ei_dialog_ok.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					txt_time.setText(edt_ei_dialog_eventname.getText().toString());
					deleteDialogView.dismiss();
				}
			});
		    
		   
		    img_ei_dialog_close.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					
					deleteDialogView.dismiss();
				}
			});
		    

		    
		    
		    deleteDialogView.show();

}





/**************************************************************************************/


public void popup3()
{
	final Dialog deleteDialogView = new Dialog(getActivity());
	
	deleteDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);
	deleteDialogView.getWindow().setBackgroundDrawableResource(
		     R.color.transparent);
	deleteDialogView.setContentView(R.layout.dialog_invitetion_address);
	
	deleteDialogView.setCancelable(false);
	
		    
		    final ImageView img_ei_dialog_close;
		    final TextView txt_ei_dialog_title,txt_ei_dialog_ok;
		    
		    final EditText edt_ei_dialog_venue_name,edt_ei_dialog_street_name,edt_ei_dialog_state_name;
		    
		    txt_ei_dialog_title=(TextView)deleteDialogView.findViewById(R.id.txt_ei_dialog_title);
		    txt_ei_dialog_ok=(TextView)deleteDialogView.findViewById(R.id.txt_ei_dialog_ok);
		    edt_ei_dialog_venue_name =(EditText)deleteDialogView.findViewById(R.id.edt_ei_dialog_venue_name);
		    edt_ei_dialog_street_name =(EditText)deleteDialogView.findViewById(R.id.edt_ei_dialog_street_name);
		    edt_ei_dialog_state_name =(EditText)deleteDialogView.findViewById(R.id.edt_ei_dialog_state_name);
		    
		    img_ei_dialog_close=(ImageView)deleteDialogView.findViewById(R.id.img_ei_dialog_close);
		    
		    fs.settypefacetextviewbellota(txt_ei_dialog_title);
		    fs.settypefacetextview(txt_ei_dialog_ok);
		    
		    txt_ei_dialog_title.setText("Enter Event Address");
		    
		    txt_ei_dialog_ok.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					txt_venue_name.setText(edt_ei_dialog_venue_name.getText().toString());
					txt_street_name.setText(edt_ei_dialog_street_name.getText().toString());
					txt_state_name.setText(edt_ei_dialog_state_name.getText().toString());
					
					deleteDialogView.dismiss();
				}
			});
		    
		   
		    img_ei_dialog_close.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					
					deleteDialogView.dismiss();
				}
			});
		    

		    
		    
		    deleteDialogView.show();

}





/**************************************************************************************/




/*public void drag(MotionEvent event, View v)
{
	LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) v.getLayoutParams();	
    
    switch(event.getAction())
    {
       case MotionEvent.ACTION_MOVE:
       {
         params.topMargin = (int)event.getRawY() - (v.getHeight());
         params.leftMargin = (int)event.getRawX() - (v.getWidth()/2);
         v.setLayoutParams(params);
         break;
       }
       case MotionEvent.ACTION_UP:
       {
         params.topMargin = (int)event.getRawY() - (v.getHeight());
         params.leftMargin = (int)event.getRawX() - (v.getWidth()/2);
         v.setLayoutParams(params);
         break;
       }
       case MotionEvent.ACTION_DOWN:
       {
        v.setLayoutParams(params);
        break;
       }
    }

	
}
*/



/*public void onPause() {
	Log.e("qt", " on pause");
	
	super.onPause();
	mParams = txt_name.getCurrentLayout();
}
*/
/*@Override
public void onResume() {
	Log.e("qt", " on Resume");
	
	super.onResume();
	if(mParams != null){
		txt_name.layout(mParams[0] , mParams[1], mParams[2], mParams[3]);
	}
}
*/
/*******************************************************/
//public void drag(MotionEvent event, View v)
public void drag(MotionEvent event, View v)
{

    FrameLayout.LayoutParams params = (android.widget.FrameLayout.LayoutParams) v.getLayoutParams();
    
    switch(event.getAction())
    {
    
       case MotionEvent.ACTION_MOVE:
       {
         params.topMargin = (int)event.getRawY() - (v.getHeight());
         params.leftMargin = (int)event.getRawX() - (v.getWidth()/2);
         v.setLayoutParams(params);
         break;
       }
       case MotionEvent.ACTION_UP:
       {
         params.topMargin = (int)event.getRawY() - (v.getHeight());
         params.leftMargin = (int)event.getRawX() - (v.getWidth()/2);
         v.setLayoutParams(params);
         break;
       }
       case MotionEvent.ACTION_DOWN:
       {
    	 
        v.setLayoutParams(params);
        break;
       }
    }
	
	
	
	
	
}

/***********************************************************/


/***********************************************************************/


}