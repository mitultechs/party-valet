package com.partyvalvet.partyvalvet.fragment.RSVP.host;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.entity.mime.MinimalField;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_RSVP_host_Detail_Schedule extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	
	ImageView img_ds_adddining; 
	TextView txt_ds_comments,txt_ds_RSVP,txt_ds_dining,txt_ds_num_of_guest,txt_ds_allow_guest_for_din,txt_ds_sehedule_date,txt_ds_sehedule_date_display,txt_ds_add_to_cal,txt_ds_show_num_of_guest;
	EditText edt_ds_comments,edt_ds_diningcustomadd;
	ToggleButton toggleButton_ds_rsvp,toggleButton_ds_dining,toggleButton_ds_num_of_guest,toggleButton_ds_allow_guest_for_din,toggleButton_ds_schedule_date,toggleButton_ds_add_to_cal,toggleButton_ds_show_num_of_guest;
	
	LinearLayout lin_ds_diningadd,lin_ds_container,lin_ds_scheduledate;
	
	ArrayList<String> listag;
	
	static int count = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_rsvp_detail_schedul, null);
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		listag = new ArrayList<String>();
		
		MainActivity.screen ="rsvpscheduledetail";
		MainActivity.txt_main_title.setText(getString(R.string.headingdetail));
		
		fs.hideheading();
		MainActivity.img_main_menu.setVisibility(View.VISIBLE);
		MainActivity.img_main_menu.setEnabled(true);
		
		MainActivity.txt_main_done.setVisibility(View.VISIBLE);
		
		
		bindid(view);
		toggleButton_ds_dining.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				if (!toggleButton_ds_dining.isChecked()) {
					lin_ds_diningadd.setVisibility(View.GONE);
				}
				else
				{
					
					lin_ds_diningadd.setVisibility(View.VISIBLE);
				}
			}
		});
		
		toggleButton_ds_schedule_date.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				if (!toggleButton_ds_schedule_date.isChecked()) {
					lin_ds_scheduledate.setVisibility(View.GONE);
				}
				else
				{
					lin_ds_scheduledate.setVisibility(View.VISIBLE);
					
					View multiPickerLayout = LayoutInflater.from(getActivity()).inflate(R.layout.datetime_dialog, null);
					final DatePicker multiPickerDate = (DatePicker) multiPickerLayout.findViewById(R.id.multipicker_date);
					final TimePicker multiPickerTime = (TimePicker) multiPickerLayout.findViewById(R.id.multipicker_time);
					
					//multiPickerTime.setIs24HourView(true);
					DialogInterface.OnClickListener dialogButtonListener = new DialogInterface.OnClickListener() {
					  @Override
					  public void onClick(DialogInterface dialog, int which) {
					    switch(which) {
					      case DialogInterface.BUTTON_NEGATIVE: {
					        // user tapped "cancel"
					        dialog.dismiss();
					        break;
					      }
					      case DialogInterface.BUTTON_POSITIVE: {
					        // user tapped "set"
					        // here, use the "multiPickerDate" and "multiPickerTime" objects to retreive the date/time the user selected
					    	 
					    	  int day = multiPickerDate.getDayOfMonth();
					    	  int month = multiPickerDate.getMonth();
					    	  int year = multiPickerDate.getYear();
					    	  int hour = multiPickerTime.getCurrentHour();
					    	  int min = multiPickerTime.getCurrentMinute();
					    			  
					    			 
					    	    
					    	  //txt_ds_sehedule_date_display.setText(day+"/"+year+"/"+month);
					    	  
					    	  
					    	    Calendar cal = Calendar.getInstance();
					    	    cal.set(Calendar.YEAR, year);
					    	    cal.set(Calendar.DAY_OF_MONTH, day);
					    	    cal.set(Calendar.MONTH, month);
					    	    String format = new SimpleDateFormat("E yy MMM ").format(cal.getTime());
					    	    String time = "";
					    	    try {       
					    	           String _24HourTime = hour+":"+min;
					    	           SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
					    	           SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
					    	           Date _24HourDt = _24HourSDF.parse(_24HourTime);
					    	           System.out.println(_24HourDt);
					    	           System.out.println(_12HourSDF.format(_24HourDt));
					    	           time =_12HourSDF.format(_24HourDt);
					    	       } catch (Exception e) {
					    	           e.printStackTrace();
					    	       }
					    	    txt_ds_sehedule_date_display.setText(format+" "+time);
					    	    
					    	    
					    	    
					    	    
					    	  
					        break;
					      }
					      default: {
					        dialog.dismiss();
					        break;
					      }
					    }
					  }
					};

					AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
					builder.setView(multiPickerLayout);
					builder.setPositiveButton("Set", dialogButtonListener);
					builder.setNegativeButton("Cancel", dialogButtonListener);
					builder.show();
				}
			}
		});
		
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		
		img_ds_adddining=(ImageView)view.findViewById(R.id.img_ds_adddining);
		img_ds_adddining.setOnClickListener(this);
		
		txt_ds_comments=(TextView)view.findViewById(R.id.txt_ds_comments);
		txt_ds_RSVP=(TextView)view.findViewById(R.id.txt_ds_RSVP);
		txt_ds_dining=(TextView)view.findViewById(R.id.txt_ds_dining);
		txt_ds_num_of_guest=(TextView)view.findViewById(R.id.txt_ds_num_of_guest);
		txt_ds_allow_guest_for_din=(TextView)view.findViewById(R.id.txt_ds_allow_guest_for_din);
		txt_ds_sehedule_date=(TextView)view.findViewById(R.id.txt_ds_sehedule_date);
		txt_ds_sehedule_date_display=(TextView)view.findViewById(R.id.txt_ds_sehedule_date_display);
		txt_ds_add_to_cal=(TextView)view.findViewById(R.id.txt_ds_add_to_cal);
		txt_ds_show_num_of_guest=(TextView)view.findViewById(R.id.txt_ds_show_num_of_guest);
		
		fs.settypefacetextviewbellota(txt_ds_comments);
		fs.settypefacetextviewbellota(txt_ds_RSVP);
		fs.settypefacetextviewbellota(txt_ds_dining);
		fs.settypefacetextviewbellota(txt_ds_num_of_guest);
		fs.settypefacetextviewbellota(txt_ds_allow_guest_for_din);
		fs.settypefacetextviewbellota(txt_ds_sehedule_date);
		fs.settypefacetextviewbellota(txt_ds_sehedule_date_display);
		fs.settypefacetextviewbellota(txt_ds_add_to_cal);
		fs.settypefacetextviewbellota(txt_ds_show_num_of_guest);
		
		
		edt_ds_comments=(EditText)view.findViewById(R.id.edt_ds_comments);
		edt_ds_diningcustomadd=(EditText)view.findViewById(R.id.edt_ds_diningcustomadd);
		
		fs.settypefacetextviewbellota(edt_ds_comments);
		fs.settypefacetextviewbellota(edt_ds_diningcustomadd);
		
		
		toggleButton_ds_rsvp=(ToggleButton)view.findViewById(R.id.toggleButton_ds_rsvp);
		toggleButton_ds_dining=(ToggleButton)view.findViewById(R.id.toggleButton_ds_dining);
		toggleButton_ds_num_of_guest=(ToggleButton)view.findViewById(R.id.toggleButton_ds_num_of_guest);
		toggleButton_ds_allow_guest_for_din=(ToggleButton)view.findViewById(R.id.toggleButton_ds_allow_guest_for_din);
		toggleButton_ds_schedule_date=(ToggleButton)view.findViewById(R.id.toggleButton_ds_schedule_date);
		toggleButton_ds_add_to_cal=(ToggleButton)view.findViewById(R.id.toggleButton_ds_add_to_cal);
		toggleButton_ds_show_num_of_guest=(ToggleButton)view.findViewById(R.id.toggleButton_ds_show_num_of_guest);
		
		lin_ds_diningadd=(LinearLayout)view.findViewById(R.id.lin_ds_diningadd);
		lin_ds_container=(LinearLayout)view.findViewById(R.id.lin_ds_container);
		lin_ds_scheduledate=(LinearLayout)view.findViewById(R.id.lin_ds_scheduledate);
		
		lin_ds_diningadd.setVisibility(View.GONE);
		lin_ds_scheduledate.setVisibility(View.GONE);
		
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.img_ds_adddining:
			if (!edt_ds_diningcustomadd.getText().toString().equalsIgnoreCase("")) {
				LayoutInflater layoutInflater = (LayoutInflater) getActivity()
						.getBaseContext().getSystemService(
								Context.LAYOUT_INFLATER_SERVICE);
				final View addView = layoutInflater.inflate(R.layout.row, null);
				TextView textOut = (TextView) addView
						.findViewById(R.id.textout);
				fs.settypefacetextviewbellota(textOut);
				textOut.setText(edt_ds_diningcustomadd.getText().toString());
				
				listag.add(edt_ds_diningcustomadd.getText().toString());
				addView.setTag(edt_ds_diningcustomadd.getText().toString());

				textOut.setOnLongClickListener(new View.OnLongClickListener() {

					@Override
					public boolean onLongClick(View arg0) {
						// TODO Auto-generated method stub
						((LinearLayout) addView.getParent())
								.removeView(addView);
						String pos = (String) addView.getTag();
						if (listag.size() != 0) {

							listag.remove(pos);
							Log.d("listag_size", listag.size() + " ");
							count--;
						} else {
							count = 0;
						}
						Log.d("position arg0", arg0.getTag()
								+ " addviewposition " + addView.getTag() + "--");
						String strlisttag = listag.toString();
						Log.d("alltags in list", strlisttag);
						return false;
					}
				});
				edt_ds_diningcustomadd.setText("");

				lin_ds_container.addView(addView);
				count++;

			} else {

				Toast.makeText(getActivity(), "Enter Tag", Toast.LENGTH_LONG)
						.show();
			}

			
			break;

		default:
			break;
		}
	}

}
