package com.partyvalvet.partyvalvet.fragment.RSVP;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.customeAdepter.Customlist_My_rsvp;
import com.partyvalvet.partyvalvet.customeAdepter.Customlist_My_rsvp_hosting;
import com.partyvalvet.partyvalvet.customeAdepter.Customlist_My_rsvp_hostlist;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ConnectionDetector;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_My_RSVP extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	TextView txt_my_rsvp_attending,txt_my_rsvp_hosting;
	ListView list_my_rsvp_hosting;
	LinearLayout lin_my_rsvp_change;
	Customlist_My_rsvp adapterattending;
	Customlist_My_rsvp_hostlist adapterhosting;
	ArrayList<HashMap<String, String>> arraylistattanding,arraylisthosting;
	
	String urlattending,urlhosting,userid,url;
	
	SharedPreferences preflogin;
	
	AsyncTask<Void, Void, Void> task;
	
	int flagattanding=0,flaghosting=0;;
	
	ProgressDialog pDialog ;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_my_rsvp_host_and_attend, null);
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		fs.showheading();
		
		arraylistattanding = new ArrayList<HashMap<String, String>>();
		arraylisthosting = new ArrayList<HashMap<String, String>>();
		//MainActivity.screen ="login";
		MainActivity.screen ="myinvitation";
		MainActivity.txt_main_title.setText(getString(R.string.headingmyrsvps));
		bindid(view);
		preflogin = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
		if (preflogin!=null) {
			userid = preflogin.getString("id", "");
			/*c_id= preflogin.getString("c_id", "");
			plan = preflogin.getString("plan", "");*/
		}
		
		if (ConnectionDetector
				.isNetworkAvailable(getActivity())) {

			//urlmyinvitation = getString(R.string.url)+"select_my_card/index.php?user_id="+userid;
			//url = getString(R.string.url)+"select_envelope_liner/index.php?plan=all";
			//urlmyeneveleop = getString(R.string.url)+"select_my_envelope/index.php?user_id="+userid;
			if (arraylistattanding!=null && arraylistattanding.size()==0) {
				urlattending = getString(R.string.url)+"select_myrsvp_attending/index.php?uid="+userid;
				
				task =	new getattendinglist();
				task.execute();	
			}
			
		
		} else {
			fs.toast(getString(R.string.nointernet));
		}
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		txt_my_rsvp_attending=(TextView)view.findViewById(R.id.txt_my_rsvp_attending);
		txt_my_rsvp_hosting=(TextView)view.findViewById(R.id.txt_my_rsvp_hosting);
		
		fs.settypefacetextviewbellota(txt_my_rsvp_attending);
		fs.settypefacetextviewbellota(txt_my_rsvp_hosting);
		
		txt_my_rsvp_attending.setOnClickListener(this);
		txt_my_rsvp_hosting.setOnClickListener(this);
		
		list_my_rsvp_hosting=(ListView)view.findViewById(R.id.list_my_rsvp_hosting);
		
		lin_my_rsvp_change =(LinearLayout)view.findViewById(R.id.lin_my_rsvp_change);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.txt_my_rsvp_attending:
			if (arraylistattanding.size()>0 && arraylistattanding!=null) {
				list_my_rsvp_hosting.setVisibility(View.VISIBLE);
				adapterattending = new Customlist_My_rsvp(getActivity(), arraylistattanding);
				list_my_rsvp_hosting.setAdapter(adapterattending);
				lin_my_rsvp_change.setBackground(getResources().getDrawable(R.drawable.attendingwhitebg));
				txt_my_rsvp_attending.setText(getString(R.string.attending));
				
				txt_my_rsvp_attending.setTextColor(getResources().getColor(R.color.sye_text_color));
				txt_my_rsvp_hosting.setTextColor(getResources().getColor(R.color.whitecolor));
			}
			else
			{
				lin_my_rsvp_change.setBackground(getResources().getDrawable(R.drawable.attendingwhitebg));
				txt_my_rsvp_attending.setText(getString(R.string.attending));
				txt_my_rsvp_attending.setTextColor(getResources().getColor(R.color.sye_text_color));
				txt_my_rsvp_hosting.setTextColor(getResources().getColor(R.color.whitecolor));
				list_my_rsvp_hosting.setVisibility(View.GONE);
			}
			break;
		case R.id.txt_my_rsvp_hosting:
			if (arraylisthosting.size()>0 && arraylisthosting!=null) {
				list_my_rsvp_hosting.setVisibility(View.VISIBLE);
				adapterhosting = new Customlist_My_rsvp_hostlist(getActivity(), arraylisthosting);
				list_my_rsvp_hosting.setAdapter(adapterhosting);
				lin_my_rsvp_change.setBackground(getResources().getDrawable(R.drawable.hostingwhitebg));
				txt_my_rsvp_hosting.setText(getString(R.string.hosting));
				txt_my_rsvp_hosting.setTextColor(getResources().getColor(R.color.sye_text_color));
				txt_my_rsvp_attending.setTextColor(getResources().getColor(R.color.whitecolor));
				
			}
			else
			{
				lin_my_rsvp_change.setBackground(getResources().getDrawable(R.drawable.hostingwhitebg));
				txt_my_rsvp_attending.setText(getString(R.string.attending));
				txt_my_rsvp_hosting.setTextColor(getResources().getColor(R.color.sye_text_color));
				txt_my_rsvp_attending.setTextColor(getResources().getColor(R.color.whitecolor));
				list_my_rsvp_hosting.setVisibility(View.GONE);
			}
			break;

		default:
			break;
		}
	}
	
	public class getattendinglist extends AsyncTask<Void, Void, Void>
	{

		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			String jsonstringattanding = sh.makeServiceCall(urlattending, ServiceHandler.GET);
			if (jsonstringattanding!=null) {
				
				try {
					JSONObject jobj = new JSONObject(jsonstringattanding);
					if (jobj.has("200")) {
						if (arraylistattanding.size()>0||arraylistattanding!=null) {
							arraylistattanding.clear();
						}
						JSONArray arydata =  jobj.getJSONArray("200");
						if (arydata.length()>0) {
							flagattanding =1;
							for (int i = 0; i < arydata.length(); i++) {
								JSONObject objdata = arydata.getJSONObject(i);
								HashMap<String, String> map = new HashMap<String, String>();
								
								map.put("eid", objdata.getString("eid"));
								map.put("event_name", objdata.getString("event_name"));
								map.put("event_date", objdata.getString("event_date"));
								map.put("event_status",objdata.getString("event_status"));
								
								arraylistattanding.add(map);
							}
							
						}
					}
					else if (jobj.has("400")) {
						flagattanding =0;
					}
					
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			if (flagattanding==1) {
				adapterattending = new Customlist_My_rsvp(getActivity(), arraylistattanding);
				list_my_rsvp_hosting.setAdapter(adapterattending);
			}
			
			if (ConnectionDetector
					.isNetworkAvailable(getActivity())) {

				//urlmyinvitation = getString(R.string.url)+"select_my_card/index.php?user_id="+userid;
				//url = getString(R.string.url)+"select_envelope_liner/index.php?plan=all";
				//urlmyeneveleop = getString(R.string.url)+"select_my_envelope/index.php?user_id="+userid;
				urlhosting = getString(R.string.url)+"select_myrsvp_host/index.php?uid="+userid;
				
			task =	new gethostinglist();
			task.execute();
			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage(getString(R.string.plzwait));
            pDialog.setCancelable(false);
            pDialog.show();
		}
		
	}

	
	public class gethostinglist extends AsyncTask<Void, Void, Void>
	{

		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			String jsonstringattanding = sh.makeServiceCall(urlhosting, ServiceHandler.GET);
			if (jsonstringattanding!=null) {
				
				try {
					JSONObject jobj = new JSONObject(jsonstringattanding);
					if (jobj.has("200")) {
						if (arraylisthosting.size()>0||arraylisthosting!=null) {
							arraylisthosting.clear();
						}
						JSONArray arydata =  jobj.getJSONArray("200");
						if (arydata.length()>0) {
							
							flaghosting =1;
							for (int i = 0; i < arydata.length(); i++) {
								JSONObject objdata = arydata.getJSONObject(i);
								HashMap<String, String> map = new HashMap<String, String>();
								
								map.put("eid", objdata.getString("eid"));
								map.put("event_name", objdata.getString("event_name"));
								map.put("event_date", objdata.getString("event_date"));
								map.put("event_status",objdata.getString("event_status"));
								
								arraylisthosting.add(map);
							}
							
						}
					}
					else if (jobj.has("400")) {
						flaghosting =0;
					}
					
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			/*if (flaghosting==1) {
				adapter = new Customlist_My_rsvp(getActivity(), arraylisthosting);
				list_my_rsvp_hosting.setAdapter(adapter);
			}*/
			
			
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
		
	}

	public void onDestroy() {
	    super.onDestroy();

	    if(task != null && task.getStatus() != AsyncTask.Status.FINISHED) {
	        task.cancel(true);
	    }
	}

}
