package com.partyvalvet.partyvalvet.fragment.RSVP.guest;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_My_RSVP_Change_rsvp extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	
	TextView txt_change_rsvp_current,txt_change_rsvp_current_status,txt_change_rsvp_new,txt_change_rsvp_yes,txt_change_rsvp_no,txt_change_rsvp_comments,txt_change_rsvp_update;
	
	ImageView img_change_rsvp_yes,img_change_rsvp_no;
	
	EditText edt_change_rsvp_comments;
	
	LinearLayout lin_change_rsvp_yes,lin_change_rsvp_no;
	
	SharedPreferences preflogin;
	
	String event_status;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_change_rsvp, null);
		MainActivity.txt_main_title.setText(getString(R.string.headingchangersvp));
		MainActivity.screen ="changersvp";
		
		preflogin = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
		
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		
		bindid(view);
		if (preflogin!=null) {
			
			event_status =preflogin.getString("event_status", "");
			
		}
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		
		txt_change_rsvp_current=(TextView)view.findViewById(R.id.txt_change_rsvp_current);
		txt_change_rsvp_current_status=(TextView)view.findViewById(R.id.txt_change_rsvp_current_status);
		txt_change_rsvp_new=(TextView)view.findViewById(R.id.txt_change_rsvp_new);
		txt_change_rsvp_yes=(TextView)view.findViewById(R.id.txt_change_rsvp_yes);
		txt_change_rsvp_no=(TextView)view.findViewById(R.id.txt_change_rsvp_no);
		txt_change_rsvp_comments=(TextView)view.findViewById(R.id.txt_change_rsvp_comments);
		txt_change_rsvp_update=(TextView)view.findViewById(R.id.txt_change_rsvp_update);
		
		fs.settypefacetextviewbellota(txt_change_rsvp_current);
		fs.settypefacetextviewbellota(txt_change_rsvp_current_status);
		fs.settypefacetextviewbellota(txt_change_rsvp_new);
		fs.settypefacetextviewbellota(txt_change_rsvp_yes);
		fs.settypefacetextviewbellota(txt_change_rsvp_no);
		fs.settypefacetextviewbellota(txt_change_rsvp_comments);
		fs.settypefacetextview(txt_change_rsvp_update);
		
		txt_change_rsvp_update.setOnClickListener(this);
		
		img_change_rsvp_yes=(ImageView)view.findViewById(R.id.img_change_rsvp_yes);
		img_change_rsvp_no=(ImageView)view.findViewById(R.id.img_change_rsvp_no);
		
		edt_change_rsvp_comments=(EditText)view.findViewById(R.id.edt_change_rsvp_comments);
		
		fs.settypefaceedittextbellota(edt_change_rsvp_comments);
		
		lin_change_rsvp_yes=(LinearLayout)view.findViewById(R.id.lin_change_rsvp_yes);
		lin_change_rsvp_no=(LinearLayout)view.findViewById(R.id.lin_change_rsvp_no);
		
		lin_change_rsvp_yes.setOnClickListener(this);
		lin_change_rsvp_no.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.lin_change_rsvp_yes:

			img_change_rsvp_yes.setImageDrawable(getResources().getDrawable(R.drawable.rshostingselected));
			
			img_change_rsvp_no.setImageDrawable(getResources().getDrawable(R.drawable.rshostingunselected));
			
			break;
			
		case R.id.lin_change_rsvp_no:
			
			img_change_rsvp_no.setImageDrawable(getResources().getDrawable(R.drawable.rshostingselected));
			img_change_rsvp_yes.setImageDrawable(getResources().getDrawable(R.drawable.rshostingunselected));
			
			break;
			
		case R.id.txt_change_rsvp_update:
			
			popup();
			
			break;
			
		default:
			break;
		}
	}
	
	public void popup()
	{
		final Dialog deleteDialogView = new Dialog(getActivity());
		
		deleteDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);
		deleteDialogView.getWindow().setBackgroundDrawableResource(
			     R.color.transparent);
		deleteDialogView.setContentView(R.layout.dialog_my_srvp_attending);
		
		deleteDialogView.setCancelable(false);
		
			    
			    final ImageView img_ei_dialog_close;
			    final TextView txt_ei_dialog_message;
			    
			    
			    
			    txt_ei_dialog_message=(TextView)deleteDialogView.findViewById(R.id.txt_ei_dialog_message);
			    
			    
			    img_ei_dialog_close=(ImageView)deleteDialogView.findViewById(R.id.img_ei_dialog_close);
			    
			    fs.settypefacetextviewbellota(txt_ei_dialog_message);
			    
			   
			    img_ei_dialog_close.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						
						
						deleteDialogView.dismiss();
					}
				});
			    

			    
			    deleteDialogView.show();

	}

}
