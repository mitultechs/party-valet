package com.partyvalvet.partyvalvet.fragment.RSVP.guest;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.fragment.Fragment_Create_event_selection;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_My_RSVP_attending_details extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	
	String userid,eid,url;
	SharedPreferences preflogin;
	TextView txt_rsvp_attend_changersvp,txt_rsvp_attend_title,txt_rsvp_attend_viewonrsvp,txt_rsvp_attend_myrsvp,txt_rsvp_attend_myrsvp_status;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_my_rsvp_attending, null);
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		
		MainActivity.txt_main_title.setText(getString(R.string.headingrsvpattend));
		MainActivity.screen ="myrsvphosting";
		
		bindid(view);
		
		preflogin = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
		if (preflogin!=null) {
			
			userid = preflogin.getString("id", "");
			eid = preflogin.getString("eid", "");
			
			txt_rsvp_attend_title.setText(preflogin.getString("event_name", ""));
		}
		
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		txt_rsvp_attend_changersvp=(TextView)view.findViewById(R.id.txt_rsvp_attend_changersvp);
		txt_rsvp_attend_title=(TextView)view.findViewById(R.id.txt_rsvp_attend_title);
		txt_rsvp_attend_viewonrsvp=(TextView)view.findViewById(R.id.txt_rsvp_attend_viewonrsvp);
		txt_rsvp_attend_myrsvp=(TextView)view.findViewById(R.id.txt_rsvp_attend_myrsvp);
		txt_rsvp_attend_myrsvp_status=(TextView)view.findViewById(R.id.txt_rsvp_attend_myrsvp_status);
		
		fs.settypefacetextviewbellota(txt_rsvp_attend_changersvp);
		fs.settypefacetextviewbellota(txt_rsvp_attend_title);
		fs.settypefacetextviewbellota(txt_rsvp_attend_viewonrsvp);
		fs.settypefacetextviewbellota(txt_rsvp_attend_myrsvp);
		fs.settypefacetextviewbellota(txt_rsvp_attend_myrsvp_status);
		txt_rsvp_attend_changersvp.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.txt_rsvp_attend_changersvp:
			FragmentManager fm = getActivity().getSupportFragmentManager();
	        FragmentTransaction ft = fm.beginTransaction();
	       
	        Fragment fragment = null;
	 		fragment = new Fragment_My_RSVP_Change_rsvp();
			
		       //ft.setCustomAnimations(R.anim.animation_leave,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		        ft.commit();
			break;

		default:
			break;
		}
	}

}
