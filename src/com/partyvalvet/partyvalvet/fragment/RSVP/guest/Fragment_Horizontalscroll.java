package com.partyvalvet.partyvalvet.fragment.RSVP.guest;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;

import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_Horizontalscroll extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	
	Button right,left;
	HorizontalScrollView hsv;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.horizontalscroll, null);
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		
		bindid(view);
		
		/*right.setOnTouchListener(new OnTouchListener() {
			  private Handler mHandler;
			    private long mInitialDelay = 300;
			    private long mRepeatDelay = 100;
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				 switch (event.getAction()) {
		            case MotionEvent.ACTION_DOWN:
		                if (mHandler != null)
		                    return true;
		                mHandler = new Handler();
		                mHandler.postDelayed(mAction, mInitialDelay);
		                break;
		            case MotionEvent.ACTION_UP:
		                if (mHandler == null)
		                    return true;
		                mHandler.removeCallbacks(mAction);
		                mHandler = null;
		                break;
		        }
				return false;
			}
			 Runnable mAction = new Runnable() {
			        @Override
			        public void run() {
			            hsv.scrollTo((int) hsv.getScrollX() + 10, (int) hsv.getScrollY());
			            mHandler.postDelayed(mAction, mRepeatDelay);
			        }
			    };
		});*/
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		right=(Button)view.findViewById(R.id.right);
		left=(Button)view.findViewById(R.id.left);

		hsv =(HorizontalScrollView)view.findViewById(R.id.hsv);
		right.setOnClickListener(this);
		left.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		switch (v.getId()) {
		case R.id.right:
			hsv.scrollTo((int)hsv.getScrollX() + 80, (int)hsv.getScrollY());
			break;
		case R.id.left:
			hsv.scrollTo((int)hsv.getScrollX() - 80, (int)hsv.getScrollY());
			break;

		default:
			break;
		}
	}

}

