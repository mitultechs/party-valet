package com.partyvalvet.partyvalvet.fragment.RSVP.guest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_My_RSVP_guest_fillform extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	
	TextView txt_rsvp_guest_your_response,txt_rsvp_guest_your_response_yes,txt_rsvp_guest_your_response_no,txt_rsvp_guest_inviteby_host,txt_rsvp_guest_num_of_guest_attending,txt_rsvp_guest_spinner_value;
	TextView txt_rsvp_guest_menu_of_event,txt_rsvp_guest_menu_of_item_vegetable,txt_rsvp_guest_menu_of_item_listitem,txt_rsvp_guest_add_dining_choice,txt_rsvp_guest_add_to_cal,txt_rsvp_guest_dietary_restrictions,txt_rsvp_guest_submit,txt_rsvp_guest_cancel;
	LinearLayout lin_rsvp_guest_your_response_yes,lin_rsvp_guest_your_response_no,lin_ds_diningadd,lin_ds_container;
	FrameLayout frm_rsvp_guest_num_of_guest_attending;
	Spinner spin_rsvp_guest_spinner_num_of_guest;
	EditText edt_ds_diningcustomadd,edt_rsvp_guest_dietary_restrictions;
	ImageView img_ds_adddining,img_rsvp_guest_your_response_yes,img_rsvp_guest_your_response_no;
	ToggleButton toggleButton_rsvp_guest_add_to_cal;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_rsvp_guest, null);
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		
		bindid(view);
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		txt_rsvp_guest_your_response=(TextView)view.findViewById(R.id.txt_rsvp_guest_your_response);
		txt_rsvp_guest_your_response_yes=(TextView)view.findViewById(R.id.txt_rsvp_guest_your_response_yes);
		txt_rsvp_guest_your_response_no=(TextView)view.findViewById(R.id.txt_rsvp_guest_your_response_no);
		txt_rsvp_guest_inviteby_host=(TextView)view.findViewById(R.id.txt_rsvp_guest_inviteby_host);
		txt_rsvp_guest_num_of_guest_attending=(TextView)view.findViewById(R.id.txt_rsvp_guest_num_of_guest_attending);
		txt_rsvp_guest_spinner_value=(TextView)view.findViewById(R.id.txt_rsvp_guest_spinner_value);
		txt_rsvp_guest_menu_of_event=(TextView)view.findViewById(R.id.txt_rsvp_guest_menu_of_event);
		txt_rsvp_guest_menu_of_item_vegetable=(TextView)view.findViewById(R.id.txt_rsvp_guest_menu_of_item_vegetable);
		txt_rsvp_guest_menu_of_item_listitem=(TextView)view.findViewById(R.id.txt_rsvp_guest_menu_of_item_listitem);
		txt_rsvp_guest_add_dining_choice=(TextView)view.findViewById(R.id.txt_rsvp_guest_add_dining_choice);
		txt_rsvp_guest_add_to_cal=(TextView)view.findViewById(R.id.txt_rsvp_guest_add_to_cal);
		txt_rsvp_guest_dietary_restrictions=(TextView)view.findViewById(R.id.txt_rsvp_guest_dietary_restrictions);
		txt_rsvp_guest_submit=(TextView)view.findViewById(R.id.txt_rsvp_guest_submit);
		txt_rsvp_guest_cancel=(TextView)view.findViewById(R.id.txt_rsvp_guest_cancel);
		
		
		fs.settypefacetextviewbellota(txt_rsvp_guest_your_response);
		fs.settypefacetextviewbellota(txt_rsvp_guest_your_response_yes);
		fs.settypefacetextviewbellota(txt_rsvp_guest_your_response_no);
		fs.settypefacetextviewbellota(txt_rsvp_guest_inviteby_host);
		fs.settypefacetextviewbellota(txt_rsvp_guest_num_of_guest_attending);
		fs.settypefacetextviewbellota(txt_rsvp_guest_spinner_value);
		fs.settypefacetextviewbellota(txt_rsvp_guest_menu_of_event);
		fs.settypefacetextviewbellota(txt_rsvp_guest_menu_of_item_vegetable);
		fs.settypefacetextviewbellota(txt_rsvp_guest_menu_of_item_listitem);
		fs.settypefacetextviewbellota(txt_rsvp_guest_add_dining_choice);
		fs.settypefacetextviewbellota(txt_rsvp_guest_add_to_cal);
		fs.settypefacetextviewbellota(txt_rsvp_guest_dietary_restrictions);
		fs.settypefacetextviewbellota(txt_rsvp_guest_submit);
		fs.settypefacetextviewbellota(txt_rsvp_guest_cancel);
		
		
		
		lin_rsvp_guest_your_response_yes=(LinearLayout)view.findViewById(R.id.lin_rsvp_guest_your_response_yes);
		lin_rsvp_guest_your_response_no=(LinearLayout)view.findViewById(R.id.lin_rsvp_guest_your_response_no);
		lin_ds_diningadd=(LinearLayout)view.findViewById(R.id.lin_ds_diningadd);
		lin_ds_container=(LinearLayout)view.findViewById(R.id.lin_ds_container);
		
				
		frm_rsvp_guest_num_of_guest_attending=(FrameLayout)view.findViewById(R.id.frm_rsvp_guest_num_of_guest_attending);
		
		
		
		spin_rsvp_guest_spinner_num_of_guest=(Spinner)view.findViewById(R.id.spin_rsvp_guest_spinner_num_of_guest);
		
		edt_ds_diningcustomadd=(EditText)view.findViewById(R.id.edt_ds_diningcustomadd);
		edt_rsvp_guest_dietary_restrictions=(EditText)view.findViewById(R.id.edt_rsvp_guest_dietary_restrictions);
		
		fs.settypefaceedittextbellota(edt_ds_diningcustomadd);
		fs.settypefaceedittextbellota(edt_rsvp_guest_dietary_restrictions);
		
		img_ds_adddining=(ImageView)view.findViewById(R.id.img_ds_adddining);
		img_rsvp_guest_your_response_yes=(ImageView)view.findViewById(R.id.img_rsvp_guest_your_response_yes);
		img_rsvp_guest_your_response_no=(ImageView)view.findViewById(R.id.img_rsvp_guest_your_response_no);
		
		
		toggleButton_rsvp_guest_add_to_cal =(ToggleButton)view.findViewById(R.id.toggleButton_rsvp_guest_add_to_cal);
		
		txt_rsvp_guest_submit.setOnClickListener(this);
		txt_rsvp_guest_cancel.setOnClickListener(this);
		
		lin_rsvp_guest_your_response_yes.setOnClickListener(this);
		lin_rsvp_guest_your_response_no.setOnClickListener(this);
		lin_ds_diningadd.setOnClickListener(this);
		lin_ds_container.setOnClickListener(this);

		img_ds_adddining.setOnClickListener(this);
		
		frm_rsvp_guest_num_of_guest_attending.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.txt_rsvp_guest_submit:

			break;
		case R.id.txt_rsvp_guest_cancel:

			break;
		case R.id.lin_rsvp_guest_your_response_yes:
			
			

			break;
		case R.id.lin_rsvp_guest_your_response_no:

			break;
		case R.id.lin_ds_diningadd:

			break;
		case R.id.lin_ds_container:

			break;
		case R.id.img_ds_adddining:

			break;
		case R.id.frm_rsvp_guest_num_of_guest_attending:

			break;

		default:
			break;
		}
	}

}
