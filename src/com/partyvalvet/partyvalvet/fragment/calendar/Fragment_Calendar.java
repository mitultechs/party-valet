package com.partyvalvet.partyvalvet.fragment.calendar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_Calendar extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;

	// ---------------------------------------
	//private TextView Output;
	//private Button changeDate;

	private int year;
	private int month1;
	private int day;

	static final int DATE_PICKER_ID = 1111;

	// ------------------------------------

	public GregorianCalendar month, itemmonth;// calendar instances.

	public CalendarAdapter adapter;// adapter instance
	public Handler handler;// for grabbing some event values for showing the dot
							// marker.
	public ArrayList<String> items; // container to store calendar items which
									// needs showing the event marker
	public ArrayList<String> selecteditems;

	String selected_date = " ";
	TextView title;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.calendar, null);
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		MainActivity.screen = "addtocalendar";
		MainActivity.txt_main_title.setText(getString(R.string.headingaddtocal));

		bindid(view);

		Locale.setDefault(Locale.US);
		month = (GregorianCalendar) GregorianCalendar.getInstance();
		itemmonth = (GregorianCalendar) month.clone();

		items = new ArrayList<String>();
		selecteditems = new ArrayList<String>();
		adapter = new CalendarAdapter(getActivity(), month);

		// ----------------------
	//	Output = (TextView) view.findViewById(R.id.Output);
	//	changeDate = (Button) view.findViewById(R.id.changeDate);

		// Get current date by calender

		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month1 = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);

		// -------------------------
		GridView gridview = (GridView) view.findViewById(R.id.gridview);
		gridview.setAdapter(adapter);

		handler = new Handler();
		handler.post(calendarUpdater);

		gridview.setBackgroundColor(Color.WHITE);
		TextView title = (TextView) view.findViewById(R.id.title);
		title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));

		RelativeLayout previous = (RelativeLayout) view.findViewById(R.id.previous);

		previous.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setPreviousMonth();
				refreshCalendar();
			}
		});

		RelativeLayout next = (RelativeLayout) view.findViewById(R.id.next);
		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setNextMonth();
				refreshCalendar();

			}
		});

		gridview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				
				
				//
				adapter.setSelected(v);
				//adapter.notifyDataSetChanged();
				//((CalendarAdapter) parent.getAdapter()).setSelected(v);
				/*	//selecteditems.add(CalendarAdapter.dayString.get(position));
				
				String selectedGridDate = CalendarAdapter.dayString
						.get(position);
				String[] separatedTime = selectedGridDate.split("-");
				String gridvalueString = separatedTime[2].replaceFirst("^0*",
						"");// taking last part of date. ie; 2 from 2012-12-02.
				int gridvalue = Integer.parseInt(gridvalueString);
				// navigate to next or previous month on clicking offdays.
				if ((gridvalue > 10) && (position < 8)) {
					setPreviousMonth();
					 refreshCalendar();

				} else if ((gridvalue < 7) && (position > 28)) {
					setNextMonth();
					 refreshCalendar();
				}
			//	adapter.setSelected(v);
				 showToast(selectedGridDate);
				try {
					selected_date = selected_date.concat(String
							.valueOf(selectedGridDate));

					selected_date = selected_date.concat(",");
				} catch (Exception e) {
					// TODO: handle exception

				}
				showToast(selected_date);
*/
			}
		});
		// -----------------------------------------
		/*Output.setText(new StringBuilder()

				// Month is 0 based, just add 1
				.append(month1 + 1).append("-").append(day).append("-")
				.append(year).append(" "));
*/
		// Button listener to show date picker dialog

		
		// ----------------------------------------

		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		 title = (TextView) view.findViewById(R.id.title);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}
	
	
	
	 
	    protected Dialog onCreateDialog(int id) {
	        switch (id) {
	        case DATE_PICKER_ID:
	             
	            // open datepicker dialog. 
	            // set date picker for current date 
	            // add pickerListener listner to date picker
	            return new DatePickerDialog(getActivity(), pickerListener, year, month1,day);
	        }
	        return null;
	    }
	 
	    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {
	 
	        // when dialog box is closed, below method will be called.
	        @Override
	        public void onDateSet(DatePicker view, int selectedYear,
	                int selectedMonth, int selectedDay) {
	             
	            year  = selectedYear;
	            month1 = selectedMonth;
	            day   = selectedDay;
	 
	            // Show selected date 
	           /* Output.setText(new StringBuilder().append(month1 + 1)
	                    .append("-").append(day).append("-").append(year)
	                    .append(" "));
	     	   */        	            
	           }
	        };
	
	
	
	
	
	
	
	
	
	
	
	//-------------------------------------
	protected void setNextMonth() {
		if (month.get(GregorianCalendar.MONTH) == month
				.getActualMaximum(GregorianCalendar.MONTH)) {
			month.set((month.get(GregorianCalendar.YEAR) + 1),
					month.getActualMinimum(GregorianCalendar.MONTH), 1);
		} else {
			month.set(GregorianCalendar.MONTH,
					month.get(GregorianCalendar.MONTH) + 1);
			}

	}

	protected void setPreviousMonth() {
		if (month.get(GregorianCalendar.MONTH) == month
				.getActualMinimum(GregorianCalendar.MONTH)) {
			month.set((month.get(GregorianCalendar.YEAR) - 1),
					month.getActualMaximum(GregorianCalendar.MONTH), 1);
		} else {
			month.set(GregorianCalendar.MONTH,
					month.get(GregorianCalendar.MONTH) - 1);
		}

	}

	protected void showToast(String string) {
		Toast.makeText(getActivity(), string, Toast.LENGTH_SHORT).show();

	}

	public void refreshCalendar() {
		 

		adapter.refreshDays();
		adapter.notifyDataSetChanged();
		handler.post(calendarUpdater); // generate some calendar items

		title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
	}

	public Runnable calendarUpdater = new Runnable() {

		@Override
		public void run() {
			items.clear();

			// Print dates of the current week
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
			String itemvalue;
			for (int i = 0; i < 7; i++) {
				itemvalue = df.format(itemmonth.getTime());
				itemmonth.add(GregorianCalendar.DATE, 1);
				items.add("2012-09-12");
				items.add("2012-10-07");
				items.add("2012-10-15");
				items.add("2012-10-20");
				items.add("2012-11-30");
				items.add("2012-11-28");
			}

		//	adapter.setItems(items);
			adapter.notifyDataSetChanged();
		}
	};

}
