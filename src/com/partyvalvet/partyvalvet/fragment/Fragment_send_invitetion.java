package com.partyvalvet.partyvalvet.fragment;

import java.io.File;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.fragment.RSVP.host.Fragment_RSVP_host_Detail_Schedule;
import com.partyvalvet.partyvalvet.fragment.calendar.Fragment_Calendar;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_send_invitetion extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	
	TextView txt_send_now,txt_schedul_later;
	LinearLayout lin_send_now,lin_schedul_later;
	ImageView img_invitetion_privew;
	SharedPreferences preflogin;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_send_invitetion, null);
		MainActivity.screen = "sendinvitation";
		MainActivity.txt_main_title.setText(getString(R.string.headingsendinvitaion));
				
		
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		fs.showheading();
		
		bindid(view);
		
		preflogin = getActivity().getSharedPreferences("login",
				Context.MODE_PRIVATE);
		
		  String image_name = preflogin.getString("image_name", "");
		
		
		  File f = new File(image_name);
		  
		  Bitmap bmp = BitmapFactory.decodeFile(f.getAbsolutePath());
		  img_invitetion_privew.setImageBitmap(bmp);
		
		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		txt_send_now=(TextView)view.findViewById(R.id.txt_send_now);		
		txt_schedul_later=(TextView)view.findViewById(R.id.txt_schedul_later);	
		
		lin_send_now=(LinearLayout)view.findViewById(R.id.lin_send_now);
		lin_schedul_later=(LinearLayout)view.findViewById(R.id.lin_schedul_later);
		img_invitetion_privew=(ImageView)view.findViewById(R.id.img_invitetion_privew);
		
		fs.settypefacetextview(txt_send_now);
		fs.settypefacetextview(txt_schedul_later);
		
		lin_send_now.setOnClickListener(this);
		txt_schedul_later.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
       
        Fragment fragment = null;
		
		switch (v.getId()) {
		case R.id.lin_send_now:
			   fragment = new Fragment_RSVP_host_Detail_Schedule();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		       ft.commit();
			break;
	
		case R.id.txt_schedul_later:
			   fragment = new Fragment_Calendar();
			
		       ft.setCustomAnimations(R.anim.animation_enter,R.anim.animation_leave);
		       ft.replace(R.id.framl_main_screen, fragment);
		       ft.commit();
			break;
	

		default:
			break;
		}
	}

}
