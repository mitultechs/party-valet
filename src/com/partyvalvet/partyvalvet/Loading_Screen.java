package com.partyvalvet.partyvalvet;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class Loading_Screen extends Activity {

	ImageView  img_loding;
	SharedPreferences preflogin;
	String userid="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_loading__screen);

		preflogin = getSharedPreferences("login",
				Context.MODE_PRIVATE);
		if (preflogin!=null) {
			userid = preflogin.getString("id", "");
		}
		 img_loding = (ImageView)findViewById(R.id.img_loding);
		
		 img_loding.setBackgroundResource(R.drawable.loading_anim);
		 AnimationDrawable frameAnimation =(AnimationDrawable)img_loding.getBackground();
		 
		/* frameAnimation.setCallback(img_loding);
		 frameAnimation.setVisible(true, true);
		*/ frameAnimation.start();
		
	
		Handler handler1 = new Handler();
		final Runnable r9 = new Runnable() {
			public void run() {

				Intent intent = new Intent(getApplicationContext(),
						MainActivity.class);
				overridePendingTransition(R.anim.animation_enter,R.anim.animation_leave);
				startActivity(intent);				
				finish();

			}
		};
		handler1.postDelayed(r9, 2350);
		
	//	handler1.postDelayed(r9, 1450);
	}
}
