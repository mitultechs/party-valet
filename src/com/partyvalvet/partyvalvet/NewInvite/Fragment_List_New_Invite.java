package com.partyvalvet.partyvalvet.NewInvite;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;

import com.partyvalvet.partyvalvet.MainActivity;
import com.partyvalvet.partyvalvet.R;
import com.partyvalvet.partyvalvet.customeAdepter.Customlist_List_New_Invite;
import com.partyvalvet.partyvalvet.customeAdepter.Customlist_My_rsvp_hosting;
import com.partyvalvet.partyvalvet.font.Font_Setting;
import com.partyvalvet.partyvalvet.fragment.RSVP.Fragment_My_RSVP_Hosting.gethostinglist;
import com.partyvalvet.partyvalvet.helper.ConnectionDetector;
import com.partyvalvet.partyvalvet.helper.ServiceHandler;

public class Fragment_List_New_Invite extends Fragment implements OnClickListener {

	Font_Setting fs;
	ServiceHandler sh;
	
	
	ListView list_new_invite;
	
	SharedPreferences preflogin;

	String userid,eid,url;
	Customlist_List_New_Invite adapter;
	ArrayList<HashMap<String, String>> arraylist;
	
	AsyncTask<Void, Void, Void> task;
	
	int flag=0;
	
	ProgressDialog pDialog ;
	
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_list_new_invite, null);
		fs = new Font_Setting(getActivity());
		sh = new ServiceHandler();
		MainActivity.txt_main_title.setText(getString(R.string.headingnewinvite));
		MainActivity.screen="myinvitation";
		
		arraylist = new ArrayList<HashMap<String, String>>();
		bindid(view);
		preflogin = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
		if (preflogin!=null) {
			
			userid = preflogin.getString("id", "");
			eid = preflogin.getString("eid", "");
			

		}
		
		
		
		if (ConnectionDetector
				.isNetworkAvailable(getActivity())) {

			//urlmyinvitation = getString(R.string.url)+"select_my_card/index.php?user_id="+userid;
			//url = getString(R.string.url)+"select_envelope_liner/index.php?plan=all";
			//urlmyeneveleop = getString(R.string.url)+"select_my_envelope/index.php?user_id="+userid;
			//url = getString(R.string.url)+"select_myrsvp/index.php?event_id=6";
			
			url = getString(R.string.url)+"select_myrsvp/index.php?event_id="+eid;
			
		task =	new getnewinvite();
		task.execute();
		
		} else {
			fs.toast(getString(R.string.nointernet));
		}

		return view;
	}

	private void bindid(View view) {
		// TODO Auto-generated method stub
		list_new_invite =(ListView)view.findViewById(R.id.list_new_invite);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	
	public class getnewinvite extends AsyncTask<Void, Void, Void>
	{

		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			String jsonstringattanding = sh.makeServiceCall(url, ServiceHandler.GET);
			if (jsonstringattanding!=null) {
				
				try {
					JSONObject jobj = new JSONObject(jsonstringattanding);
					if (jobj.has("200")) {
						if (arraylist.size()>0||arraylist!=null) {
							arraylist.clear();
						}
						JSONArray arydata =  jobj.getJSONArray("200");
						if (arydata.length()>0) {
							flag=1;
							for (int i = 0; i < arydata.length(); i++) {
								JSONObject objdata = arydata.getJSONObject(i);
								HashMap<String, String> map = new HashMap<String, String>();
								map.put("event_name", objdata.getString("event_name"));
								map.put("event_date", objdata.getString("event_date"));
								//map.put("event_status",objdata.getString("event_status"));
								
								arraylist.add(map);
							}
							
						}
					}
					else if (jobj.has("400")) {
						flag=0;
					}
					
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			if (flag==1) {
				adapter= new Customlist_List_New_Invite(getActivity(), arraylist);
				list_new_invite.setAdapter(adapter);
			}
			
			
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage(getString(R.string.plzwait));
            pDialog.setCancelable(false);
            pDialog.show();
		}
		
	}
	public void onDestroy() {
	    super.onDestroy();

	    if(task != null && task.getStatus() != AsyncTask.Status.FINISHED) {
	        task.cancel(true);
	    }
	}


}
