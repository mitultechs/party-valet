package com.partyvalvet.partyvalvet;


import android.support.v4.app.FragmentActivity;
import android.view.Window;
import android.view.WindowManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class Splesh_Screen extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// Remove title bar
				this.requestWindowFeature(Window.FEATURE_NO_TITLE);

				// Remove notification bar
				this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
						WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splesh__screen);

		
		Handler handler1 = new Handler();
		final Runnable r9 = new Runnable() {
			public void run() {

				Intent intent = new Intent(getApplicationContext(),
						Loading_Screen.class);
				overridePendingTransition(R.anim.animation_enter,R.anim.animation_leave);
				startActivity(intent);
				
				finish();

			}
		};
	//	handler1.postDelayed(r9, 1000);
		handler1.postDelayed(r9, 2000);
		
		
	}

	
}
